<?php
  include 'lib/nusoap.php'; 
  require_once '../wp-content/plugins/auto-get-numbers/extends.php';
  
  $server = new soap_server();
  $server->configureWSDL('giovangchotso', 'urn:giovangchotso');
  
  function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') 
  {
    for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
    return $s;
  }
  
  function generatePasswordWP()
  {
    return mt_rand_str(4);
  }
  
  function haspPasswordWP($post_password)
  {
    return md5($post_password);
  }
  
  function checkService($mobile_customer, $params)
  {
    $commands = array(
                    'DK1'    =>  'HUYMB',
                    'DKMB'   =>  'HUYMB', 
                    'DK2'    =>  'HUYMT', 
                    'DKMT'   =>  'HUYMT', 
                    'DK3'    =>  'HUYMN', 
                    'DKMN'   =>  'HUYMN', 
                    'DK4'    =>  'HUYTTB', 
                    'DKTTB'  =>  'HUYTTB', 
                    'DK5'    =>  'HUYTTT', 
                    'DKTTT'  =>  'HUYTTT', 
                    'DK6'    =>  'HUYTTN', 
                    'DKTTN'  =>  'HUYTTN'
                  );  
    
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    
    $query = mysql_query("SELECT params, command FROM tbl_mps_api_subscribe WHERE msisdn='".$mobile_customer."' AND params='".$params."'") or die(mysql_error());
    
    $rows = array();
    while($row = mysql_fetch_array($query)) 
    {
      $cc = str_replace(' ', '', strtoupper($row['command']));
      if($params == "1")
      {
        $rows[] = array($cc=>$row['params']);
      }
      else
      {
        $rows[] = array($commands[$cc]=>$row['params']);
      }
    } 
    
    return $rows;
  }
  
  function countKeyArray($array)
  {
    $count = array();
    
    $count_huymb = 0;
    $count_huymt = 0;
    $count_huymn = 0;
    $count_huyttb = 0;
    $count_huyttt = 0;
    $count_huyttn = 0;
    
    foreach($array as $data)
    {
      foreach($data as $key => $value)
      {
        switch($key)
        {
          case 'HUYMB':
            $count_huymb++;
          break;
          
          case 'HUYMT':
            $count_huymt++;
          break;
          
          case 'HUYMN':
            $count_huymn++;
          break;
          
          case 'HUYTTB':
            $count_huyttb++;
          break;
          
          case 'HUYTTT':
            $count_huyttt++;
          break;
          
          case 'HUYTTN':
            $count_huyttn++;
          break;
        }
      }
    }
    
    if($count_huymb >= 1)
    {
      $count['HUYMB'] = $count_huymb;
    }
    
    if($count_huymt >= 1)
    {
      $count['HUYMT'] = $count_huymt;
    }
    
    if($count_huymn >= 1)
    {
      $count['HUYMN'] = $count_huymn;
    }
    
    if($count_huyttb >= 1)
    {
      $count['HUYTTB'] = $count_huyttb;
    }
    
    if($count_huyttt >= 1)
    {
      $count['HUYTTT'] = $count_huyttt;
    }
    
    if($count_huyttn >= 1)
    {
      $count['HUYTTN'] = $count_huyttn;
    }
    
    return $count;
  }

  function getAllDeleteServices($mobile)
  {
    return countKeyArray(checkService($mobile, '1'));
  }
  
  function getAllRegistService($mobile)
  {
    return countKeyArray(checkService($mobile, '0'));
  }
  
  function getCompareArrays($mobile)
  {
    $recommands = array(
                    'HUYMB'    =>  'DK1', 
                    'HUYMT'    =>  'DK2', 
                    'HUYMN'    =>  'DK3', 
                    'HUYTTB'   =>  'DK4', 
                    'HUYTTT'   =>  'DK5', 
                    'HUYTTN'   =>  'DK6'
                  );  
                  
    $del_sers = getAllDeleteServices($mobile);
    $res_sers = getAllRegistService($mobile);
    
    $status = array();
    foreach($res_sers as $key => $value)
    {
      $ck_key = array_key_exists(strtoupper($key), $del_sers);
      if($ck_key)
      {
        if($value > $del_sers[strtoupper($key)])
        {
          $status[$recommands[strtoupper($key)]] = "1";
        }
        else
        {
          $status[$recommands[strtoupper($key)]] = "0";
        }
      }
      else
      {
        $status[$recommands[strtoupper($key)]] = "1";
      }
    }
    
    return $status;
  }
  
  function smsRequest($username, $password, $msisdn, $content, $shortcode, $alias, $params)
  {
  
    $param = array(
                'username'    =>  $username,
                'password'    =>  $password,
                'msisdn'      =>  $msisdn,
                'content'     =>  $content,
                'shortcode'   =>  $shortcode,
                'alias'       =>  $alias,
                'params'      =>  $params
              );
              
    $client = new nusoap_client('http://10.58.128.105:7300/smsws?wsdl','wsdl');
    // $url_service = "http://localhost/wp1/giovangchotso/smsws.php?wsdl";
    $result = $client->call('smsRequest', $param);
    
    return $result;
  }
  
  function subRequest($username, $password, $serviceid, $msisdn, $chargetime, $params, $mode, $amount, $command)
  {
    if($username == 'gvcssubcribe' && $password == 'JjgkjdbfYIu847HJ88#%')
    {
      $extends = new functionExtends();
      
      $created_at = date('Y-m-d H:i:s');
      $fields1 = 'username, password, serviceid, msisdn, chargetime, params, mode, amount, command, created_at, updated_at	';
      $values1 = "'".$username."', '".md5($password)."', '".$serviceid."', '".$msisdn."', '".$chargetime."', '".$params."', '".$mode."', '".$amount."', '".$command."', '".$created_at."', '".$created_at."'";
      
      $log1 = "subRequest =>  Date:  ".$created_at."  |  Fields:  ".$fields1."  | Values: ".$values1."  |  Command:  ".$command."\n"; 
      error_log($log1, 3, "var/log/service.log");
      
      $password_send = generatePasswordWP();
      $password_customer = haspPasswordWP($password_send);
      
      $commands = array(
                        'DK1'=>'DK1','DK MB'=>'DK MB',
                        'DK2'=>'DK2','DK MT'=>'DK MT',
                        'DK3'=>'DK3','DK MN'=>'DK MN',
                        'DK4'=>'DK4','DK TTB'=>'DK TTB',
                        'DK5'=>'DK5','DK TTT'=>'DK TTT',
                        'DK6'=>'DK6','DK TTN'=>'DK TTN'
                      );
      
      $flags = getCompareArrays($msisdn);
      if(!array_search('1', $flags) && array_search(strtoupper($command), $commands) == strtoupper($command))
      {  
        $extends->getInsertNewValues('tbl_mps_api_subscribe', $fields1, $values1);
        $contentMT = 'Mat khau dang nhap DV Gio Vang Chot So la: '.$password_send.'. Moi Quy khach truy cap http://giovangchotso.vn de su dung DV. Chi tiet goi 19006430 (1000d/p).';  
        $queue = smsRequest('mps_test', 'mps201407', $msisdn, $contentMT, '5143', '5143', 'Text');
        
        $fields_smsgw = 'username, password, msisdn, content, shortcode, alias, params, results, created_at, updated_at';
        $values_smsgw = "'mps_test', 'mps201407', '".$msisdn."', '".$contentMT."', '5143', '5143', 'Text', '".$queue['return']."', '".$created_at."', '".$created_at."'";
        $extends->getInsertNewValues('tbl_mps_smsgw_mtlistener', $fields_smsgw, $values_smsgw);
        
        switch($queue['return'])
        {
          case "1":
            return "1"; 
          break;

          case "2":
            return "2";
          break;
          
          case "200":
            return "200";
          break;
          
          case "201":
            return "201";
          break;
          
          case "202":
            return "202";
          break;
          
          case "203":
            return "203";
          break;
          
          case "400":
            return "400";
          break;
        }
      }
      else
      {
        $extends->getInsertNewValues('tbl_mps_api_subscribe', $fields1, $values1);
      }  
      
      $fields2 = 'fullname_customer, username_customer, password_customer, mobile_customer, payments_customer, status_customer, serviceid, created_at, updated_at';
      
      $passwords = $extends->getAllValueOfField('tbl_customer', 'password_customer', 'mobile_customer', $msisdn, 'where');
      if($passwords && !empty($passwords))
      {
        foreach($passwords as $val)
        {
          if(!empty($val))
          {  
            $password_customer = $val;
          }
        }
      }
      
      $values2 = "'".$msisdn."', '".$msisdn."', '".$password_customer."', '".$msisdn."', 'MPS', '0', '".$serviceid."', '".$created_at."', '".$created_at."'";
      $log2 = "subRequest =>  Date:  ".$created_at."  |  Fields:  ".$fields2."  | Values: ".$values2."\n"; 
      error_log($log2, 3, "var/log/service.log");
      
      if(empty($serviceid) || empty($msisdn) || empty($chargetime) || empty($mode) || empty($command)) 
      {
        return "300"; 
      }
      
      switch(strtoupper(trim($mode)))
      {
        case "CHECK":
          $extends->getInsertNewValues('tbl_customer', $fields2, $values2);
        break;
        
        case "REAL":
          $extends->getInsertNewValues('tbl_customer', $fields2, $values2);
        break;
        
        default:
          return "300";
        break;
      }
    }
    else
    {
      return "301";
    }
    
    return "0";
  }

  $server->register('subRequest', array('username' => 'xsd:string','password' => 'xsd:string','serviceid' => 'xsd:string','msisdn' => 'xsd:string','chargetime' => 'xsd:dateTime','params' => 'xsd:string','mode' => 'xsd:string','amount' => 'xsd:int','command' => 'xsd:string'),array('result' => 'xsd:string'));

  $query = isset($HTTP_RAW_POST_DATA)? $HTTP_RAW_POST_DATA : '';
  $server->service($query);
?>