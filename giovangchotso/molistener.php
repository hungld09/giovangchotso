<?php
  include 'lib/nusoap.php';
  require_once '../wp-content/plugins/auto-get-numbers/connection.php';
  require_once '../wp-content/plugins/auto-get-numbers/extends.php';
  
  $server = new soap_server();
  $server->configureWSDL('giovangchotso', 'urn:giovangchotso');
  
  function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') 
  {
    for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
    return $s;
  }
  
  function generatePasswordWP()
  {
    return mt_rand_str(4);
  }
  
  function haspPasswordWP($post_password)
  {
    return md5($post_password);
  }
  
  function getPassWord($mobile_customer)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");

    $query = mysql_query("SELECT password_customer FROM tbl_customer WHERE mobile_customer='".$mobile_customer."'") or die(mysql_error());
    $password['password_customer'] = mysql_fetch_array($query);

    return $password;
  }
  
  function checkService($mobile_customer, $params)
  {
    $commands = array(
                    'DK1'    =>  'HUYMB',
                    'DKMB'   =>  'HUYMB', 
                    'DK2'    =>  'HUYMT', 
                    'DKMT'   =>  'HUYMT', 
                    'DK3'    =>  'HUYMN', 
                    'DKMN'   =>  'HUYMN', 
                    'DK4'    =>  'HUYTTB', 
                    'DKTTB'  =>  'HUYTTB', 
                    'DK5'    =>  'HUYTTT', 
                    'DKTTT'  =>  'HUYTTT', 
                    'DK6'    =>  'HUYTTN', 
                    'DKTTN'  =>  'HUYTTN'
                  );  
    
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    
    $query = mysql_query("SELECT params, command FROM tbl_mps_api_subscribe WHERE msisdn='".$mobile_customer."' AND params='".$params."'") or die(mysql_error());
    
    $rows = array();
    while($row = mysql_fetch_array($query)) 
    {
      $cc = str_replace(' ', '', strtoupper($row['command']));
      if($params == "1")
      {
        $rows[] = array($cc=>$row['params']);
      }
      else
      {
        $rows[] = array($commands[$cc]=>$row['params']);
      }
    } 
    
    return $rows;
  }
  
  function countKeyArray($array)
  {
    $count = array();
    
    $count_huymb = 0;
    $count_huymt = 0;
    $count_huymn = 0;
    $count_huyttb = 0;
    $count_huyttt = 0;
    $count_huyttn = 0;
    
    foreach($array as $data)
    {
      foreach($data as $key => $value)
      {
        switch($key)
        {
          case 'HUYMB':
            $count_huymb++;
          break;
          
          case 'HUYMT':
            $count_huymt++;
          break;
          
          case 'HUYMN':
            $count_huymn++;
          break;
          
          case 'HUYTTB':
            $count_huyttb++;
          break;
          
          case 'HUYTTT':
            $count_huyttt++;
          break;
          
          case 'HUYTTN':
            $count_huyttn++;
          break;
        }
      }
    }
    
    if($count_huymb >= 1)
    {
      $count['HUYMB'] = $count_huymb;
    }
    
    if($count_huymt >= 1)
    {
      $count['HUYMT'] = $count_huymt;
    }
    
    if($count_huymn >= 1)
    {
      $count['HUYMN'] = $count_huymn;
    }
    
    if($count_huyttb >= 1)
    {
      $count['HUYTTB'] = $count_huyttb;
    }
    
    if($count_huyttt >= 1)
    {
      $count['HUYTTT'] = $count_huyttt;
    }
    
    if($count_huyttn >= 1)
    {
      $count['HUYTTN'] = $count_huyttn;
    }
    
    return $count;
  }

  function getAllDeleteServices($mobile)
  {
    return countKeyArray(checkService($mobile, '1'));
  }
  
  function getAllRegistService($mobile)
  {
    return countKeyArray(checkService($mobile, '0'));
  }
  
  function getCompareArrays($mobile)
  {
    $recommands = array(
                    'HUYMB'    =>  'DK1', 
                    'HUYMT'    =>  'DK2', 
                    'HUYMN'    =>  'DK3', 
                    'HUYTTB'   =>  'DK4', 
                    'HUYTTT'   =>  'DK5', 
                    'HUYTTN'   =>  'DK6'
                  );  
                  
    $del_sers = getAllDeleteServices($mobile);
    $res_sers = getAllRegistService($mobile);
    
    $status = array();
    foreach($res_sers as $key => $value)
    {
      $ck_key = array_key_exists(strtoupper($key), $del_sers);
      if($ck_key)
      {
        if($value > $del_sers[strtoupper($key)])
        {
          $status[$recommands[strtoupper($key)]] = "1";
        }
        else
        {
          $status[$recommands[strtoupper($key)]] = "0";
        }
      }
      else
      {
        $status[$recommands[strtoupper($key)]] = "1";
      }
    }
    
    return $status;
  }
  
  function smsRequest($username, $password, $msisdn, $content, $shortcode, $alias, $params)
  {
  
    $param = array(
                'username'    =>  $username,
                'password'    =>  $password,
                'msisdn'      =>  $msisdn,
                'content'     =>  $content,
                'shortcode'   =>  $shortcode,
                'alias'       =>  $alias,
                'params'      =>  $params
              );
              
    $client = new nusoap_client('http://10.58.128.105:7300/smsws?wsdl','wsdl');
    // $url_service = "http://localhost/wp1/giovangchotso/smsws.php?wsdl";
    $result = $client->call('smsRequest', $param);
    
    return $result;
  }
  
  function moRequest($username, $password, $source, $dest, $content)
  {
    if($username == 'gvcsmolistener' && $password == 'JHBSjhjHJ$#5hjgh8866')
    {
      
      $created_at = date('Y-m-d H:i:s');
      if(substr($source,0,2) == "84")
      {
        $source = substr($source,2);
      }
      
      $log = "moRequest   =>  Date:  ".$created_at."  |  username:  ".$username."  | password: ".md5($password)."  | source: ".$source."  | dest: ".$dest."  | content: ".$content."\n";     
      error_log($log, 3, "var/log/service.log");
      
      $fields = 'username, password, source, dest, content, created_at, updated_at';
      $values = "'".$username."', '".md5($password)."', '".$source."', '".$dest."', '".$content."', '".$created_at."', '".$created_at."'";
      
      $extends = new functionExtends();
      $extends->getInsertNewValues('tbl_mps_smsgw_molistener', $fields, $values);
      
      switch(strtoupper(trim($content)))
      {
        case 'MK':
          $password_send = generatePasswordWP();
          $password_customer = haspPasswordWP($password_send);
          
          $idAll = $extends->getAllValueOfField('tbl_customer', 'id', 'mobile_customer', $source, 'where');
          foreach($idAll as $val)
          {
            $params = array("password_customer"=>$password_customer, "updated_at"=>$created_at);
            $where = "id = '".$val."'";
            $extends->getUpdateValueNew('tbl_customer', $params, $where);
          }
          
          $customer = $extends->checkExistValueField($source, 'msisdn', 'tbl_mps_api_subscribe', 'where');
          switch($customer)
          {
            case 'true':
              $flags = getCompareArrays($source);
              if(!empty($flags) && array_search('1', $flags))
              {
                $contentMT = 'Mat khau dang nhap DV Gio Vang Chot So la: '.$password_send.'. Moi Quy khach truy cap http://giovangchotso.vn de su dung DV. Chi tiet goi 19006430 (1000d/p).';
                $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
              }
              else
              {
                $contentMT = 'Quy khach chua dang ky DV Gio Vang Chot So. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac goi 19006430 (1.000d/p).';
                $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
              }
            break;
            
            case 'false':
              $contentMT = 'Quy khach chua dang ky DV Gio Vang Chot So. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac goi 19006430 (1.000d/p).';
              $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
            break;
          }
          
          $log_sms = 'smsRequest => Date: '.$created_at. ' | Response Result: '.$queue['return'].' | username: mps_test, password: mps201407, source: '.$source.', content: '.$contentMT.' dest: '.$dest.', alias: '.$dest.', params: Text';
          error_log($log_sms, 3, "var/log/service.log");
          
          $fields_smsgw = 'username, password, msisdn, content, shortcode, alias, params, results, created_at, updated_at';
          $values_smsgw = "'mps_test', 'mps201407', '".$source."', '".$contentMT."', '".$dest."', '".$dest."', 'Text', '".$queue['return']."', '".$created_at."', '".$created_at."'";
          $extends->getInsertNewValues('tbl_mps_smsgw_mtlistener', $fields_smsgw, $values_smsgw);
          
          switch($queue['return'])
          {
            case "1":
              return "1"; 
            break;

            case "2":
              return "2";
            break;
            
            case "200":
              return "200";
            break;
            
            case "201":
              return "201";
            break;
            
            case "202":
              return "202";
            break;
            
            case "203":
              return "203";
            break;
            
            case "400":
              return "400";
            break;
          }
          
        break;
        
        case 'KT':
          $customer = $extends->checkExistValueField($source, 'msisdn', 'tbl_mps_api_subscribe', 'where');
          switch($customer)
          {
            case 'true':
              $flags = getCompareArrays($source);
              if(!empty($flags) && array_search('1', $flags))
              {
                $services = array(
                  "DK1"=>"KQXS Mien Bac (2.000d/ngay)",
                  "DK2"=>"KQXS Mien Trung (2.000d/ngay)", 
                  "DK3"=>"KQXS Mien Nam (2.000d/ngay)",
                  "DK4"=>"Tuong thuat truc tiep KQXS Mien Bac (3.000d/ngay)", 
                  "DK5"=>"Tuong thuat truc tiep KQXS Mien Trung (3.000d/ngay)", 
                  "DK6"=>"Tuong thuat truc tiep KQXS Mien Nam (3.000d/ngay)"
                );
                
                $pK_sv_price = ''; $i = 0;
                foreach($flags as $key=>$val)
                {
                  if($val == "1")
                  {
                    if($i == 0)
                    {
                      $pK_sv_price .= $services[$key];
                    }
                    else
                    {
                      $pK_sv_price .= ', '.$services[$key];
                    }
                    $i++;
                  }
                }
                
                $contentMT = 'Quy khach dang su goi cuoc '.$pK_sv_price.'  cua DV Gio Vang Chot So. Vui long truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac goi 19006430 (1.000d/p).';
                $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
              }
              else
              {
                $contentMT = 'Quy khach chua su dung DV Gio Vang Chot So. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac goi 19006430 (1.000d/p).';
                $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');    
              }
            break;
            
            case 'false':
              $contentMT = 'Quy khach chua su dung DV Gio Vang Chot So. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac goi 19006430 (1.000d/p).';
              $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
            break;
          }
          
          
          $log_sms = 'smsRequest => Date: '.$created_at. ' | Response Result: '.$queue['return'].' | username: mps_test, password: mps201407, source: '.$source.', content: '.$contentMT.' dest: '.$dest.', alias: '.$dest.', params: Text';
          
          error_log($log_sms, 3, "var/log/service.log");
          
          $fields_smsgw = 'username, password, msisdn, content, shortcode, alias, params, results, created_at, updated_at';
          $values_smsgw = "'mps_test', 'mps201407', '".$source."', '".$contentMT."', '".$dest."', '".$dest."', 'Text', '".$queue['return']."', '".$created_at."', '".$created_at."'";
          $extends->getInsertNewValues('tbl_mps_smsgw_mtlistener', $fields_smsgw, $values_smsgw);
          
          switch($queue['return'])
          {
            case "1":
              return "1"; 
            break;

            case "2":
              return "2";
            break;
            
            case "200":
              return "200";
            break;
            
            case "201":
              return "201";
            break;
            
            case "202":
              return "202";
            break;
            
            case "203":
              return "203";
            break;
            
            case "400":
              return "400";
            break;
          }
        break;
        
        default:
          $contentMT = 'Tin nhan sai cu phap. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac goi 19006430 (1.000d/p).';
          $queue = smsRequest('mps_test', 'mps201407', $source, $contentMT, $dest, $dest, 'Text');
          
          $fields_smsgw = 'username, password, msisdn, content, shortcode, alias, params, results, created_at, updated_at';
          $values_smsgw = "'mps_test', 'mps201407', '".$source."', '".$contentMT."', '".$dest."', '".$dest."', 'Text', '".$queue['return']."', '".$created_at."', '".$created_at."'";
          
          $log_sms = 'smsRequest => Date: '.$created_at. ' | Response Result: '.$queue['return'].' | username: mps_test, password: mps201407, source: '.$source.', content: '.$contentMT.' dest: '.$dest.', alias: '.$dest.', params: Text';
          error_log($log_sms, 3, "var/log/service.log");
          
          $extends->getInsertNewValues('tbl_mps_smsgw_mtlistener', $fields_smsgw, $values_smsgw);
          return "200";
        break;
      }
    }
    else
    {
      return "201";
    }
    
    return "0";
  }
  
  $server->register('moRequest', array('username' => 'xsd:string','password' => 'xsd:string','source' => 'xsd:string','dest' => 'xsd:string','content' => 'xsd:string'),array('result' => 'xsd:string'));
  
  $query = isset($HTTP_RAW_POST_DATA)? $HTTP_RAW_POST_DATA : '';
  $server->service($query);
?>