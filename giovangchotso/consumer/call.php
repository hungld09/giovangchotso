<?php
  header("Content-Type: text/xml; charset=utf-8");
  include('../libs/nusoap.php');
  $query    =   isset($HTTP_RAW_POST_DATA)  ? $HTTP_RAW_POST_DATA : 0;
  $sp       =   new soap_parser($query, 'UTF-8', 'POST');
  
  $gdb      =   $sp->buildVal(3);
  $g1       =   $sp->buildVal(4);
  $g2       =   $sp->buildVal(5);
  $g3       =   $sp->buildVal(6);
  $g4       =   $sp->buildVal(7);
  $g5       =   $sp->buildVal(8);
  $g6       =   $sp->buildVal(9);
  $g7       =   $sp->buildVal(10);
  $g8       =   $sp->buildVal(11);
  $subcode  =   $sp->buildVal(12);
  $ngay     =   $sp->buildVal(13);
  
  // $c        =   new soapclient('http://localhost/wp1/giovangchotso/service.php/Reverse');
  $c        =   new soapclient('http://125.212.225.107:8991/giovangchotso/service.php/Reverse');
  $c->call('Reverse', array('GDB'=>$gdb,'G1'=>$g1,'G2'=>$g2,'G3'=>$g3,'G4'=>$g4,'G5'=>$g5,'G6'=>$g6,'G7'=>$g7,'G8'=>$g8,'subcode'=>$subcode,'ondate'=>$ngay));
  
  echo $c->responseData;
  