﻿<?php
/**
 * Options Management Administration Screen.
 *
 * If accessed directly in a browser this page shows a list of all saved options
 * along with editable fields for their values. Serialized data is not supported
 * and there is no way to remove options via this page. It is not linked to from
 * anywhere else in the admin.
 *
 * This file is also the target of the forms in core and custom options pages
 * that use the Settings API. In this case it saves the new option values
 * and returns the user to their page of origin.
 *
 * @package WordPress
 * @subpackage Administration
 */
?>

<?php 
  
  require_once 'connection.php';
  
  try
  {
    if( 
        isset($_POST['cname']) && !empty($_POST['cname']) && 
        isset($_POST['cmobile']) && !empty($_POST['cmobile']) && 
        isset($_POST['cpassword']) && !empty($_POST['cpassword']) &&
        isset($_POST['serviceHidden']) && !empty($_POST['serviceHidden']) 
        $_POST['cmobile'] == $_POST['cpassword']
      )
    {  
      echo 'success';
      
    }
  }
  catch(Exeption $e)
  {
    echo 'error';
  }