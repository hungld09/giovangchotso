﻿<?php 
  require_once 'extends.php';
  $extends = new functionExtends();

  if(isset($_GET['page']))
  {
    $per_page = 20;  
    $page = $_GET['page'];
    $start = ($page-1)*$per_page;
    $allresults = $extends->getAllResult("tbl_result", "created_at", $start.", ".$per_page);
    echo '<table class="table table-bordered" id="table-ketqua-xoso">
            <thead>
              <tr>
                <th>#ID</th>
                <th>Tỉnh</th>
                <th>Đặc biệt</th>
                <th>Giải nhất</th>
                <th>Giải nhì</th>
                <th>Giải ba</th>
                <th>Giải tư</th>
                <th>Giải năm</th>
                <th>Giải sáu</th>
                <th>Giải bảy</th>
                <th>Giải tám</th>
                <th>Ngày tạo</th>
                <th>Công cụ</th>
              </tr>
            </thead>
            <tbody>';
                
          foreach($allresults as $result)
          {  
            if(!empty($result))
            {
              echo '<tr>';
              foreach($result as $key=>$val)
              { 
                if($key == "id")
                {  
                  echo '<th scope="row">'.$val.'</th>';
                }
                else if($key == "content_result")
                {
                  if(!empty($val))
                  {
                    $vals = unserialize($val);
                    if(!empty($vals))
                    {
                      foreach($vals as $kdt=>$dt)
                      {
                        foreach($dt as $ke=>$va)
                        {
                          if($kdt == "mb")
                          {
                            if(!in_array($ke, array("Lototructiep", "Dauloto", "Ditloto")))
                            {
                              if($ke == "giaibay")
                              {
                                echo '<td>'.str_replace("-", "<br>", $va).'</td><td>Không có</td>';
                              }
                              else
                              {
                                echo '<td>'.str_replace("-", "<br>", $va).'</td>';
                              }
                            }
                          }
                          else
                          {
                            if(!in_array($ke, array("Lototructiep", "Dauloto", "Ditloto")))
                            {
                              echo '<td>'.str_replace("-", "<br>", $va).'</td>';
                            }
                          }
                        }
                      }
                    }
                  }
                }
                else if($key != "status_result" && $key != "updated_at")
                {
                  echo '<td>'.($key == "created_at" ? str_replace("-", "/", $val) : str_replace("-", "<br>", $val)).'</td>';
                }      
              }
              echo '
                      <td class="xs-tools">
                        <a href="javascript:void(0)" class="detail-result" xs_id = "'.$result['id'].'">
                          <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><span class="glyphicon-class">Chi tiết</span><br>
                        </a>
                        <a href="#" title="Hiển thị">
                          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span><span class="glyphicon-class">Xóa bỏ</span><br>
                        </a>
                        <a href="#" title="Hiển thị">
                          <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="glyphicon-class">Sửa chữa</span>
                        </a>
                      </td>
                    </tr>
                ';
                    
            }        
          }
    echo '</tbody>
        </table>';  
    
  }
  echo '<div class="popUpDetail" style="z-index:9999;padding:10px;display:none; width: 80%;position: fixed;top: 20%; left: 10%; border: 1px solid #ccc;background:#fff">';
  echo '<div class="" style="height: 20px;margin-bottom: 10px;border-bottom: 1px solid #ccc; width: 100%;"><a href="javascript::void()" class="closePopUp" style="float:right">x</a></div>';
  echo '<table class="table table-bordered" id="table-ketqua-xoso-detail">';
            
  echo '</table>';
  echo '</div>';
  echo ' 
        <script type="text/javascript">
            function showPopUp(){
                $(".popUpDetail").show();
            }
            function hidePopUp(){
                $(".popUpDetail").hide();
            }
            jQuery(document).ready(function($){
                $(".detail-result").click(function(){
                    var xs_id = $(this).attr("xs_id"); 
                    $.ajax({
                        type: "POST",
                        url:  "'.plugins_url('/resdata-detail.php',__FILE__).'",
                        data: {"xs_id":xs_id},
                        dataType:"html",
                        success: function(html){
                            alert(html); return;
                            $("#table-ketqua-xoso-detail").html(html);
                            showPopUp();
                        }
                     });   
                });
                $(".closePopUp").click(function(){
                    $(".popUpDetail").hide();
                });
            }); 
        </script> ';