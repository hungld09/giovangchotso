﻿<?php 
  include 'AES.class.php';
  
  class executedAESClass
  {
  
    function __construct(){ 
    }
    
    function getPublicKeyVT() 
    {
      return '-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0gzslfAyeD5AViregoWB
RdrHQn+PxhHVUnEcP+wZno5+RTaUQGRhu7TQwbrJ45eLf2E7zvgpcvH2GIX24xdz
UdUZ7wrgJI0DBoq8mFM/K2u0a65o0iFBi91p8CjScNdh1GiW7/z1JV0oOXI5VwYy
J7+mGaf3alWyyfGk8lyFKHicbojH8qN3E8WMMaDh+sQIrkoYELoXbEiprUDDqcQQ
7zyWbxb1+Y9trjgXGmboPNN8NBQ74rL4RUnxcFBizc7VMCj/nZazXe6OmTkOC0uK
ksnMaw97dcycNDLBKbO2pYxThV//das9znapwSPCvlFuAmtuIp4ruAk39ygs8be7
3VIf2ZtHBuVFKGfk6ELAdTgRFTDN2Iz0eIzTTc7E2wCJ2XX/aS2KAd+DMdGEFNPo
q+b6U9Vwj5KB+JN3JE6kwzxxtlThqxM0sQfwKwWkx89wjZ0LX5/6EnHamliX/TZD
/ZBDhOQ+3mhEsMeJDjtc2OUllHmufw//yYwY9tdgtlC9ALCOtN6xRn++8mJqDk5q
zA6xBoR5xmehf+/5BbLCYHbxxEQtBq9kEZFrUenyolJ1oApi0xoVmOk+Ya36+dG/
qqbzvnJLs4SriO38DXck0fgXxEqGhdfDxy+l9XYpzmMZVGOexdb94cGO74Qu4kY+
panymLbWjKLIHPhMJVQ0I0MCAwEAAQ==
-----END PUBLIC KEY-----
';
    }
    
    function getPrivateKeyVT() 
    {
      return '';
    }
    
    function getPublicKeyCP() 
    {
      return '-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAn4gR5IHyPXQmZQRkprxW
kcmot0tGM1BupXKE6YIm5T3WJ6AJGrmFbma5NB0xzGVQduicl+cDvlaum5bTBYtv
q1sHXj+Y/da2iUHaoT2M8gVrA/6VZFKc2tB8Ng88Y5ZZCD+jlXRmi+mcKHIgSr8U
8FlRrnaoIfCql+S7hL7Wczo611Ewi5+Jtjgiz5bCDIdoNTj6t4F1j9TCpa5ljYLo
zPLURwslDa7jR+bqXwmE84dr0BNnks4gteQgH2CL9VqNTLFcbK0Leq51KNEsEFGG
PHLt2PuyLDOFtPhDpEHlgHdDet3dTPnjhfp17tWfNXSSq03yzQ2byLSQUEHi9aMX
Ovtgs2A77u3SqAEmR23Pv6Y5GS09yJQmajSQMlAaantoEDWc9WUF0HU3WjE+Si2E
gPsKNIqFF4y82Hm1Kl+u7CjuR2jHVtv1VZ49EWERx16lzPARuoEyCocGva4X9Vfi
E4cErM+1N9LClUNAEsCC/aRYsG0oiTddxith+mig+hySt6xSfWruz5lJZG+AoKcF
BebICHw2HWgRmrnJrnPO5mamdCdZ2OGBvUBLuO4NUltCHji2c6vThHIeGp4TMTNO
f/N47UCUrau4A2YrKiHLTMGd/kPq787oTko7+VJYKRk97KY8zAp+RtkiSbT6vKfn
nkHkvDJIssbVVrKGT0k4DHMCAwEAAQ==
-----END PUBLIC KEY-----
';
    }
    
    function getPrivateKeyCP() 
    {
      return '-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAn4gR5IHyPXQmZQRkprxWkcmot0tGM1BupXKE6YIm5T3WJ6AJ
GrmFbma5NB0xzGVQduicl+cDvlaum5bTBYtvq1sHXj+Y/da2iUHaoT2M8gVrA/6V
ZFKc2tB8Ng88Y5ZZCD+jlXRmi+mcKHIgSr8U8FlRrnaoIfCql+S7hL7Wczo611Ew
i5+Jtjgiz5bCDIdoNTj6t4F1j9TCpa5ljYLozPLURwslDa7jR+bqXwmE84dr0BNn
ks4gteQgH2CL9VqNTLFcbK0Leq51KNEsEFGGPHLt2PuyLDOFtPhDpEHlgHdDet3d
TPnjhfp17tWfNXSSq03yzQ2byLSQUEHi9aMXOvtgs2A77u3SqAEmR23Pv6Y5GS09
yJQmajSQMlAaantoEDWc9WUF0HU3WjE+Si2EgPsKNIqFF4y82Hm1Kl+u7CjuR2jH
Vtv1VZ49EWERx16lzPARuoEyCocGva4X9VfiE4cErM+1N9LClUNAEsCC/aRYsG0o
iTddxith+mig+hySt6xSfWruz5lJZG+AoKcFBebICHw2HWgRmrnJrnPO5mamdCdZ
2OGBvUBLuO4NUltCHji2c6vThHIeGp4TMTNOf/N47UCUrau4A2YrKiHLTMGd/kPq
787oTko7+VJYKRk97KY8zAp+RtkiSbT6vKfnnkHkvDJIssbVVrKGT0k4DHMCAwEA
AQKCAgBW1qC2vC16WjFUJ5IKWNbewOC/9Y+e+xdfo/jJivlVK9XY9/o8u+Pv9qa1
wXaMBlJIh9JFtZCKGXEubGktyXrd0c0u9nt3IVt3V4uE3ZZNX7mjEP+M/pM3YOlc
G+sJYpvQK6QGBLiVTHKeaZE+XsktE9GsoBZhcznJOM8CMUdWrcm9zwoX1p4FvITm
bu/uFf8ZiYygOy4dPvk9arMI1suWVFBM47olup8pUn8dgHcIgRykxQtKFD4mBKmf
qtREmCf2KU1gMsne2/wWXdYYg+gIG59wz4FrIhiUK9fWH7Cnv0PSK9b8wcGh+nYb
rdjeJjh4WxPhnpjAJdmwdkzu3PoVWIALUPR1zBiVEgT9dwrqUIXzt43MHTF0Hstw
bFEQ8JIaoEXfr1mcRL2kqfJ8bcCD+u8MUy9LtvJPzJc6/uguY3mIpwkY2kjq3Gr4
XhxTqrSS0BqLm+TrkuatOZPWwvjGtqEtZi0GOGUeFE+kFP2ODQPzAIs3RVPRScnS
cJvMW5BP8nwiIz7T54KPLGYXKtMSGtfTqkcNrDh4HmXDJk9z1NpdApQVe85RHI4X
K0Mp3P3z+12mB9I/8+ZcdulrrgIPKA8IECc/JRGTS8aFnh+PDaanDo3+z8XBTr5R
OZCmoyY/LkxtgdVag/snBftDoHep6svqwOybcVgclE7AQ1N6MQKCAQEA69qRrPBt
Yz/k2Gd6xJAHk+5q5Cj+bQVkGHHq37k+IVkFZGuE+MdEcctE7OxQfZU9yLPpybs2
k5fsmA+OUKR3qU+uKSKf6bIBcGksFG3XuQzsvDTaHfvcxXBWsSTw9eN3cpL3q6aD
1PGimzs/Qo+yAI/rghw4xUJMVKuwkDykTBd0O0uBfypjMjLT71+FZ0uaSk50EW7Z
ONzHM2uUqMZPZbL8mVZjqmg3rf28cCsxSDw9pEie8IKmJmFXJhWBjboRO+ztWz9n
1Nw4NwCR4nTbrkV2iTL25b2jNf4cHCwbaQDh9U4YLARd5MJTKXY3rddNByDAvVEZ
6ZbLmgtiPQRepwKCAQEArSiOgvGrltd45YiYTdn3dGptFkcnZFRwNrr7XBqF03Es
hxqBMRbztJYU9D24wZhrV/m5x6p/mzHRlO1UmSS9LjM6ntuOHml3/hRGI+O4BlRG
mcDrpp68/gmBhgFLjKq83VF1P+IfUkFAFyoJ3i81WMPusZ+ZBVawSzNowb4SkIml
2UfwBweOA1n4xtGy6pIwbGVEi+I8BfFarFgl2THhTYTBVSRspdQJRnkwOL8WYXzZ
lLClDrqS5xj5R8ELO3J+3MLAmBviKTwm09+YMyE+ebUuzxOsSUAho5C4Mrp7r173
zLxFOeS7Fvr5TowoWYZ2NjtAjoEaUkLKyaJRkeFJVQKCAQBicAonDP4K04sXCzGB
Qr/27SZt7fIq3HonfbxS/gTBdF88x2drUffuKrGs1QDPOW/dCcJE8T9ZIKqd2LhE
TjCnWNtOzeXCawoQucStDh9gW9Wew8xZgVpmiXmVW966MeMTzeVTHh+dy/BbK8I+
bK0VcPwRhOfo5L/pAvOhb73/CDb53IzC35UhSXeNS+fo8fQGmXH2KLa+9d6qRnd4
bbSLtMWa1iSo5fB/TQzOICjkhkgP/kNgDJd+I1h1S7qTxZoV0dBLKk6S2AsaYcGB
q9ZYiSNtJflIb/rjuRhQZdRW4ghtEtgh3vy1UqnU/0ND6j17AR+QpH3VMZvuwluR
Lfa3AoIBAQCmE9UDwFjL1yQsOmOhn6HYNU8lY99jOh1aUOJOx119m/mSJxisBZB+
rsewLharD8481daaq2uyZQjXpv1R2MvdqOLbhEb94jhlJ/rR51IUN/Zy93bNG0i2
+lUROuLEKunz93HWcMGLueuHjBlk0bB/z4R7NlCkry1tIwShdfUGTg8UpAuSImvR
yRRzVKMemlH5VPN2mujo5kEKNY0vkMi/+ckYKVRPhFvVDNz0QEpt/DoPg5D73HST
U/+bE4r7XGhSwjPIcE3tMLUo1zZ6K7NpNh6MHBlNNEZPQ7l3ZibE3/gZlKVsznWm
p9Y/wOveWbm9b/0N8MNXDJrE43sKyKH1AoIBAQDKNWct4Of7QOOyNk49DLVoAjnf
6hmy0mzmo+xpnmsitqsrTCcK2gV4mZj8mSPwwi9RaxhhCcgRekgzKiJ11iea5OQv
vST0q+8x9Q6LDE7fZQiN9fk9LOgsjAl63P8UZ0TYRjuayd4k6lE0AL3DlHfx4yMM
ln/ICGySnFT+E285agCrDGlA7UyGdM4Ba48hyQKTCB9SwKbvjdv/MNbe8mkEf8W7
Py4Ppbn/hpSlS++BcV/QGcEoS7xhyizUbzOSqtw4s9AJi2Ceyvlgp12l6AedNOZS
t3u35S1FRCpENf8rBKXgneaLQ+DgO2ZfTnj5cbL5GyAgPxwyCNYezTfuBp17
-----END RSA PRIVATE KEY-----
';
    }

    function encrypt($encrypt, $key)
    {
      $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, pack("H*", $key), $encrypt, MCRYPT_MODE_ECB, $iv));
      return $encrypted;
    }

    function decrypt($decrypt, $key)
    { 
      $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128,  pack("H*", $key), base64_decode($decrypt), MCRYPT_MODE_ECB, $iv);
      return $decrypted;
    }

    function pkcs5_pad ($text, $blocksize) 
    { 
      $pad = $blocksize - (strlen($text) % $blocksize); 
      return $text . str_repeat(chr($pad), $pad); 
    }
    
    function getDataEncrypted($data, $aeskey, $pub_key)
    {
      $aesexec = new executedAESClass(); // $aesexec->
      
      // Tham số cần mã hoá
      $data = $aesexec->pkcs5_pad($data, 16);

      //B1. Ma hoa du lieu bang AES	
      $value_encrypt_aes = $aesexec->encrypt($data, $aeskey);

      //B2. Input du lieu co gan key AES
      $value_with_key = 'value='.$value_encrypt_aes.'&key='.$aeskey;

      //B3. Ma hoa du lieu bang public key	
      openssl_public_encrypt($value_with_key, $data_encrypted, $pub_key);
      $data_encrypted = base64_encode($data_encrypted);
      
      // URL Encode
      return urlencode($data_encrypted);
    }
    
    function getSignature($data_encrypted, $pri_key_cp)
    {
      //B4. Ky ban tin 
      $signature ='';
      openssl_sign($data_encrypted, $signature, $pri_key_cp, OPENSSL_ALGO_SHA1);

      // Base64 Encode
      $signature = base64_encode($signature);

      // URL Encode
      return urlencode($signature);
    }
    
    function getLinkRedirect($server, $aeskey, $data, $pub_key, $pri_key_cp, $paraRequest)
    {
      $aesexec = new executedAESClass(); // $aesexec->
      
      // URL Encode
      $data_encrypted = $aesexec->getDataEncrypted($data, $aeskey, $pub_key);

      // URL Encode
      $signature = $aesexec->getSignature($data_encrypted, $pri_key_cp);

      // URL Final of Charge
      $href = $server.$paraRequest.'&DATA='.urlencode($data_encrypted).'&SIG='.$signature;
      
      // return '<a href="'.$href.'" title="'.$title.'" class="'.$classstyle.'" role="button">'.$title.'</a>';
      return $href;
    }
    
    function getLinkRender($paraEncrypt, $paraRequest, $status)
    {
      $aesexec = new executedAESClass(); // $aesexec->
        
      $z          = 'abcdefghijuklmno0123456789012345';
      $iv         = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $aes        = new AES($z, 'CBC', $iv);
      $encrypted  = $aes->encrypt();
      $aeskey     = bin2hex($encrypted);

      $pub_key    = $aesexec->getPublicKeyVT();
      $pri_key_cp = $aesexec->getPrivateKeyCP();
      
      $server = 'http://125.235.32.12/MPS/';
      switch($status)
      {
        case "1":
          $server = 'http://125.235.4.194/test/';
          // $server = 'http://vas.vietteltelecom.vn/test/';
          // $server = 'http://vas.vietteltelecom.biz/test/';
        break;
        
        case "0":
          $server = 'http://vas.vietteltelecom.vn/MPS/';
        break;
      }
      
      return $aesexec->getLinkRedirect($server, $aeskey, $paraEncrypt, $pub_key, $pri_key_cp, $paraRequest);
    }
    
    function getLinkPayment($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
    }
    
    function getLinkMobile($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
    }
    
    function getLinkLogin($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
      
    }
    
    function getLinkLoginClient($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
    }
    
    function getLinkCheckStatus($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
    }
    
    function getLinkPaymentOTP($parasneedencrypt, $parassendtohttprequest, $status)
    {
      $aesexec = new executedAESClass();
      return $aesexec->getLinkRender($parasneedencrypt, $parassendtohttprequest, $status);
    }
    
    // function getMainInterface($status)
    // {
      // $aesexec = new executedAESClass(); // $aesexec->

      // $html = '';  
      // $html .= '<meta charset="utf-8">';
      // $html .= '<p style="word-wrap: break-word;"><strong>THANH TOÁN GIẢ LẬP CONTENT PROVIDER (CP)</strong><p/>';
      
      // /***            Thanh toán            ***/
      // /***    a.      Tham số cần mã hoá    ***/
      // /***    b.	    Tham số gửi lên HTTP request    ***/
      
           // /* PRO	    Định danh của CP trên hệ thống MPS */
           // /* SER	    Dịch vụ thanh toán */
           // /* SUB	    Mô tả gói cước của dịch vụ */
           // /* DATA	  Dữ liệu sau khi đã được mã hoá  */
           // /* SIG	    Chữ ký của dữ liệu mã hoá */
          
          
          
      // $sub1      =   'Daily charge';                                       //  SUB	    Mô tả gói cước của dịch vụ    
      // $cmd1      =   'REGISTER';                                           //  CMD	    Thanh toán cho hình thức nào: DOWNLOAD, REGISTER, CANCEL, RESTORE,… RESTORE: với command này hệ thống sẽ thực hiện trừ tiền nếu qua chu kỳ trừ cước với thuê bao dùng dịch vụ đang ở trạng thái inactive
          
      // $cate1     =   'Giovangchotso_Ketqua1';                              //  CATE	  Danh mục của dịch vụ cần thanh toán
      // $item1     =   'Giovangchotso_Ketqua1';                              //  ITEM	  Sản phẩm cần thanh toán
      // $sub_cp1   =   'DCV';                                                //  SUB_CP	Nhà cung cấp đầu cuối
      // $cont1     =   'Đăng ký gói kết quả miền bắc dịch vụ Giovangchotso'; //  CONT	  Mô tả thêm
      // $price1    =   '2000';                                               //  PRICE	  Giá tiền thanh toán (VNĐ)
      // $req1      =   'Giovangchotso_Ketqua1';                              //  REQ	    Mã định danh giao dịch bên CP
      // $mobile1   =   '01689022985';                                        //  MOBILE	Số điện thoại cần thanh toán. Nếu trường hợp truyền số điện thoại sang mà truy cập qua 3G thì sẽ không check nữa mà charge qua tham số này, áp dụng cho các CP được phép charge trực tiếp
      // $source1   =   '';                                                   //  SOURCE	Nguồn thanh toán (WAP/CLIENT), nếu không truyền dữ liệu sang, mặc định là wap. Nếu là app thì in luôn giá trị, không cần phải trả lại link url redirect nữa 
      // $type1     =   'Mobile';                                             //  TYPE	  Hình thức thanh toán, nếu không có tham số này thì mặc định là thanh toán trên tài khoản Mobile. Ngoài ra các tài khoản khác như: card, banking, vas,…
      
      
      // /*** Thanh toán ***/
      // $parasneedencrypt1 = 'SUB='.$sub1.'&CATE='.$cate1.'&ITEM='.$item1.'&SUB_CP='.$sub_cp1.'&CONT='.$cont1.'&PRICE='.$price1.'&REQ='.$req1.'&MOBILE='.$mobile1.'&SOURCE='.$source1.'&TYPE='.$type1;
      // $parassendtohttprequest1 = 'charge.html?PRO=DCV&SER=GIOVANGCHOTSO&SUB='.$sub1.'&CMD='.$cmd1;
      // $html .= $aesexec->getLinkPayment($parasneedencrypt1, $parassendtohttprequest1, "1", "btn btn-primary");
      
      // /*** Lấy Số Mobile ***/
      // $sub2      =   'Daily charge';                                       //  SUB	    Mô tả gói cước của dịch vụ  
      // $req2      =   'Giovangchotso_Ketqua1';                              //  REQ	    Mã định danh giao dịch bên CP 
      // $source2   =   '';                                                   //  SOURCE	Nguồn thanh toán (WAP/CLIENT), nếu không truyền dữ liệu sang, mặc định là wap. Nếu là app thì in luôn giá trị, không cần phải trả lại link url redirect nữa 
      // $sess2     =   '';                                                   //  SESS	  Session id của khách hàng khi truy cập vào site của CP
      
      // $parasneedencrypt2 = 'SUB'.$sub2.'REQ='.$req2.'&SOURCE='.$source2.'&SESS='.$sess2;
      // $parassendtohttprequest2 = 'mobile.html?PRO=DCV&SER=GIOVANGCHOTSO&SUB='.$sub2;
      // $html .= $aesexec->getLinkMobile($parasneedencrypt2, $parassendtohttprequest2, "1", "btn btn-default");
      
      
      
      // /*** Login ***/
      // $sub3    = 'GIOVANGCHOTSO_GOIKETQUA1';                                // SUB	Dịch vụ thanh toán
      // $req3    = 'Giovangchotso_Ketqua1';                                   // REQ	Mã định danh giao dịch bên CP
      // $source3 = '';                                                        // Nguồn thanh toán (WAP/CLIENT), nếu không truyền dữ liệu sang, mặc định là wap 
      // $sess3   = '';                                                        // Session id của khách hàng
      
      // $parasneedencrypt3 = 'SUB='.$sub3.'&REQ='.$req3.'&SOURCE='.$source3.'&SESS='.$sess3;
      // $parassendtohttprequest3 = 'login.html?PRO=DCV&SER=GIOVANGCHOTSO&SUB=GIOVANGCHOTSO_GOIKETQUA1';
      // $html .= $aesexec->getLinkLogin($parasneedencrypt3, $parassendtohttprequest3, "1", "btn btn-success");
      
      
      
      // /*** Login Client ***/
      // $sub4    = 'Giovangchotso_Ketqua1';                                    // SUB	Gói cước dịch vụ thanh toán
      // $req4    = 'Giovangchotso_Ketqua1';                                    // REQ	Mã định danh giao dịch bên CP
      // $imei4   = '';                                                         // IMEI	Số imei của thiết bị, bắt buộc phải có khi source_type = CLIENT
      // $source4 = '';                                                         // SOURCE	Nguồn thanh toán (CLIENT/WAP), nếu không truyền dữ liệu sang, mặc định là wap 
      // $sess4   = '';                                                         // SESS	Session id của khách hàng khi login qua client của CP
      // $subb4   = 'Daily charge';                                             // SUB	Mô tả gói cước của dịch vụ
      // $user4   = '01689022985';                                              // Số mobile KH, dành cho việc khi truy cập qua wifi hoặc client
      // $pass4   = '';                                                         // PASS	Mật khẩu của KH, dành cho việc khi truy cập qua wifi hoặc client      
      
      // $parasneedencrypt4 = 'SUB='.$sub4.'&REQ='.$req4.'&IMEI='.$imei4.'&SOURCE='.$source4.'&SESS='.$sess4;
      // $parassendtohttprequest4 = 'login.html?PRO=DCV&SER=GIOVANGCHOTSO&SUB='.$subb4.'&USER='.$user4.'&PASS='.$pass4;
      // $html .= $aesexec->getLinkLoginClient($parasneedencrypt4, $parassendtohttprequest4, "1", "btn btn-info");
      
      
      // /*** Check trạng thái giao dịch ***/
      // $sub5    = 'Daily charge';                                             // SUB	Mô tả gói cước của dịch vụ
      // $cate5   = 'Giovangchotso_Ketqua1';                                    // CATE	Danh mục của dịch vụ cần thanh toán
      // $item5   = 'Giovangchotso_Ketqua1';                                    // Sản phẩm cần thanh toán
      // $sub_cp5 = 'DCV';                                                      // Nhà cung cấp đầu cuối
      // $cont5   = 'Đăng ký gói kết quả miền bắc dịch vụ Giovangchotso';       // CONT	 Mô tả thêm
      // $price5  = '2000';                                                     // PRICE	Giá thanh toán (mệnh giá 1.000 VNĐ)
      // $req5    = 'Giovangchotso_Ketqua1';                                    // REQ	Mã định danh giao dịch mà CP muốn check
      // $mobile5 = '01689022985';                                              // MOBILE	Số điện thoại cần thanh toán. Nếu trường hợp truyền số điện thoại sang mà truy cập qua 3G thì sẽ không check nữa mà charge qua tham số này
      // $source5 = '';                                                         // SOURCE	Nguồn thanh toán (client, wap/web, app), nếu không truyền dữ liệu sang, mặc định là wap. Nếu là app thì in luôn giá trị, không cần phải trả lại link url redirect nữa 
      
      // $parasneedencrypt5 = 'SUB='.$sub5.'&CATE='.$cate5.'&ITEM='.$item5.'&SUB_CP='.$sub_cp5.'&CONT='.$cont5.'&PRICE='.$price5.'&REQ='.$req5.'&MOBILE='.$mobile5.'&SOURCE='.$source5;
      // $parassendtohttprequest5 = 'check.html?PRO=DCV&CMD=CHECK&SER=GIOVANGCHOTSO&SUB='.$sub5;
      // $html .= $aesexec->getLinkCheckStatus($parasneedencrypt5, $parassendtohttprequest5, "1", "btn btn-danger");
      
      
      
      // /*** Thanh Toán OTP ***/
      // $sub6      = 'Daily charge';                                           // SUB	Mô tả gói cước của dịch vụ
      // $cate6     = 'Giovangchotso_Ketqua1';                                  // CATE	Danh mục của dịch vụ cần thanh toán
      // $item6     = 'Giovangchotso_Ketqua1';                                  // ITEM	Sản phẩm cần thanh toán
      // $sub_cp6   = 'DCV';                                                    // SUB_CP	Nhà cung cấp đầu cuối
      // $cont6     = 'Đăng ký gói kết quả miền bắc dịch vụ Giovangchotso';     // CONT	 Mô tả thêm
      // $price6    = '2000';                                                   // PRICE	Giá thanh toán (VNĐ)
      // $req6     = 'Giovangchotso_Ketqua1';                                  // REQ	Mã định danh giao dịch bên CP
      // $mobile6   = '01689022985';                                            // MOBILE	Số điện thoại cần thanh toán theo dạng OTP
      // $source6   = '';                                                       // SOURCE	Nguồn thanh toán (client, wap/web, app), nếu không truyền dữ liệu sang, mặc định là wap. Nếu là app thì in luôn giá trị, không cần phải trả lại link url redirect nữa 
      // $opt6      = '';                                                       // OTP	Mã thanh toán OTP. Thời gian timeout cho phép với giao dịch OTP từ lúc nhận kết mã đến lúc submit có thể cấu hình được, default sẽ là 120s

      // $opt_type6 = '';                                                       // OTP_TYPE	Yêu cầu thanh toán OTP: 0 – yêu cầu gửi OTP, 1 – yêu cầu thanh toán với mã OTP nhập lên, cp_request_id gửi lên khi submit OTP phải trùng với cp_request_id của giao dịch yêu cầu gửi OTP
      // $cmd6      = 'REGISTER';                                               // CMD	Thanh toán cho hình thức nào: DOWNLOAD, REGISTER, CANCEL,…
      
      // $parasneedencrypt6 = 'SUB='.$sub6.'&CATE='.$cate6.'&ITEM='.$item6.'&SUB_CP='.$sub_cp6.'&CONT='.$cont6.'&PRICE='.$price6.'&REQ='.$req6.'&MOBILE='.$mobile6.'&SOURCE='.$source6.'&OTP='.$otp6.'&OTP_TYPE='.$otp_type6;
      // $parassendtohttprequest6 = 'check.html?PRO=DCV&CMD='.$cmd6.'&SER=GIOVANGCHOTSO&SUB='.$sub6; 
      // $html .= $aesexec->getLinkPaymentOTP($parasneedencrypt6, $parassendtohttprequest6, "1", "btn btn-default");
      
      // return $html;
    // }
    
  }
  
  