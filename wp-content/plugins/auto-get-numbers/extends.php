<?php 
  require_once 'connection.php';
  
  class functionExtends
  {
      const VT_MPS_DETECT_MSISDN_ADDR= 'http://vas.vietteltelecom.vn/test/mobile.html';
      const VT_MPS_REDIRECT_ADDR = "http://vas.vietteltelecom.vn/MPS/charge.html";
      const VT_MPS_REDIRECT_ADDR_TEST = "http://125.235.4.194/test/charge.html";
      const VT_PROVIDER_ID= 'DCV';
      const VT_SERVICE_NAME_GIOVANG= 'GIOVANGCT_DCV';
      const VT_GIOVANG_CPCODE = "033";
      const KEY_PUBLIC_ACCOUNT = "chotsogiovang";
      const VT_SERVICE_NAME_GIOVANGCT_KQ1 = "GIOVANGCT_DCV_KQ1_NGAY";
      const VT_SERVICE_NAME_GIOVANGCT_KQ2 = "GIOVANGCT_DCV_KQ2_NGAY";
      const VT_SERVICE_NAME_GIOVANGCT_KQ3 = "GIOVANGCT_DCV_KQ3_NGAY";
      const VT_SERVICE_NAME_GIOVANGCT_TT1 = "GIOVANGCT_DCV_TT1_NGAY";
      const VT_SERVICE_NAME_GIOVANGCT_TT2 = "GIOVANGCT_DCV_TT2_NGAY";
      const VT_SERVICE_NAME_GIOVANGCT_TT3 = "GIOVANGCT_DCV_TT3_NGAY";
      const VT_ERR_NONE = 0;
        const VT_ERR_MSISDN_NOT_FOUND = 1;
        const VT_ERR_IP_NOT_IN_POOL = 4;
        const VT_ERR_MSISDN_NOT_VALID = 11;
        const VT_ERR_FIELD_NOT_FOUND_AMOUNT = 13;
        const VT_ERR_FIELD_NOT_FOUND_REQID = 14;
        const VT_ERR_FIELD_NOT_FOUND_VALUE = 15;
        const VT_ERR_FIELD_NOT_FOUND_AESKEY = 16;
        const VT_ERR_FIELD_NOT_FOUND_NAME_ITEM = 17;
        const VT_ERR_FIELD_NOT_FOUND_CATEGORY_ITEM = 18;
        const VT_ERR_INVALID_CPCODE = 22;
        const VT_ERR_INVALID_CHARGING = 23;
        const VT_ERR_MISSING_CONFIRMATION = 24;
        const VT_ERR_INVALID_REQID = 25;
        const VT_ERR_MPS_SYSTEM = 503;
        const VT_ERR_BUSSINESS_LOGIC = 101;
        const VT_ERR_SUBSCRIBE = 102;
        const VT_ERR_MOIBILE_CHARGING = 103;
        const VT_ERR_UNSUBSCRIBE = 104;
        const VT_ERR_INVALID_SIGNATURE = 201;
        const VT_ERR_CHARGING_NOT_SUCCESS = 202;
        const VT_ERR_INSUFFICENT_BALANCE = 401;
        const VT_ERR_NOT_REGISTERED = 402;
        const VT_ERR_SUBSCRIBER_NOT_FOUND = 403;
        const VT_ERR_INVALID_MSISDN = 404;
        const VT_ERR_NOT_FOUND_MSISDN = 406;
        const VT_ERR_NOT_VALID_ACTION_FOR_CP = 407;
        const VT_ERR_ALREADY_REGISTERED = 408;
        const VT_ERR_BLOCKED_ACCOUNT = 409;
        const VT_ERR_INVALID_VIETTEL_NUMBER = 410;
        const VT_ERR_ALREADY_CANCELED = 411;
        const VT_ERR_NOT_YET_REGISTERED = 412;
        const VT_ERR_PRICE_NOT_VALID = 413;
        const VT_ERR_STILL_IN_CHARGING_PERIOD = 414;
        const VT_ERR_SYSTEM_ERROR = 440;
        const VT_ERR_MSISDN_OWNER_CHANGED = 405;
        const VT_ERR_NOT_REGISTERED_NUMBER = 501;
        const VT_ERR_WRONG_MPS_PASSWORD = 202;
        const VT_ERR_MPS_ACCOUNT_NOT_FOUND = 203;
        const VT_ERR_MPS_ACCOUNT_NOT_REGISTERED = 204;
        const VT_ERR_MPS_ACCOUNT_ALREADY_REGISTERED = 205;
      const VT_PUBLIC_KEY = <<<EOD
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0gzslfAyeD5AViregoWB
RdrHQn+PxhHVUnEcP+wZno5+RTaUQGRhu7TQwbrJ45eLf2E7zvgpcvH2GIX24xdz
UdUZ7wrgJI0DBoq8mFM/K2u0a65o0iFBi91p8CjScNdh1GiW7/z1JV0oOXI5VwYy
J7+mGaf3alWyyfGk8lyFKHicbojH8qN3E8WMMaDh+sQIrkoYELoXbEiprUDDqcQQ
7zyWbxb1+Y9trjgXGmboPNN8NBQ74rL4RUnxcFBizc7VMCj/nZazXe6OmTkOC0uK
ksnMaw97dcycNDLBKbO2pYxThV//das9znapwSPCvlFuAmtuIp4ruAk39ygs8be7
3VIf2ZtHBuVFKGfk6ELAdTgRFTDN2Iz0eIzTTc7E2wCJ2XX/aS2KAd+DMdGEFNPo
q+b6U9Vwj5KB+JN3JE6kwzxxtlThqxM0sQfwKwWkx89wjZ0LX5/6EnHamliX/TZD
/ZBDhOQ+3mhEsMeJDjtc2OUllHmufw//yYwY9tdgtlC9ALCOtN6xRn++8mJqDk5q
zA6xBoR5xmehf+/5BbLCYHbxxEQtBq9kEZFrUenyolJ1oApi0xoVmOk+Ya36+dG/
qqbzvnJLs4SriO38DXck0fgXxEqGhdfDxy+l9XYpzmMZVGOexdb94cGO74Qu4kY+
panymLbWjKLIHPhMJVQ0I0MCAwEAAQ==
-----END PUBLIC KEY-----
EOD;
	
	const DCV_PUBLIC_KEY = <<<EOD
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAn4gR5IHyPXQmZQRkprxW
kcmot0tGM1BupXKE6YIm5T3WJ6AJGrmFbma5NB0xzGVQduicl+cDvlaum5bTBYtv
q1sHXj+Y/da2iUHaoT2M8gVrA/6VZFKc2tB8Ng88Y5ZZCD+jlXRmi+mcKHIgSr8U
8FlRrnaoIfCql+S7hL7Wczo611Ewi5+Jtjgiz5bCDIdoNTj6t4F1j9TCpa5ljYLo
zPLURwslDa7jR+bqXwmE84dr0BNnks4gteQgH2CL9VqNTLFcbK0Leq51KNEsEFGG
PHLt2PuyLDOFtPhDpEHlgHdDet3dTPnjhfp17tWfNXSSq03yzQ2byLSQUEHi9aMX
Ovtgs2A77u3SqAEmR23Pv6Y5GS09yJQmajSQMlAaantoEDWc9WUF0HU3WjE+Si2E
gPsKNIqFF4y82Hm1Kl+u7CjuR2jHVtv1VZ49EWERx16lzPARuoEyCocGva4X9Vfi
E4cErM+1N9LClUNAEsCC/aRYsG0oiTddxith+mig+hySt6xSfWruz5lJZG+AoKcF
BebICHw2HWgRmrnJrnPO5mamdCdZ2OGBvUBLuO4NUltCHji2c6vThHIeGp4TMTNO
f/N47UCUrau4A2YrKiHLTMGd/kPq787oTko7+VJYKRk97KY8zAp+RtkiSbT6vKfn
nkHkvDJIssbVVrKGT0k4DHMCAwEAAQ==
-----END PUBLIC KEY-----
EOD;
	
	const DCV_PRIVATE_KEY = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAn4gR5IHyPXQmZQRkprxWkcmot0tGM1BupXKE6YIm5T3WJ6AJ
GrmFbma5NB0xzGVQduicl+cDvlaum5bTBYtvq1sHXj+Y/da2iUHaoT2M8gVrA/6V
ZFKc2tB8Ng88Y5ZZCD+jlXRmi+mcKHIgSr8U8FlRrnaoIfCql+S7hL7Wczo611Ew
i5+Jtjgiz5bCDIdoNTj6t4F1j9TCpa5ljYLozPLURwslDa7jR+bqXwmE84dr0BNn
ks4gteQgH2CL9VqNTLFcbK0Leq51KNEsEFGGPHLt2PuyLDOFtPhDpEHlgHdDet3d
TPnjhfp17tWfNXSSq03yzQ2byLSQUEHi9aMXOvtgs2A77u3SqAEmR23Pv6Y5GS09
yJQmajSQMlAaantoEDWc9WUF0HU3WjE+Si2EgPsKNIqFF4y82Hm1Kl+u7CjuR2jH
Vtv1VZ49EWERx16lzPARuoEyCocGva4X9VfiE4cErM+1N9LClUNAEsCC/aRYsG0o
iTddxith+mig+hySt6xSfWruz5lJZG+AoKcFBebICHw2HWgRmrnJrnPO5mamdCdZ
2OGBvUBLuO4NUltCHji2c6vThHIeGp4TMTNOf/N47UCUrau4A2YrKiHLTMGd/kPq
787oTko7+VJYKRk97KY8zAp+RtkiSbT6vKfnnkHkvDJIssbVVrKGT0k4DHMCAwEA
AQKCAgBW1qC2vC16WjFUJ5IKWNbewOC/9Y+e+xdfo/jJivlVK9XY9/o8u+Pv9qa1
wXaMBlJIh9JFtZCKGXEubGktyXrd0c0u9nt3IVt3V4uE3ZZNX7mjEP+M/pM3YOlc
G+sJYpvQK6QGBLiVTHKeaZE+XsktE9GsoBZhcznJOM8CMUdWrcm9zwoX1p4FvITm
bu/uFf8ZiYygOy4dPvk9arMI1suWVFBM47olup8pUn8dgHcIgRykxQtKFD4mBKmf
qtREmCf2KU1gMsne2/wWXdYYg+gIG59wz4FrIhiUK9fWH7Cnv0PSK9b8wcGh+nYb
rdjeJjh4WxPhnpjAJdmwdkzu3PoVWIALUPR1zBiVEgT9dwrqUIXzt43MHTF0Hstw
bFEQ8JIaoEXfr1mcRL2kqfJ8bcCD+u8MUy9LtvJPzJc6/uguY3mIpwkY2kjq3Gr4
XhxTqrSS0BqLm+TrkuatOZPWwvjGtqEtZi0GOGUeFE+kFP2ODQPzAIs3RVPRScnS
cJvMW5BP8nwiIz7T54KPLGYXKtMSGtfTqkcNrDh4HmXDJk9z1NpdApQVe85RHI4X
K0Mp3P3z+12mB9I/8+ZcdulrrgIPKA8IECc/JRGTS8aFnh+PDaanDo3+z8XBTr5R
OZCmoyY/LkxtgdVag/snBftDoHep6svqwOybcVgclE7AQ1N6MQKCAQEA69qRrPBt
Yz/k2Gd6xJAHk+5q5Cj+bQVkGHHq37k+IVkFZGuE+MdEcctE7OxQfZU9yLPpybs2
k5fsmA+OUKR3qU+uKSKf6bIBcGksFG3XuQzsvDTaHfvcxXBWsSTw9eN3cpL3q6aD
1PGimzs/Qo+yAI/rghw4xUJMVKuwkDykTBd0O0uBfypjMjLT71+FZ0uaSk50EW7Z
ONzHM2uUqMZPZbL8mVZjqmg3rf28cCsxSDw9pEie8IKmJmFXJhWBjboRO+ztWz9n
1Nw4NwCR4nTbrkV2iTL25b2jNf4cHCwbaQDh9U4YLARd5MJTKXY3rddNByDAvVEZ
6ZbLmgtiPQRepwKCAQEArSiOgvGrltd45YiYTdn3dGptFkcnZFRwNrr7XBqF03Es
hxqBMRbztJYU9D24wZhrV/m5x6p/mzHRlO1UmSS9LjM6ntuOHml3/hRGI+O4BlRG
mcDrpp68/gmBhgFLjKq83VF1P+IfUkFAFyoJ3i81WMPusZ+ZBVawSzNowb4SkIml
2UfwBweOA1n4xtGy6pIwbGVEi+I8BfFarFgl2THhTYTBVSRspdQJRnkwOL8WYXzZ
lLClDrqS5xj5R8ELO3J+3MLAmBviKTwm09+YMyE+ebUuzxOsSUAho5C4Mrp7r173
zLxFOeS7Fvr5TowoWYZ2NjtAjoEaUkLKyaJRkeFJVQKCAQBicAonDP4K04sXCzGB
Qr/27SZt7fIq3HonfbxS/gTBdF88x2drUffuKrGs1QDPOW/dCcJE8T9ZIKqd2LhE
TjCnWNtOzeXCawoQucStDh9gW9Wew8xZgVpmiXmVW966MeMTzeVTHh+dy/BbK8I+
bK0VcPwRhOfo5L/pAvOhb73/CDb53IzC35UhSXeNS+fo8fQGmXH2KLa+9d6qRnd4
bbSLtMWa1iSo5fB/TQzOICjkhkgP/kNgDJd+I1h1S7qTxZoV0dBLKk6S2AsaYcGB
q9ZYiSNtJflIb/rjuRhQZdRW4ghtEtgh3vy1UqnU/0ND6j17AR+QpH3VMZvuwluR
Lfa3AoIBAQCmE9UDwFjL1yQsOmOhn6HYNU8lY99jOh1aUOJOx119m/mSJxisBZB+
rsewLharD8481daaq2uyZQjXpv1R2MvdqOLbhEb94jhlJ/rR51IUN/Zy93bNG0i2
+lUROuLEKunz93HWcMGLueuHjBlk0bB/z4R7NlCkry1tIwShdfUGTg8UpAuSImvR
yRRzVKMemlH5VPN2mujo5kEKNY0vkMi/+ckYKVRPhFvVDNz0QEpt/DoPg5D73HST
U/+bE4r7XGhSwjPIcE3tMLUo1zZ6K7NpNh6MHBlNNEZPQ7l3ZibE3/gZlKVsznWm
p9Y/wOveWbm9b/0N8MNXDJrE43sKyKH1AoIBAQDKNWct4Of7QOOyNk49DLVoAjnf
6hmy0mzmo+xpnmsitqsrTCcK2gV4mZj8mSPwwi9RaxhhCcgRekgzKiJ11iea5OQv
vST0q+8x9Q6LDE7fZQiN9fk9LOgsjAl63P8UZ0TYRjuayd4k6lE0AL3DlHfx4yMM
ln/ICGySnFT+E285agCrDGlA7UyGdM4Ba48hyQKTCB9SwKbvjdv/MNbe8mkEf8W7
Py4Ppbn/hpSlS++BcV/QGcEoS7xhyizUbzOSqtw4s9AJi2Ceyvlgp12l6AedNOZS
t3u35S1FRCpENf8rBKXgneaLQ+DgO2ZfTnj5cbL5GyAgPxwyCNYezTfuBp17
-----END RSA PRIVATE KEY-----
EOD;
    function __construct(){
        if(!isset($_SESSION['msisdn']) && !isset($_SESSION['DATA'])){ 
            $link = self::makeMsisdnUrl();
            header('Location: '.$link);exit();
        }
    }
    public static function makeMsisdnUrl($msisdn='', $service_id='GIOVANGCT_DCV_KQ1_NGAY', $command='', $content='', $price='') {
        
        $reqid = self::makeRequestID();
        $session_id_sub = session_id();
        $param = "SUB=".$service_id."&REQ=$reqid"."&SOURCE=WAP"."&SESS=$session_id_sub";
//		$infomation = " | param".$param."\n";
//		$file = '/tmp/detectmsisdn.log'; 
        echo 'Params'.$param .'</br>';
        $encrypted_param = self::encryptAndSignParam($param);
        $url = self::VT_MPS_DETECT_MSISDN_ADDR."?"
                        ."PRO=".self::VT_PROVIDER_ID
                        ."&SER=".self::VT_SERVICE_NAME_GIOVANG
                        ."&SUB=".$service_id
                        ."&".$encrypted_param;
//        echo $url;die;
//        file_put_contents($file, $infomation, FILE_APPEND | LOCK_EX);
        return $url;
    }
    public static function makeRequestID() {
        $id = Date('ymdHis');
        $id = self::VT_GIOVANG_CPCODE . $id; 
        // TODO: sua lai thanh tu dong tang 000-999 dung APC
        $idx = self::thousand_padding(round(microtime() * 1000));
        return $id.$idx;
    }
    // encrypt and sign data to form: DATA=....&SIG=...
    public static function encryptAndSignParam($input) {
        include("AES.class.php");
        $z = 'abcdefghijuklmno0123456789012345';
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $aes = new AES($z, 'CBC', $iv);
        $encrypted = $aes->encrypt();
        
        $aeskey = bin2hex($encrypted);
        // 		$aeskey = "0dded43371cd3150953b256eb3ae4bc8";
        echo "\n\ninput: " . $input.'<br/>';
        echo "\n\nAES key: " . $aeskey.'<br/>';

        //B2. Ma hoa du lieu bang AES
        $input = self::pkcs5_pad($input, 16);
        $input_encrypted = self::encrypt($input,$aeskey);
        echo "\n\ninput_encrypted: " . $input_encrypted.'<br/>';

        //B3. Input du lieu co gan key AES
        $value_with_key = 'value='.$input_encrypted.'&key='.$aeskey;
        echo("\n\nvalue Encrypt + key aes: " .$value_with_key).'<br/>';

        //B4. Ma hoa du lieu bang public key
        $data_encrypted ="";
        openssl_public_encrypt($value_with_key,$data_encrypted,self::VT_PUBLIC_KEY);
        $data_encrypted = base64_encode($data_encrypted);
        echo("\n\nvalue Encrypt with public key: " .$data_encrypted).'<br/>';

        //B5. Tao chu ky
        $signature ='';
        openssl_sign($data_encrypted, $signature, self::DCV_PRIVATE_KEY, OPENSSL_ALGO_SHA1);
        $data_encrypted = urlencode($data_encrypted);
        $signature = base64_encode($signature);
        $signature = urlencode($signature);
        echo("\n\nsignature: " . $signature).'<br/>';

        //B6. Tao param
        $res = "DATA=$data_encrypted&SIG=$signature";
        return $res;
    }
    function getContentFile($link1)
    {
      $result='';
      $file1='';
      if($file1=fopen($link1,'r'))
      {
        while(!feof($file1)) 
        {
          $result.= fread($file1, 8192);
        }
        fclose($file1);
      }
      
      return $result;
    }
    
    function msgArray() 
    {
      return array(
        'DK1:DK#MB'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Bac. Phi DV la 2.000d/ngay, gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MB gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [mat khau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430(1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Nhan KQXS cua Gio Vang Chot So Mien Bac (2.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>'QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Bac. Ngay dau tien phi 0d, tu ngay thu 2 phi DV la 2.000d/ngay va duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MB gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: 5169. De su dung DV, truy cap http://giovangchotso.com.vn. LH 19005169 (1000d/p). ',
                      'money_zero'=>'Tai khoan cua QK khong du de su dung DV Nhan KQXS cua Gio Vang Chot So Mien Bac (2.000d/ngay), vui long nap them tien va thu lai sau. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        'DK2:DK#MT'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Trung. Phi DV la 2.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MT gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Nhan KQXS cua Gio Vang Chot So Mien Trung (2.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>' QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Trung. Phi DV la 2.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MT gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'money_zero'=>'Tai khoan cua QK khong du de su dung DV Nhan KQXS cua Gio Vang Chot So khu vuc Mien Trung (2.000d/ngay), vui long nap them tien va thu lai sau. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        'DK3:DK#MN'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Nam. Phi DV la 2.000d/ngay va duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MN gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Nhan KQXS Gio Vang Chot So Mien Nam (2.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>'QK da dang ky thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Nam. Phi DV la 2.000d/ngay va duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY MN gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p). ',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'money_zero'=>'Tai khoan cua QK khong du de su dung DV Nhan KQXS cua Gio Vang Chot So Mien Nam (2.000d/ngay), vui long nap them tien va thu lai sau. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#MB'=>array(
                      'success_1'=>'QK da huy thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Bac. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Nhan KQXS cua Gio Vang Chot So Mien Bac. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Nhan KQXS cua Gio Vang Chot So Mien Bac. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#MT'=>array(
                      'success_1'=>'QK da huy thanh cong DV Nhan KQXS Gio Vang Chot So Mien Trung. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Nhan KQXS Gio Vang Chot So Mien Trung. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Nhan KQXS cua Gio Vang Chot So Mien Trung. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#MN'=>array(
                      'success_1'=>'QK da huy thanh cong DV Nhan KQXS Gio Vang Chot So Mien Nam. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Nhan KQXS Gio Vang Chot So Mien Nam. De dang ky nhan KQXS Mien Bac soan DK MB (2.000d/ngay), Mien Trung soan DK MT (2.000d/ngay), Mien Nam soan DK MN (2.000d/ngay) gui 5169. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Nhan KQXS cua Gio Vang Chot So Mien Nam. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        'DK4:DK#TTB'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Bac. Phi DV la 3.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY TTB gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Bac (3.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Bac. Phi DV la 3.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY TTB gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p). ',
                      'money_zero'=>'Tai khoan cua QK khong du de su dung DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Bac (3.000d/ngay), vui long nap them tien va thu lai sau. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        'DK5:DK#TTT'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Trung. Phi DV la 3.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY TTT gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Trung (3.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Trung. Phi DV la 3.000d/ngay, duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung DV. De huy, soan HUY TTT gui 5169. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [matkhau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'money_zero'=>''
                    ),
        'DK6:DK#TTN'=>array(
                      'success_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Nam. Phi DV la 3.000d/ngay va duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung. De huy, soan HUY TTN gui 5169. Chi tiet soan HD TTN gui 5169 hoac LH 19006430 (1.000d/p)',
                      'success_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [mat khau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK dang su dung DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Nam (3.000d/ngay). Truy cap http://giovangchotso.vn de su dung DV. Chi tiet soan HD gui 5169 hoac LH 19006430 (1.000d/p).',
                      'mobile_unregister_1'=>'QK da dang ky thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Nam. Phi DV la 3.000d/ngay va duoc gia han hang ngay. Truy cap http://giovangchotso.vn de su dung. De huy, soan HUY TTN gui 5169. Chi tiet soan HD TTN gui 5169 hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_2'=>'Mat khau dang nhap DV giovangchotso cua ban la: [mat khau]. De su dung DV, truy cap http://giovangchotso.vn. LH 19006430 (1000d/p).',
                      'money_zero'=>'Tai khoan cua QK khong du de su dung DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Nam (3.000d/ngay), vui long nap them tien va thu lai sau. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#TTB'=>array(
                      'success_1'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Bac. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay) gui 5169. Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Bac. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay) gui 5169. Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Bac. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#TTT'=>array(
                      'success_1'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Trung. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay). Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Trung. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay). Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Trung. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HUY#TTN'=>array(
                      'success_1'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Nam. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay) gui 5169. Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da huy thanh cong DV Tuong thuat truc tiep KQXS Gio Vang Chot So Mien Nam. De dang ky tuong thuat truc tiep KQXS Mien Bac soan DK TTB (3.000d/ngay), Mien Trung soan DK TTT (3.000d/ngay), Mien Nam soan DK TTN (3.000d/ngay) gui 5169. Truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Yeu cau huy khong thanh cong. QK chua dang ky DV Tuong thuat truc tiep KQXS cua Gio Vang Chot So Mien Nam. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    ),
        '0:HD#3M'=>array(
                      'success_1'=>'Chao mung QK den voi DV Gio Vang Chot So. De dang ky Nhan KQXS Mien Bac soan DK MB, soan HUY MB de huy; Mien Trung soan DK MT, soan HUY MT de huy; Mien Nam soan DK MN, soan HUY MN de huy. Cuoc phi 2.000d/ngay. De dang ky Tuong thuat truc tiep KQXS Mien Bac soan DK TTB, soan HUY TTB de huy; Mien Trung soan DK TTT, soan HUY TTT de huy; Mien Nam soan DK TTN, soan HUY TTN de huy. Cuoc phi 3.000d/ngay. De kiem tra cac goi cuoc dang su dung, soan KT, de lay mat khau, soan MK. Tat ca gui 5169. Chi tiet truy cap http://giovangchotso.vn. LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'Chao mung QK den voi DV Gio Vang Chot So. De dang ky Nhan KQXS Mien Bac soan DK MB, soan HUY MB de huy; Mien Trung soan DK MT, soan HUY MT de huy; Mien Nam soan DK MN, soan HUY MN de huy. Cuoc phi 2.000d/ngay. De dang ky Tuong thuat truc tiep KQXS Mien Bac soan DK TTB, soan HUY TTB de huy; Mien Trung soan DK TTT, soan HUY TTT de huy; Mien Nam soan DK TTN, soan HUY TTN de huy. Cuoc phi 3.000d/ngay. De kiem tra cac goi cuoc dang su dung, soan KT, de lay mat khau, soan MK. Tat ca gui 5169. Chi tiet truy cap http://giovangchotso.vn. LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'Chao mung QK den voi DV Gio Vang Chot So. De dang ky Nhan KQXS Mien Bac soan DK MB, soan HUY MB de huy; Mien Trung soan DK MT, soan HUY MT de huy; Mien Nam soan DK MN, soan HUY MN de huy. Cuoc phi 2.000d/ngay. De dang ky Tuong thuat truc tiep KQXS Mien Bac soan DK TTB, soan HUY TTB de huy; Mien Trung soan DK TTT, soan HUY TTT de huy; Mien Nam soan DK TTN, soan HUY TTN de huy. Cuoc phi 3.000d/ngay. De kiem tra cac goi cuoc dang su dung, soan KT, de lay mat khau, soan MK. Tat ca gui 5169. Chi tiet truy cap http://giovangchotso.vn. LH 19006430 (1.000d/p)'),
        '0:ERROR#SYNTAX'=>array(
                      'success_1'=>'QK da soan sai cu phap. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'system_error'=>'Hien tai he thong dang ban, QK vui long thu lai sau. Chi tiet tai http://giovangchotso.vn hoac LH 19006430 (1.000d/p).',
                      'mobile_existing'=>'QK da soan sai cu phap. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)',
                      'mobile_unregister_1'=>'QK da soan sai cu phap. Chi tiet soan HD gui 5169, truy cap http://giovangchotso.vn hoac LH 19006430 (1.000d/p)'
                    )
      
      );
    }
    
    function checkParamCommand($params, $command)
    {
      $DANGKY = array("DK1","DK MB", "DK2","DK MT", "DK3","DK MN", "DK4","DK TTB", "DK5","DK TTT", "DK6","DK TTN");
      $HUY = array("HUY MB", "HUY MT", "HUY MN", "HUY TTB", "HUY TTT", "HUY TTN");
      
      switch($params)
      {
        case "0":
          if(in_array($command, $DANGKY))
          {
            return true;
          }
          else
          {
            return false;
          }
        break;
        
        case "1":
          if(in_array($command, $HUY))
          {
            return true;
          }
          else
          {
            return false;
          }
        break;
        
        default:
          return false;
        break;
      }
    }	
  
    function get_thu($ngay)
    {
      switch(date("D",strtotime($ngay)))
      {
        case 'Sun':
        $thu='Chủ Nhật';break;
        case 'Mon':
        $thu='Thứ Hai';
        break;
        case 'Tue':
        $thu='Thứ Ba';
        break;
        case 'Wed':
        $thu='Thứ Tư';
        break;
        case 'Thu':
        $thu='Thứ Năm';
        break;
        case 'Fri':
        $thu='Thứ Sáu';
        break;
        case 'Sat':
        $thu='Thứ 7';
        break;
      }
      
      return $thu;
    }
    
    function random_number($so)
    {
      $sonhan=0;$tu=0;
      switch($so)
      {
        case 5: $sonhan=99999;break;
        case 4: $sonhan=9999;break;
        case 3: $sonhan=999;break;
        case 2: $sonhan=99;break;
      }
      
      switch($so)
      {
        case 5: $tu=00000;break;
        case 4: $tu=0000;break;
        case 3: $tu=000;break;
        case 2: $tu=00;break;
      }
      
      $kq=rand($tu,$sonhan);
      if(count($kq)==$so)
      {
        return $kq;
      }
      else
      {
        $k="";
        for($i=0;$i<$so-strlen($kq);$i++)
        {
          $k.="0";
        }
        
        return $k.$kq;
      }
      
    }
    
    function checkExistValueField($value, $field, $table, $type)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
    
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      
      if($type == 'like')
      {
        $query = mysql_query("SELECT ".$field." FROM ".$table." WHERE ".$field." LIKE '%".$value."%'") or die(mysql_error());
        $row = mysql_fetch_array($query);
        if(isset($row[$field]) && !empty($row[$field]))
        {
          return 'true';
        }
      }
      else
      {
        $query = mysql_query("SELECT ".$field." FROM ".$table." WHERE ".$field."='".$value."'") or die(mysql_error());
        $row = mysql_fetch_array($query);
        if(isset($row[$field]) && !empty($row[$field]))
        {
          return 'true';
        }
      }
      
      return 'false';
    }
    
    function checkExistValueField2($fields, $field1, $field2, $cond1, $cond2, $table, $type)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
    
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      
      if($type == 'like')
      {
        $query = mysql_query("SELECT ".$fields." FROM ".$table." WHERE ".$field1." LIKE '%".$cond1."%' AND ".$field2." LIKE %'".$cond2."'%") or die(mysql_error());
        $row = mysql_fetch_array($query);
        if(isset($row[$fields]) && !empty($row[$fields]))
        {
          return 'true';
        }
      }
      else
      {
        $query = mysql_query("SELECT ".$fields." FROM ".$table." WHERE ".$field1."='".$cond1."' AND ".$field2."='".$cond2."'") or die(mysql_error());
        $row = mysql_fetch_array($query);
        if(isset($row[$fields]) && !empty($row[$fields]))
        {
          return 'true';
        }
      }
      
      return 'false';
    }
    
    function getAllValueOfField($table_name, $field, $fieldcond, $value, $type)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
      if($type == 'like')
      {
        $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."  LIKE '%".$value."%'") or die(mysql_error());
        while ($row = mysql_fetch_array($query)) 
        {
          $rows[] = $row[$field];
        }  
      }
      else
      {
        $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."='".$value."'") or die(mysql_error());
        while ($row = mysql_fetch_array($query)) 
        {
          $rows[] = $row[$field];
        } 
      }
      
      return $rows;
    }
    
    function getValueOfField($table_name, $field, $fieldcond, $value)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."='".$value."'") or die(mysql_error());
      $row = mysql_fetch_array($query);
      
      return $row[$field];
    }
    
    function getValueOfFieldLike($table_name, $field, $fieldcond, $value)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond." LIKE '%".$value."%'") or die(mysql_error());
      $row = mysql_fetch_array($query);
      
      return $row[$field];
    }
    
    function getInsertNewValues($table_name, $fields, $values) 
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      } 
      $conn->set_charset("utf8");
      $sql = "INSERT INTO ".$table_name."(".$fields.") VALUES (".$values.")";
      $conn->query($sql);

      $conn->close();
    }
    
    function getInsertValues($table_name, $value) 
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      } 
      $date = current_time('mysql');  
      $date1 = date('Y-m-d');
      $sql = "INSERT INTO ".$table_name."(time, date, ketqua) VALUES ('".$date."','".$date1."','".$value."')";
      $conn->query($sql);

      $conn->close();
    }
    
    function getUpdateValue($table_name, $value, $id)
    { 
      $conn = new Connection();
      $str_conn = $conn->_Connection();

      $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      } 
      $date = current_time('mysql');  
      $date1 = date('Y-m-d');
      $sql = "UPDATE ".$table_name." SET time='".$date."', date='".$date1."', ketqua='".$value."' WHERE id=".$id;
      $conn->query($sql);

      $conn->close();
    }
    function getUpdateServiceMapping($values, $id,$date){
        $conn = new Connection();
      $str_conn = $conn->_Connection();

      $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      } 
      $sql = "UPDATE service_subscriber_mapping SET is_active='".$values."', updated_at='".$date."' WHERE id=".$id;
      $conn->query($sql);

      $conn->close();
    }
    // $params is array: $params = array("$field1"=>"$value1", "$field2"=>"$value2", ...);
    // $where is string: $where = "$field = $value";
    function getUpdateValueNew($table_name, $params, $where)
    { 
      $append_fields = ''; $i = 0;
      foreach($params as $field=>$value)
      {
        if($field == 'status_section')
        {
          if($i == 0)
          {
            $append_fields .= $field."=".$value."";
          }
          else
          {
            $append_fields .= ", ".$field."=".$value."";
          }
        }
        else
        {
          if($i == 0)
          {
            $append_fields .= $field."='".$value."'";
          }
          else
          {
            $append_fields .= ", ".$field."='".$value."'";
          }
        }
        $i++;
      }
    
      $conn = new Connection();
      $str_conn = $conn->_Connection();

      $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      }  
      $conn->set_charset("utf8");
      $sql = "UPDATE ".$table_name." SET ".$append_fields." WHERE ".$where;
      $conn->query($sql);

      $conn->close();
    }
    
    function getAllResult($table, $field, $limit)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." ORDER BY ".$field." DESC LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = array("id"=>$row["id"], "province_id"=>$row["province_id"], "content_result"=>$row["content_result"], "status_result"=>$row["status_result"], "created_at"=>$row["created_at"], "updated_at"=>$row["updated_at"]);
      } 
      
      return $rows;
    }
    function getAllResultValues($table, $fieldcond, $values, $field, $desc, $limit)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." WHERE ".$fieldcond." = ".$values." ORDER BY ".$field." ".$desc." LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = $row;
      } 
      return $rows;
    }
    function getAllResultValuesRecord($table, $fieldcond1, $values1, $fieldcond2, $values2, $field, $desc, $limit)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." WHERE ".$fieldcond1." = ".$values1." AND ".$fieldcond2." = ".$values2." ORDER BY ".$field." ".$desc." LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = $row;
      } 
      return $rows;
    }
    function getAllResultValuesRecordService($table, $fieldcond1, $values1, $fieldcond2, $values2, $fieldcond3, $values3, $field, $desc, $limit){
        $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." WHERE ".$fieldcond1." = ".$values1." AND ".$fieldcond2." = ".$values2." AND ".$fieldcond3." = ".$values3." ORDER BY ".$field." ".$desc." LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = $row;
      } 
      return $rows;
    }
    function getResultValues($table, $fieldcond, $values, $field, $limit)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
    
      $query = mysql_query("SELECT * FROM ".$table." WHERE ".$fieldcond." = ".$values." ORDER BY ".$field." ASC LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_assoc($query)) 
      {
        $rows= $row;
      } 
      return $rows;
    }
    function getResultValuesService($table, $fieldcond1, $values1, $fieldcond2, $values2, $field, $limit)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
    
      $query = mysql_query("SELECT * FROM ".$table." WHERE ".$fieldcond1." = ".$values1." and ".$fieldcond2." = ".$values2." and is_active = 1 ORDER BY ".$field." ASC LIMIT ".$limit) or die(mysql_error());
      while ($row = mysql_fetch_assoc($query)) 
      {
        $rows= $row;
      } 
      return $rows;
    }
    
    function checkStatusCustomer()
    {
      
      
    }
    
    function getPagerHtml($table, $field)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      $html = ' <button type="button" class="btn btn-default btn-first btn-backward-first">
                  <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-default btn-backward" style="border-radius: 0px; -o-border-radius: 0px; -ms-border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;">
                  <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                </button>
                </div>
                <div class="btn-group-carousel">
                <div class="btn-group btn-group-carousel-inner" role="group" aria-label="First group">
                ';
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." ORDER BY ".$field." DESC") or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = array("id"=>$row["id"], "province_id"=>$row["province_id"]);
        $i++;
      }
      
      if(count($rows)%20 == 0)
      {  
        $numPagers = count($rows)/20;
      }
      else
      {
        $numPagers = count($rows)/20 + 1;
      }
      
      if($numPagers >= 1)
      {
        for($i = 1; $i <= $numPagers; $i++)
        {
          $html .= '<button type="button" class="btn btn-default btn-number btn-'.$i.'">'.$i.'</button>';
        }
      }
      
      $html .= '</div></div>
                <div class="btn-group" role="group" aria-label="First group" style="margin-left: 0px;">
                <button type="button" class="btn btn-default btn-forward" style="border-radius: 0px; -o-border-radius: 0px; -ms-border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;">
                  <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-default btn-last btn-forward-last">
                  <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
                </button>';
                
      return $html;
    }
    
    function getMessageToolbar($table, $field, $number)
    {
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      mysql_query("SHOW FULL PROCESSLIST");
      $rows = array();
    
      $query = mysql_query("SELECT * FROM ".$table." ORDER BY ".$field." DESC") or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = array("id"=>$row["id"], "province_id"=>$row["province_id"]);
        $i++;
      } 
      
      return 'Hiển thị '.$number.' của '.count($rows).' bản ghi tìm được.';
    }
    
    
    function createTable($table_name, $sql) 
    {
      // global $wpdb;
      // if(mysql_num_rows(mysql_query("SHOW TABLES LIKE '".$table_name."'")) == 1) 
      // {
        
      // }
      // else 
      // { 
        // $charset_collate = $wpdb->get_charset_collate();
        // $sql = "CREATE TABLE ".$table_name." (".$sql.") ".$charset_collate.";";
        // require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        // dbDelta( $sql );
      // }
    }
    static function pkcs5_pad ($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }
    static function encrypt($encrypt, $key){
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, pack("H*", $key), $encrypt, MCRYPT_MODE_ECB, $iv));
        return $encrypted;
    }
	
    static function decrypt($decrypt, $key){
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128,  pack("H*", $key), base64_decode($decrypt), MCRYPT_MODE_ECB, $iv);
        return $decrypted;
    }
    public static function validatorMobile($mobileNumber, $typeFormat = 0){
        $valid_number = $mobileNumber;
        if(preg_match('/^(84|0|)(96|97|98|162|163|164|165|166|167|168|169)\d{7}$/', $mobileNumber, $matches)){
                if($typeFormat == 0){
                        if ($matches[1] == '0' || $matches[1] == ''){
                                $valid_number = preg_replace('/^(0|)/', '84', $mobileNumber);
                        }else{
                                $valid_number = $mobileNumber;
                        }
                }else if($typeFormat == 1){
                        if ($matches[1] == '84' || $matches[1] == ''){
                                $valid_number = preg_replace('/^(84|)/', '0', $mobileNumber);
                        }else{
                                $valid_number = $mobileNumber;
                        }
                }else if ($typeFormat == 2){
                        if ($matches[1] == '84' || $matches[1] == '0'){
                                $valid_number = preg_replace('/^(84|0)/', '', $mobileNumber);
                        }else{
                                $valid_number = $mobileNumber;
                        }
                }
        }
        return $valid_number;
    }
    static public function thousand_padding($number) {
        if ($number < 10) {
                return "00" . $number;
        }
        else if ($number < 100) {
                return "0" . $number;
        }
        else {
                return $number;
        }
    }
    public static function decryptParam($data, $signature) {
		$data = str_replace(' ', '+', $data); // cai nay do dau "+" trong url bi decode thanh " "
// 		echo "\n\nEncrypted data: $data";
// 		echo "\n\nSignature: $signature";
	
		//B7. Xac thuc chu ky
		$verify = openssl_verify ($data , base64_decode(urldecode($signature)) , self::VT_PUBLIC_KEY, OPENSSL_ALGO_SHA1);
// 		echo("\n\nVerify: ". ($verify==0?"OK":"FAILED"));
	
		//B8. giai ma du lieu = viettel priv1
		$data_decrypted = "";
		openssl_private_decrypt(base64_decode($data), $data_decrypted, self::DCV_PRIVATE_KEY);
// 		echo("\n\nvalue Decrypt with private key: " . $data_decrypted);
	
		//B9. tach value va key
		parse_str($data_decrypted, $param);
		if (isset($param["VALUE"])) {
			$value = $param["VALUE"];
		}
		else if (isset($param["value"])) {
			$value = $param["value"];
		} 
		$value = str_replace(' ', '+', $value);
		
		if (isset($param["KEY"])) {
			$key = $param["KEY"];
		}
		else if (isset($param["key"])) {
			$key = $param["key"];
		}
// 		echo("\n\nvalue: " . $value);
// 		echo("\n\nkey: " . $key);
	
		//B10. Giai ma du lieu bang AES
		$value_decrypt = self::decrypt($value,$key);
		return $value_decrypt;
	}
        public static function redirectChargingUrl($reqid, $msisdn, $service_id, $command, $cost, $subCp = '', $content = ""){
            $ser = self::VT_SERVICE_NAME_GIOVANG;
            switch (intval($service_id)){
                    case 4:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_KQ1;
                            break;
                    case 5:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_KQ2;
                            break;
                    case 6:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_KQ3;
                            break;
                    case 7:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_TT1;
//				$ser = self::GIOVANGCT_DCV_TT1_NGAY;
                            break;
                    case 8:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_TT2;
//				$ser = self::GIOVANGCT_DCV_TT2_NGAY;
                            break;
                    case 9:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_TT3;
//				$ser = self::GIOVANGCT_DCV_TT3_NGAY;
                            break;
                    default:
                            $service_name = self::VT_SERVICE_NAME_GIOVANGCT_KQ1;
                            break;
            }
            $command = 'CANCEL';
            $infomation = '----------------------------------------------------------------------------------------------'.PHP_EOL;
            $infomation .= date('Y-m-d h:i:s').' | ';
            $infomation .= 'service_name: '.$service_name. ' | msisdn: '.$msisdn." | command: $command | service_id: $service_id " ." | service_charge: ".$ser. PHP_EOL;
            file_put_contents($file, $infomation, FILE_APPEND | LOCK_EX);
            $url = self::makeChargingUrl($reqid, $msisdn, $service_name, $command, $content, $cost, $subCp, 'WAP', $ser);
            return $url;
        }
        public static function makeChargingUrl($reqid, $msisdn, $service_name, $command, $content, $price, $subCp, $source='WAP', $ser=self::VT_SERVICE_NAME_GIOVANG) {
//		$param = "SUB=".$service_name."&CATE=".self::VT_CATEGORY_ITEM.
//			"&SUB_CP=$subCp&CONT=$content"."&PRICE=$price"."&REQ=$reqid"."&MOBILE=$msisdn"."&SOURCE=".$source;
            $param = "SUB=".$service_name."&CATE=&ITEM="."&SUB_CP=$subCp&CONT=$content"."&PRICE=$price"."&REQ=$reqid"."&MOBILE=$msisdn"."&SOURCE=".$source;
//            $infomation = " Param: ".$param.PHP_EOL;
//            $file = '/tmp/orderredirectcharging.log_'.date('Y-m-d');
            $encrypted_param = self::encryptAndSignParam($param);

            $url = self::VT_MPS_REDIRECT_ADDR_TEST."?"
                            ."PRO=".self::VT_PROVIDER_ID
                            ."&CMD=$command"
                            ."&SER=".$ser
                            ."&SUB=".$service_name
                            ."&".$encrypted_param;
//            $infomation .= "Redirect url: ".$url.PHP_EOL;		
//            file_put_contents($file, $infomation, FILE_APPEND | LOCK_EX);
            return $url;
        }
    public static function getErrorCodeName($errorCode){
        switch ($errorCode){
            case self::VT_ERR_NONE :
                            $errorName = "Thông báo: Quý khách đã đăng ký thành công dịch vụ Xổ số trực tuyến của Viettel!";
                    break;
            case self::VT_ERR_INSUFFICENT_BALANCE:
                            $errorName = "Thông báo: Xin lỗi, Quý khách không đủ tiền để thực hiện giao dịch. Vui lòng nạp thêm tiền hoặc chọn gói khác!";
                    break;
            case self::VT_ERR_ALREADY_REGISTERED:
                            $errorName = "Thông báo: Thuê bao đã đăng ký dịch vụ rồi, Quý khách vui lòng hủy dịch vụ và chọn gói cước khác!";
                    break;
            case self::VT_ERR_BLOCKED_ACCOUNT:
                            $errorName = "Thông báo: Đăng ký không thành công, thuê bao của Quý khách đang bị khóa!";
                    break;
            case self::VT_ERR_ALREADY_CANCELED:
                            $errorName = "Thông báo: Thuê bao của Quý khách đã hủy dịch vụ rồi!";
                    break;
            case self::VT_ERR_NOT_YET_REGISTERED:
                            $errorName = "Thông báo: Thuê bao của Quý khách chưa đăng ký dịch vụ!";
                    break;
            case self::VT_ERR_STILL_IN_CHARGING_PERIOD:
                            $errorName = "Thông báo: Quý khách đã hủy thành công gói dịch vụ Xổ số trực tuyến. Cảm ơn Quý khách đã sử dụng dịch vụ của Viettel!";
                    break;
            default:
                            $errorName = "Thông báo: Hệ thống đang bận, Quý khách vui lòng thử lại sau!";
                    break;
        }
        return $errorName;
    }
}
