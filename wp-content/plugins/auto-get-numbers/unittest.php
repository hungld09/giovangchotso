<?php 
  require_once 'auto-get-numbers.php';
  require_once 'connection.php';
  require_once 'extends.php';
  
  class unitTest
  {
    function __construct(){ }
    
    function autoLoadMTMNV() 
    {
      $array = array( "dacbiet"   =>  "",
                      "giainhat"  =>  "78813",
                      "giainhi"   =>  "",
                      "giaiba"    =>  "58813-50026-37211-71416-11876-88535",
                      "giaitu"    =>  "",
                      "giainam"   =>  "0090-7977-2878-0341-3874-7229",
                      "giaisau"   =>  "",
                      "giaibay"   =>  "38-18-47-78",
                      "giaitam"   =>  "",
                      "title1"    =>  "Lototructiep",
                      "list1"     =>  "11-13-16-18-19-26-27-29-35-35-38-41-47-53-61-62-62-68-73-74-74-76-77-78-78-90-91",
                      "title2"    =>  "Dauloto",
                      "list2"     =>  array("", "11-13-16-18-19", "", "35-35-38", "", "53", "", "73-74-74-76-77-78-78", "", "90-91"),
                      "title3"    =>  "Ditloto",
                      "list3"     =>  array("90", "", "62-62", "", "74-74", "", "16-26-76", "", "18-38-68-78-78", "")
                    );
                    
      // global $wpob;
      // $wpob = new Auto_Get_Number();         
      // $array = unserialize($wpob->getXsMB());              
      
      $result = ' ['; 
      foreach($array as $key=>$value)
      {
        switch($key)
        {
          case 'dacbiet':
            $result .= '"'.$key.':'.$value.'"';  
          break;
          
          case 'giainhat':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainhi':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaiba':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitu':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaisau':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaibay':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'list1':
            $result .= ',"Lototructiep:'.$value.'"';
          break;
          
          case 'list2':
            if(is_array($value)): 
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Dauloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
          
          case 'list3':
            if(is_array($value)):
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Ditloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
        
        }
      }
      $result .= '];';
      
      return $result;
    }
    
    function autoLoadMTMN($type) 
    {
      // global $wpob;
      // $wpob = new Auto_Get_Number();
      // $extends = new functionExtends();
      
      /* 
      $conn = new Connection();
      $str_conn = $conn->_Connection();
      
      // if(date("H") >= "16" && date("i") >= "10" && date("H") <= "22")
      // {
        // $wpob->updateXoSo($wpob);
      // }
      
      $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      mysql_select_db($str_conn[3],$con);
      mysql_query("SET NAMES 'utf8'");
      $result = mysql_query("SHOW FULL PROCESSLIST");
      while ($row=mysql_fetch_array($result)) 
      {
        $process_id=$row["Id"];
        if ($row["Time"] > 20 ) 
        {
          $sql="KILL $process_id";
          mysql_query($sql);
        }
      }
      */
      
      // if($type == "mientrung")
      // {
        // /* $query = mysql_query("SELECT ketqua FROM wp_kqxsmt ORDER BY id DESC LIMIT 0 , 1") or die(mysql_error());
        // $row = mysql_fetch_array($query); //serialize and unserialize
        // $array = unserialize($row['ketqua']); */
        
        // $array = unserialize($wpob->getXsMT());
      // }
      // else if($type == "miennam")
      // {
        // /* $query = mysql_query("SELECT ketqua FROM wp_kqxsmn ORDER BY id DESC LIMIT 0 , 1") or die(mysql_error());
        // $row = mysql_fetch_array($query); //serialize and unserialize
        // $array = unserialize($row['ketqua']); */
        
        // $array = unserialize($wpob->getXsMN());
      // }
      
      $array = array( "dacbiet"   =>  "65874",
                      "giainhat"  =>  "41827",
                      "giainhi"   =>  "27335-44762",
                      "giaiba"    =>  "78813-50026-37211-71416-11876-88535",
                      "giaitu"    =>  "8368-1061-1362-9253",
                      "giainam"   =>  "0090-7977-2878-0341-3874-7229",
                      "giaisau"   =>  "719-491-273",
                      "giaibay"   =>  "38-18-47-78",
                      "giaitam"   =>  "38",
                      "title1"    =>  "Lototructiep",
                      "list1"     =>  "11-13-16-18-19-26-27-29-35-35-38-41-47-53-61-62-62-68-73-74-74-76-77-78-78-90-91",
                      "title2"    =>  "Dauloto",
                      "list2"     =>  array("68", "11-13-16-18-19", "26-27-29", "35-35-38", "41-47", "53", "61-62-62-68", "73-74-74-76-77-78-78", "68-78-78", "90-91"),
                      "title3"    =>  "Ditloto",
                      "list3"     =>  array("90", "11-41-61-91", "62-62", "13-53-73", "74-74", "35-35", "16-26-76", "27-47-77", "18-38-68-78-78", "19-29")
                    );
                    
      $result = ' ['; 
      foreach($array as $key=>$value)
      {
        switch($key)
        {
          case 'dacbiet':
            $result .= '"'.$key.':'.$value.'"';  
          break;
          
          case 'giainhat':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainhi':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaiba':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitu':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaisau':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaibay':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'list1':
            $result .= ',"Lototructiep:'.$value.'"';
          break;
          
          case 'list2':
            if(is_array($value)): 
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Dauloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
          
          case 'list3':
            if(is_array($value)):
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Ditloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
        
        }
      }
      $result .= '];';
      
      return $result;
    }
    
    function autoLoadMBV() 
    {
      $array = array( "dacbiet"   =>  "",
                      "giainhat"  =>  "78813",
                      "giainhi"   =>  "",
                      "giaiba"    =>  "58813-50026-37211-71416-11876-88535",
                      "giaitu"    =>  "",
                      "giainam"   =>  "0090-7977-2878-0341-3874-7229",
                      "giaisau"   =>  "",
                      "giaibay"   =>  "38-18-47-78",
                      "giaitam"   =>  "",
                      "title1"    =>  "Lototructiep",
                      "list1"     =>  "11-13-16-18-19-26-27-29-35-35-38-41-47-53-61-62-62-68-73-74-74-76-77-78-78-90-91",
                      "title2"    =>  "Dauloto",
                      "list2"     =>  array("", "11-13-16-18-19", "", "35-35-38", "", "53", "", "73-74-74-76-77-78-78", "", "90-91"),
                      "title3"    =>  "Ditloto",
                      "list3"     =>  array("90", "", "62-62", "", "74-74", "", "16-26-76", "", "18-38-68-78-78", "")
                    );
      
      // global $wpob;
      // $wpob = new Auto_Get_Number();            
      // $array = unserialize($wpob->getXsMB());              
      
      $result = ' ['; 
      foreach($array as $key=>$value)
      {
        switch($key)
        {
          case 'dacbiet':
            $result .= '"'.$key.':'.$value.'"';  
          break;
          
          case 'giainhat':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainhi':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaiba':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitu':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaisau':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaibay':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'list1':
            $result .= ',"Lototructiep:'.$value.'"';
          break;
          
          case 'list2':
            if(is_array($value)): 
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Dauloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
          
          case 'list3':
            if(is_array($value)):
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Ditloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
        }
      }
      $result .= '];';
      
      return $result;
    }
    
    function autoLoadMB() 
    {
      // global $wpob;
      // $wpob = new Auto_Get_Number();
      // $conn = new Connection();
      // $str_conn = $conn->_Connection();
      
      // if(date("H") >= "16" && date("i") >= "10" && date("H") <= "22"){
        // $wpob->updateXoSo($wpob);
      // }
      
      // $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
      // mysql_select_db($str_conn[3],$con);
      // mysql_query("SET NAMES 'utf8'");
      // $result = mysql_query("SHOW FULL PROCESSLIST");
      // while ($row=mysql_fetch_array($result)) {
      // $process_id=$row["Id"];
      // if ($row["Time"] > 20 ) {
          // $sql="KILL $process_id";
          // mysql_query($sql);
        // }
      // }
      
      // $query = mysql_query("SELECT ketqua FROM wp_kqxsmb ORDER BY id DESC LIMIT 0 , 1") or die(mysql_error());
      // $row = mysql_fetch_array($query); //serialize and unserialize
      
      // $array = unserialize($row['ketqua']);
    
      $array = array( "dacbiet"   =>  "65874",
                      "giainhat"  =>  "41827",
                      "giainhi"   =>  "27335-44762",
                      "giaiba"    =>  "78813-50026-37211-71416-11876-88535",
                      "giaitu"    =>  "8368-1061-1362-9253",
                      "giainam"   =>  "0090-7977-2878-0341-3874-7229",
                      "giaisau"   =>  "719-491-273",
                      "giaibay"   =>  "38-18-47-78",
                      "giaitam"   =>  "38",
                      "title1"    =>  "Lototructiep",
                      "list1"     =>  "11-13-16-18-19-26-27-29-35-35-38-41-47-53-61-62-62-68-73-74-74-76-77-78-78-90-91",
                      "title2"    =>  "Dauloto",
                      "list2"     =>  array("68", "11-13-16-18-19", "26-27-29", "35-35-38", "41-47", "53", "61-62-62-68", "73-74-74-76-77-78-78", "68-78-78", "90-91"),
                      "title3"    =>  "Ditloto",
                      "list3"     =>  array("90", "11-41-61-91", "62-62", "13-53-73", "74-74", "35-35", "16-26-76", "27-47-77", "18-38-68-78-78", "19-29")
                    );   
                    
                    
      // $array = unserialize($wpob->getXsMB()); 
      
      $result = ' ['; 
      foreach($array as $key=>$value)
      {
        switch($key)
        {
          case 'dacbiet':
            $result .= '"'.$key.':'.$value.'"';  
          break;
          
          case 'giainhat':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainhi':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaiba':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaitu':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giainam':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaisau':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'giaibay':
            $result .= ',"'.$key.':'.$value.'"';
          break;
          
          case 'list1':
            $result .= ',"Lototructiep:'.$value.'"';
          break;
          
          case 'list2':
            if(is_array($value)): 
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Dauloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
          
          case 'list3':
            if(is_array($value)):
              $convert = '';
              for($i = 0; $i < count($value); $i++):
                if($i == 0):
                  $convert .= $value[$i];
                else:
                  $convert .= '#'.$value[$i];
                endif;
              endfor;
              $result .= ',"Ditloto:'.$convert.'"';
            else:
              $result .= $value;
            endif;
          break;
        }
      }
      $result .= '];';
      
      return $result;
    }
  }
