<?php 

  class functionAdmin
  {
    function __construct(){ }
    
    function __getCssJs() 
    {
      // $cssjs = '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>';
      // $cssjs .= '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>';
      $cssjs = '<link type="text/css" rel="stylesheet" href="'.plugins_url('/css/bootstrap.min.css', __FILE__).'" />';
      $cssjs .= '<link type="text/css" rel="stylesheet" href="'.plugins_url('/css/css.css', __FILE__).'" />';
      $cssjs .= '<link type="text/css" rel="stylesheet" href="'.plugins_url('/css/responsive.css', __FILE__).'" />';
      $cssjs .= "<script type='text/javascript' scr='".plugins_url('/js/jquery.min.js', __FILE__)."'></script>";
      $cssjs .= '<script type="text/javascript" scr="'.plugins_url('/js/bootstrap.min.js', __FILE__).'"></script>';
      
      $cssjs .= '
        <style type="text/css">
          body {
            background-color: transparent !important;
            font-size: 13px !important;
            font-family: "Open Sans",sans-serif !important;
          }
          
          .manage-kqxs-grid thead th, .manage-kqxs-grid tbody td {
            text-align: center;
          }
          
          .manage-kqxs-grid p.help-block {
            margin: 15px 0 10px 0;
            text-align: right;
          }
          
          .table-bordered>thead>tr>th {
            border-bottom: 0px;
          }
          
          .manage-kqxs-grid tbody td.xs-tools {
            text-align: left;
          }
          
          .manage-kqxs-grid .btn-toolbar-top {
            padding-bottom: 15px;
            padding-top: 5px;
          }
          
          .manage-kqxs-grid .table {
            margin-bottom: 15px;
          }
          
          .manage-kqxs-grid tbody td.xs-tools span.glyphicon {
            padding-right: 10px;
            padding-left: 10px;
          }
          .table-bordered>tbody>tr>th {
            font-weight: normal;
          }
          
          .table-bordered>thead>tr>th {
            background-color: #f5f5f5;
          }
          
          .h3, h3 {
            font-size: 15px !important;
          }
          
          .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            font-weight: 600 !important;
          }
          
          input[type=checkbox], input[type=radio] { 
            margin: -1px 4px 0 0;
          }
          
          .picker-container {
            position: relative;
            z-index: 99;
          }
          
          .cal {
            background-color: white;
            display: block;
            width: 216px;
            // -webkit-box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.4);
            // box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.4);
            border-collapse: collapse;
            border-spacing: 0;
            margin-top: 5px;
          }

          .cal a {
            text-decoration: none;
          }

          .cal tr, .cal th, .cal td {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
            border: 1px solid #d8d8d8;
          }

          .cal caption {
            line-height: 32px;
            padding-top: 0px;
            padding-bottom: 0px;
            font-weight: bold;
            color: #e2e2e2;
            text-align: center;
            background: #0073aa;
            //background: rgba(0, 0, 0, 0.35);
            border-top: 1px solid #0073aa;
            border-bottom: 1px solid #0073aa;
            -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.04);
            box-shadow: inset 0 1px rgba(255, 255, 255, 0.04);
          }
          .cal caption a {
            display: block;
            line-height: 32px;
            padding: 0 10px;
            font-size: 15px;
            color: #e2e2e2;
          }
          .cal caption a:hover {
            color: white;
          }
          .cal caption .prev {
            float: left;
          }
          .cal caption .next {
            float: right;
          }
          .cal th, .cal td {
            width: 30px;
            text-align: center;
            text-shadow: 0 1px rgba(255, 255, 255, 0.8);
          }
          .cal th:first-child, .cal td:first-child {
            border-left: 0;
          }
          .cal th {
            line-height: 20px;
            font-size: 10px;
            color: #0073aa;
            text-transform: uppercase;
            background: #fff;
            border-left: 1px solid #f3f3f3;
          }
          .cal td {
            font-size: 11px;
            font-weight: bold;
            border-top: 1px solid #c2c2c2;
            border-left: 1px solid #c2c2c2;
          }
          .cal td a {
            clear: both;
            display: block;
            position: relative;
            width: 30px;
            line-height: 28px;
            color: #666;
            background-image: -webkit-linear-gradient(top, #eaeaea, #e5e5e5 60%, #d9d9d9);
            background-image: -moz-linear-gradient(top, #eaeaea, #e5e5e5 60%, #d9d9d9);
            background-image: -o-linear-gradient(top, #eaeaea, #e5e5e5 60%, #d9d9d9);
            background-image: linear-gradient(to bottom, #eaeaea, #e5e5e5 60%, #d9d9d9);
            -webkit-box-shadow: inset 1px 1px rgba(255, 255, 255, 0.5);
            box-shadow: inset 1px 1px rgba(255, 255, 255, 0.5);
          }
          .cal td a:hover, .cal td.off a {
            background: #fff;
          }
          .cal td.off a {
            color: #b3b3b3;
          }
          .cal td.active a, .cal td a:active {
            margin: -1px 0;
            color: #f3f3f3;
            text-shadow: 0 1px rgba(0, 0, 0, 0.3);
            background: #0091cd;
            border: 1px solid #0073aa;
            -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.05);
            box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.05);
          }
          .cal td.active:first-child a, .cal td:first-child a:active {
            border-left: 0;
            margin-left: 0;
          }
          .cal td.active:last-child a, .cal td:last-child a:active {
            border-right: 0;
            margin-right: 0;
          }
          .cal tr:last-child td.active a, .cal tr:last-child td a:active {
            border-bottom: 0;
            margin-bottom: 0;
          }
          
          input.date-picker {
            width: 219px;
          }
        
          .manage-xoso {
            background-color: #fff;
            -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
            -moz-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
            -ms-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
            -o-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
            box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
            padding: 11px 15px;
            margin: 25px 0;
          }
          
          .manage-xoso h3 {
            display: block;
            padding: 10px;
            background-color: #f5f5f5;
            border: 1px solid #d8d8d8;
          }
          
          .cal th > span.sat-sun {
            color: #ff0000;
            font-weight: bold;
          }
          
          .timerxs-msg { }
          .timerxs-day { }
          .timerxs-clock { }
          
          .formart-msg-2 {
            border-left: 4px solid #7ad03a;
          }
          
          .formart-msg-1 {
            border-left: 4px solid #ffba00;
          }
          
          .manage-xoso hr {
            border-top: 1px dotted #ddd;
            border-bottom: 1px dotted #fafafa;
            margin-top: 5px;
            margin-bottom: 5px;
          }
          
          .formart-msg-1,
          .formart-msg-2 {
            background-color: #f5f5f5;
            display: inline-block;
            line-height: 19px;
            padding: 11px 15px;
            font-size: 13px;
            text-align: left;
            margin: 2px 20px 0 2px;
            color: #444;
          }
          
          span.span-label {
            display: inline-block;
            padding: 0px 20px 0px 2px;
            line-height: 15pt;
          }
          
          .mnt-radio {
            display: inline-block;
            margin-right: 10px;
          }
          
          .msg-span {
            font-style: italic;
            color: #000;  
          }
          
          .msg-screen-xs {
            margin-left: 10px; 
            width: 500px; 
            float: left; 
            border: 1px solid #d8d8d8; 
            background-color: #f5f5f5; 
            min-height: 266px; 
            padding: 10px; 
            overflow: hidden; 
            margin-top: 25px;
          }
          
          .msg-screen-xs-inner {
            padding: 5px; 
            overflow: scroll; 
            border: 1px solid #d8d8d8; 
            background-color: #262626; 
            color: #fff; 
            width: 100%; 
            height: 244px; 
            font-size: 10px;
          }
          
          .box-option-top {
            width: 100%; 
            padding-bottom: 25px;
          }
          
          .box-option-bottom {
            width: 100%; 
            padding: 20px 10px;
            background-color: #e8e8e8;
            border: 1px solid #d8d8d8;
          }
          
          .box-option-bottom-left {
            padding-right: 10px; 
            float:left;
          }
          
          .box-option-bottom-middle {
            padding-left: 10px; 
            padding-right: 10px; 
            float:left;
          }
          
          .search-background1 {
            font-size: 13px;
            font-weight: bold;
            text-align: center;
            opacity:0.5;
            -o-opacity:0.5;
            -ms-opacity:0.5;
            -moz-opacity:0.5;
            -webkit-opacity:0.5;
            filter: alpha(opacity=50) ;
            text-decoration: none;
            width: 169px;
            background-color: #f5f5f5;
            border: 1px solid #d8d8d8;
            padding: 4px 12px;
            color:#fff;
            float: right;
            text-shadow: #fff 0px 0px 20px;
            border-radius: 4px;
            -o-border-radius: 4px;
            -ms-border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
          }
          
          .btn-group > button.btn-number,
          .btn-group > button.btn-number:hover,
          .btn-group > button.btn-number:active,
          .btn-group > button.btn-number:focus {
            border-radius: 0px; 
            -o-border-radius: 0px; 
            -ms-border-radius: 0px; 
            -moz-border-radius: 0px; 
            -webkit-border-radius: 0px;
            border-top: 0 !important;
            border-bottom: 0 !important;
          }
          
          .btn-group-carousel {
            float: left;
            overflow: hidden;
            width: 207px;
            border-top: 1px solid #d8d8d8;
            border-bottom: 1px solid #d8d8d8;
          }
          
          .btn-group-carousel-inner {
            width: 99999999px;
            margin-left: 0px;
          }
          
          .btn-group button.btn:hover,
          .btn-group button.btn:focus,
          .btn-group button.btn:active {
            outline: 0;
            border: 1px solid #d8d8d8;
          }
          
          .search-background1 label {
            margin-bottom: 3px;
          }
          
          .btn-toolbar .btn-first, 
          .btn-toolbar .btn-last {
            background-color: #f5f5f5;
          }
          
        </style>
      ';
      
      return $cssjs;
    }
  }
