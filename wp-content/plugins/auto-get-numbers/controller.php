<?php
/**
 * Options Management Administration Screen.
 *
 * If accessed directly in a browser this page shows a list of all saved options
 * along with editable fields for their values. Serialized data is not supported
 * and there is no way to remove options via this page. It is not linked to from
 * anywhere else in the admin.
 *
 * This file is also the target of the forms in core and custom options pages
 * that use the Settings API. In this case it saves the new option values
 * and returns the user to their page of origin.
 *
 * @package WordPress
 * @subpackage Administration
 */
?>

<?php 
  
  require_once 'connection.php';
  
  function checkExistValueField($value, $field, $table, $type)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
  
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    
    if($type == 'like')
    {
      $query = mysql_query("SELECT ".$field." FROM ".$table." WHERE ".$field." LIKE '%".$value."%'") or die(mysql_error());
      $row = mysql_fetch_array($query);
      if(isset($row[$field]) && !empty($row[$field])){
        return 'true';
      }
    }
    else
    {
      $query = mysql_query("SELECT ".$field." FROM ".$table." WHERE ".$field."='".$value."'") or die(mysql_error());
      $row = mysql_fetch_array($query);
      if(isset($row[$field]) && !empty($row[$field])){
        return 'true';
      }
    }
    
    return 'false';
  }
  
  function getAllValueOfField($table_name, $field, $fieldcond, $value, $type)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    $rows = array();
    if($type == 'like')
    {
      $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."  LIKE '%".$value."%'") or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = $row[$field];
      }  
    }
    else
    {
      $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."='".$value."'") or die(mysql_error());
      while ($row = mysql_fetch_array($query)) 
      {
        $rows[] = $row[$field];
      } 
    }
    
    return $rows;
  }
  
  function getValueOfField($table_name, $field, $fieldcond, $value)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$fieldcond."='".$value."'") or die(mysql_error());
    $row = mysql_fetch_array($query);
    
    return $row[$field];
  } 
  
  // echo '<pre>';
  // $test = getValueOfField('wp_kqxsmn', 'ketqua', 'id', '1');
  // var_dump($test);
  // echo '</pre>';
  // die;
  
  function getInsertValues($table_name, $fields, $values) 
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    
    $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
    if ($conn->connect_error) 
    {
      die("Connection failed: " . $conn->connect_error);
    } 
    $conn->set_charset("utf8");
    $sql = "INSERT INTO ".$table_name."(".$fields.") VALUES (".$values.")";
    $conn->query($sql);

    $conn->close();
  }
  
  // $params is array: $params = array("$field1"=>"$value1", "$field2"=>"$value2", ...);
  // $where is string: $where = "$field = $value";
  function getUpdateValue($table_name, $params, $where)
  { 
    $append_fields = ''; $i = 0;
    foreach($params as $field=>$value)
    {
      if($field == 'status_section')
      {
        if($i == 0)
        {
          $append_fields .= $field."=".$value."";
        }
        else
        {
          $append_fields .= ", ".$field."=".$value."";
        }
      }
      else
      {
        if($i == 0)
        {
          $append_fields .= $field."='".$value."'";
        }
        else
        {
          $append_fields .= ", ".$field."='".$value."'";
        }
      }
      $i++;
    }
  
    $conn = new Connection();
    $str_conn = $conn->_Connection();

    $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
    if ($conn->connect_error) 
    {
      die("Connection failed: " . $conn->connect_error);
    }  
    $conn->set_charset("utf8");
    $sql = "UPDATE ".$table_name." SET ".$append_fields." WHERE ".$where;
    $conn->query($sql);

    $conn->close();
  }
  
  function getKetQuaXoSo($link1)
  {
    $result='';
    $file1='';
    if($file1=fopen($link1,'rb'))
    {
      while (!feof($file1)) 
      {
        $result.= fread($file1, 8192);
      }
      fclose($file1);
    }
    
    return $result;
  }
  
  function getKQXSTheoNgay($section, $date)
  {
    // $link1 = 'http://ketqua.net/xo-so-mien-trung.php?ngay=28/07/2015';
    $link1 = 'http://ketqua.net/'.$section.'.php?ngay='.$date;
    $conts1 = getKetQuaXoSo($link1);
    
    $response = str_replace('> <', '><', str_replace('  ', '', preg_replace('/\s{3}/', '', $conts1)));
    $response = str_replace("&nbsp;", "", $response);
    $response = html_entity_decode($response);
    $response = explode('<div class="tc f2">', $response);
    
    $result = array();
    $keyt = '';
    $astr = array();
    for($i = 0; $i < count($response); $i++)
    {
      $astr[] = str_replace("\r\n", "", $response[$i]);
    }
    
    $j = 0;
    for($i = 0; $i < count($response); $i++)
    {
      $string = $astr[$j]; $j++;
      preg_match('#<th class="bor f2" colspan="9"><h2>(.*)<\/h2>#is', $string, $ngaythang);
      if( ! isset($ngaythang[0])) {
        $ngaythang[0] = null;
      }
      $ngaythang = explode(' ', strip_tags($ngaythang[0]));
      if (count($ngaythang) > 1) 
      {
        $ngaythang = explode('/', $ngaythang[count($ngaythang) - 1]);
        $date = $ngaythang[2] . '-' . $ngaythang[1] . '-' . $ngaythang[0];
      } 
      else 
      {
        $date = '';
      }
      
      $keytinh1 = explode('<div id="ketqua_', $string);
      if( ! isset($keytinh1[1])) {
        $keytinh1[1] = null;
      }
      $keytinh2 = explode('"><div class="bo9', $keytinh1[1]);
      $keyt = str_replace("'", "", $keytinh2[0]);
      
      $string = explode('<td class="span-2 bol f1b db">', $string);
      if( ! isset($string[1])) {
        $string[1] = null;
      }
      $string = $string[1];
      $string = strip_tags(str_replace('</td>', ',</td>', $string));
      
      $find = array('Đặc Biệt','Giải Nhất', 'Giải Nhì','Giải Ba','Giải Tư', 'Giải Năm', 'Giải Sáu', 'Giải Bảy', 'Giải Tám');
      $replace = array('|', '|', '|', '|', '|', '|', '|', '|', '|');
      $string = str_replace($find, $replace, $string);
      $string = str_split($string);
      $chuoihoanchinh = '';
      
      foreach ($string as $str) {
        if ($str != '') {
          $chuoihoanchinh .= trim($str);
        }
      }
      
      $a = array('||,||,|,,,,,,|,|,,|,|,','|,,',',|,');
      $b = array('','|,','|,');
      $s =  str_replace($a,$b,$chuoihoanchinh);

      if($s!='' and $s!='||,||,|,,,,,,|,|,,|,|,' and ($date!='0000-00-00' or $date!=''))
      {
        $chuoilayve = str_replace('|,','|',substr($s,2,-1));
        $final = explode('|',$chuoilayve);
        $final_result = array();
        $index=0;
        foreach($final as $item)
        {
          if($index <= 8)
          {
            $val_item1 = explode('setInterval', $item);
            $val_item2 = str_replace("ĐítLôTô", "#Ditloto$", str_replace("ĐầuLôTô", "#Dauloto$", str_replace("LôTôtrựctiếp", "#Lototructiep$", str_replace(',','-',$val_item1[0]))));
            
            switch($index)
            {
              case 0:
                $final_result['dacbiet'] = strip_tags($val_item2);
              break;
              
              case 1:
                $final_result['giainhat'] = strip_tags($val_item2);
              break;
              
              case 2:
                $final_result['giainhi'] = strip_tags($val_item2);
              break;
              
              case 3:
                $final_result['giaiba'] = strip_tags($val_item2);
              break;
              
              case 4:
                $final_result['giaitu'] = strip_tags($val_item2);
              break;
              
              case 5:
                $final_result['giainam'] = strip_tags($val_item2);
              break;
              
              case 6:
                $final_result['giaisau'] = strip_tags($val_item2);
              break;
              
              case 7:
                if($keyt == "mb")
                {
                  $var1 = explode('-#', strip_tags($val_item2));
                  $final_result['giaibay'] = $var1[0];
                  
                  $var2 = explode('$', $var1[1]);
                  $final_result[$var2[0]] = $var2[1];
                  
                  $var3 = explode('$', $var1[2]);
                  $final_result[$var3[0]] = $var3[1];
                  
                  $var4 = explode('$', $var1[3]);
                  $final_result[$var4[0]] = $var4[1];
                }
                else
                {
                  $final_result['giaibay'] = strip_tags($val_item2);
                }
              break;
              
              case 8:
                $var1 = explode('-#', strip_tags($val_item2));
                $final_result['giaitam'] = $var1[0];
                
                $var2 = explode('$', $var1[1]);
                $final_result[$var2[0]] = $var2[1];
                
                $var3 = explode('$', $var1[2]);
                $final_result[$var3[0]] = $var3[1];
                
                $var4 = explode('$', $var1[3]);
                $final_result[$var4[0]] = $var4[1];
              break;
            }
          }
          
          $index++;
        }
        
        $result[$keyt] = $final_result;
      }
    }
    
    return serialize($result);
  }
  
  try
  {
    if(isset($_POST['date_hidden_id']) && !empty($_POST['date_hidden_id']))
    {  
      $date_array = explode(',', $_POST['date_hidden_id']);
      $section = $_POST['update_numbers_1'];       
      $fields = 'province_id, status_result, content_result, created_at';   
      $table_name = 'tbl_result';
      $status = 1;
      $test = array();
      foreach($date_array as $data)
      {
        if($data && !empty($data))
        {
          $d1 = explode('/', $data);
          if($d1 && !empty($d1))
          {
            $created_at = $d1[2].'-'.$d1[1].'-'.$d1[0];
            $results = unserialize(getKQXSTheoNgay($section, $data));
            
            $field_exist = checkExistValueField($created_at, 'created_at', $table_name, 'like');
            
            foreach($results as $keys=>$vals)
            {
              $result = array($keys=>$results[$keys]);
              $province_id = getValueOfField('tbl_province', "id", "code", $keys);
              $ids = getAllValueOfField($table_name, "id", "created_at", $created_at, 'like');
              
              if($field_exist == "false" && (!isset($ids) || empty($ids)))
              {
                $values = "'".$province_id."', ".$status.", '".serialize($result)."', '".$created_at."'";
                getInsertValues($table_name, $fields, $values);
              }
              else if($field_exist == "true")
              {
                $params = array('province_id'=>$province_id, 'status_result'=>$status, 'content_result'=>serialize($result), 'created_at'=>$created_at);
                if(!empty($ids))
                {
                  foreach($ids as $id)
                  {
                    $where = 'id='.$id;
                    getUpdateValue($table_name, $params, $where);
                  }
                }  
                else
                {
                  foreach($ids as $id)
                  {
                    $pro_id = getValueOfField($table_name, "province_id", "id", $id);
                    if($pro_id == $province_id)
                    {
                      $where = 'id='.$id;
                      getUpdateValue($table_name, $params, $where);
                    }
                    else
                    {
                      $values = "'".$province_id."', ".$status.", '".serialize($result)."', '".$created_at."'";
                      getInsertValues($table_name, $fields, $values);
                    }
                  }
                  
                  $all_ids = getAllValueOfField($table_name, "province_id", "created_at", $created_at, 'like');
                  foreach($all_ids as $prid)
                  {
                    if($prid != $province_id)
                    {
                      $values = "'".$province_id."', ".$status.", '".serialize($result)."', '".$created_at."'";
                      getInsertValues($table_name, $fields, $values);
                    }
                  }
                } 
              }
            } 
          }
        }
      }
      echo $_POST['date_hidden_id'];
      // echo serialize($test); 
    }
  }
  catch(Exeption $e)
  {
    echo 'error';
  }
  ?>
  