<?php
  /**
  * Plugin Name: Tu dong goi ket qua xo so
  * Plugin URI: http://m.giovangchotso.net
  * Description: Plugin tu dong goi ket qua mot so tu mot trang bat ky
  * Version: 1.0 // Day la phien ban dau tien cua plugin
  * Author: http://m.giovangchotso.net
  * Author URI: http://m.giovangchotso.net
  * License: GPLv2 or later // http://m.giovangchotso.net
  **/
  
  //echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>';
  /*** Hook, Action & Filter ***/
?>

<?php
  session_start();
  $_SESSION["count_connect"] = 1;
    
  class MienNam
  {    
    function getKetQuaXoSo($link1)
    {
      $result='';
      $file1='';
      if($file1=fopen($link1,'rb'))
      {
        while (!feof($file1)) 
        {
          $result.= fread($file1, 8192);
        }
        fclose($file1);
      }
      
      return $result;
    }
    
    function getArrayFromExplode1($link1)
    {
      global $wpob;
      $wpob = new MienNam();
      $kqxs = $wpob->getKetQuaXoSo($link1);
      $ex_array_1 = explode(";", $kqxs);
      return $ex_array_1;
    }
    
    function loadTinhAll()
    {
      return array(
                    "1"=>"TP. HCM","2"=>"Đồng Tháp","3"=>"Cà Mau","7"=>"Bến Tre","8"=>"Vũng Tàu","9"=>"Bạc Liêu","10"=>"Đồng Nai","11"=>"Cần Thơ","12"=>"Sóc Trăng","13"=>"Tây Ninh","14"=>"An Giang","15"=>"Bình Thuận","16"=>"Vĩnh Long","17"=>"Bình Dương","18"=>"Trà Vinh","19"=>"Long An","20"=>"Hậu Giang","21"=>"Bình Phước","22"=>"Tiền Giang","23"=>"Kiên Giang","24"=>"Đà Lạt","26"=>"Thừa T. Huế","27"=>"Phú Yên","28"=>"Quảng Nam","29"=>"Đắk Lắk","30"=>"Đà Nẵng","31"=>"Khánh Hòa","32"=>"Bình Định","33"=>"Quảng Trị","34"=>"Quảng Bình","35"=>"Gia Lai","36"=>"Ninh Thuận","37"=>"Quảng Ngãi","38"=>"Đắk Nông","39"=>"Kon Tum","46"=>"Miền bắc","47"=>"Miền bắc","48"=>"Miền bắc","49"=>"Miền bắc","50"=>"Miền bắc","51"=>"Miền bắc"
                  );
    }
    
    function loadKeyAll()
    {
      return array(
                "1"=>"hcm","2"=>"dt","3"=>"cm","7"=>"bt","8"=>"vt","9"=>"bl","10"=>"dn","11"=>"ct","12"=>"st","13"=>"tn","14"=>"ag","15"=>"bth","16"=>"vl","17"=>"bd","18"=>"tv","19"=>"la","20"=>"hg","21"=>"bp","22"=>"tg","23"=>"kg","24"=>"dl","26"=>"tth","27"=>"py","28"=>"qna","29"=>"dlc","30"=>"dna","31"=>"kh","32"=>"bdi","33"=>"qt","34"=>"qb","35"=>"gl","36"=>"nt","37"=>"qng","38"=>"dno","39"=>"kt","46"=>"mb","47"=>"mb","48"=>"mb","49"=>"mb","50"=>"mb","51"=>"mb"
              );
    }
    
    function findTenOrKeyTinh($id, $array)
    {
      $tenkey = "";
      foreach($array as $key=>$value)
      {
        if((int)$key == (int)$id)
        {
          $tenkey = $value;
        }
      }
      
      return $tenkey;
    }
    
    function findIdArray($id, $array)
    {
      $result = '';
      for($i=0; $i < count($array); $i++)
      {
        if((int)$id == (int)$array[$i])
        {
          $result = $array[$i];
        }
      }
      
      return $result;
    }
    
    function renderArray($link1, $key)
    {
      $result = array(); 
      $rearray = array(); 
      global $wpob;
      $wpob = new MienNam();
      $array = $wpob->getArrayFromExplode1($link1);
      $exmt = explode("=", $array[1]);
      $ids = explode(",", str_replace('"', '', $exmt[1]));
      $countIds = count($ids);
      
      if($countIds >= 2)
      {
        for($i=0; $i<$countIds; $i++)
        {
          $rearray[$i][] = $array[0]; 
          $rearray[$i][] = trim(str_replace('"', '', $array[1])); 
          $rearray[$i][] = $array[2]; 
          $rearray[$i][] = $array[3];
          
          switch($i)
          {
            case 0:
              for($k0=5; $k0<=22; $k0++)
              {
                $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k0]));
              }
            break;
            
            case 1:
              for($k1=23; $k1<=41; $k1++)
              {
                $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k1]));
              }
            break;
            
            case 2:
              for($k2=42; $k2<=60; $k2++)
              {
                $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k2]));
              }
            break;
            
            case 3:
              for($k3=61; $k3<=78; $k3++)
              {
                $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k3]));
              }
            break;
          }
          
          $keytinh = $wpob->findTenOrKeyTinh($ids[$i], $key);
          
          $gdb = ''; $g1 = ''; $g2 = ''; $g3 = ''; 
          $g4 = ''; $g5 = ''; $g6 = ''; $g7 = ''; 
          $g8 = ''; $tdttt4 = ''; $tdt123_1 = ''; 
          $tdt123_2 = ''; $tdt123_3 = '';
          
          foreach($rearray[$i] as $arr)
          {
            $ex_arr = explode('=', $arr);
            
            if($ex_arr[0] != '' && 
            $ex_arr[0] != 'runtt' &&  
            $ex_arr[0] != 'listtinhnew' &&
            $ex_arr[0] != 'newtime' && 
            $ex_arr[0] != 'delay')
            {
              $exmatinh = explode('_', $ex_arr[0]);
              
              switch($exmatinh[1])
              {
                case 'Gdb':
                  $gdb = $ex_arr[1];
                break;
                
                case 'G1':
                  $g1 = $ex_arr[1];
                break;
                
                case 'G2':
                  $g2 .= '-'.$ex_arr[1];
                break;
                
                case 'G3':
                  $g3 .= '-'.$ex_arr[1];
                break;
                
                case 'G4':
                  $g4 .= '-'.$ex_arr[1];
                break;
                
                case 'G5':
                  $g5 .= '-'.$ex_arr[1];
                break;
                
                case 'G6':
                  $g6 .= '-'.$ex_arr[1];
                break;
                
                case 'G7':
                  $g7 .= '-'.$ex_arr[1];
                break;
                
                case 'G8':
                  $g8 = '-'.$ex_arr[1];
                break;
                
              }
            }
          }
          
          
          
          $num = 1;
          $result[$keytinh] = array("dacbiet" => $gdb, "giainhat" => $g1, "giainhi" => substr($g2, $num), "giaiba" => substr($g3, $num), "giaitu" => substr($g4, $num), "giainam" => substr($g5, $num), "giaisau" => substr($g6, $num), "giaibay" => substr($g7, $num), "giaitam" => substr($g8, $num));
          
          unset($gdb); unset($g1); unset($g2); unset($g3); 
          unset($g4); unset($g5); unset($g6); unset($g7); 
          unset($g8);
        }
      }
      else
      {
        $rearray = array();
        
        for($re=0; $re<=31; $re++)
        {
          $rearray[] = $array[$re];
        }
        
        $keytinh = $wpob->findTenOrKeyTinh($ids[0], $key);
          
        $gdb = ''; $g1 = ''; $g2 = ''; $g3 = ''; 
        $g4 = ''; $g5 = ''; $g6 = ''; $g7 = ''; 
        $g8 = ''; $tdttt4 = ''; $tdt123_1 = ''; 
        $tdt123_2 = ''; $tdt123_3 = '';
        
        foreach($rearray as $arrs)
        {
          $arr =  trim(str_replace(array('"', 'kqxs[', ']'), array('', '', ''), $arrs));
          $ex_arr = explode('=', $arr);
          
          if($ex_arr[0] != '' && 
          $ex_arr[0] != 'runtt' &&  
          $ex_arr[0] != 'listtinhnew' &&
          $ex_arr[0] != 'newtime' && 
          $ex_arr[0] != 'delay')
          {
          
            $exmatinh = explode('_', $ex_arr[0]);
            switch($exmatinh[1])
            {
              case 'Gdb':
                $gdb = $ex_arr[1];
              break;
              
              case 'G1':
                $g1 = $ex_arr[1];
              break;
              
              case 'G2':
                $g2 .= '-'.$ex_arr[1];
              break;
              
              case 'G3':
                $g3 .= '-'.$ex_arr[1];
              break;
              
              case 'G4':
                $g4 .= '-'.$ex_arr[1];
              break;
              
              case 'G5':
                $g5 .= '-'.$ex_arr[1];
              break;
              
              case 'G6':
                $g6 .= '-'.$ex_arr[1];
              break;
              
              case 'G7':
                $g7 .= '-'.$ex_arr[1];
              break;
              
              case 'G8':
                $g8 = '-'.$ex_arr[1];
              break;
            }
            
          }
        }
        
        $num = 1;
        $result[$keytinh] = array("dacbiet" => $gdb, "giainhat" => $g1, "giainhi" => substr($g2, $num), "giaiba" => substr($g3, $num), "giaitu" => substr($g4, $num), "giainam" => substr($g5, $num), "giaisau" => substr($g6, $num), "giaibay" => substr($g7, $num), "giaitam" => substr($g8, $num));
        
        unset($gdb); unset($g1); unset($g2); unset($g3); 
        unset($g4); unset($g5); unset($g6); unset($g7); unset($g8);
      }
      
      return $result;
    }
    
    function getKetQuaMienNam($link1)
    {
      global $wpob;
      $wpob = new MienNam();
      $key = $wpob->loadKeyAll();
      return $wpob->renderArray($link1, $key);
    }
    
    
    function redirectConnect()
    {
      $_SESSION["count_connect"] = $_SESSION["count_connect"] + 1;
      if($_SESSION["count_connect"] >= 5)
      {
        $_SESSION["count_connect"] = 1;
      }
  
      $url1 = 'http://www.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
                http://www.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
                http://www.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
                
      $url2 = 'http://ww1.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
              http://ww1.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
              http://ww1.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
              
      $url3 = 'http://ww2.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
              http://ww2.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
              http://ww2.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
              
      $url4 = 'http://www.minhchinh.com/xstt/MB.php?visit=0 , 
              http://www.minhchinh.com/xstt/MT.php?visit=0 , 
              http://www.minhchinh.com/xstt/MN.php?visit=0';
                
      if(isset($_SESSION["count_connect"]) && !empty($_SESSION["count_connect"]))
      {
        switch($_SESSION["count_connect"])
        {
          case 1:
            $url = $url1;
          break;
          
          case 2: 
            $url = $url2;
          break;
          
          case 3:
            $url = $url3;
          break;
          
          case 4:
            $url = $url4;
          break;
          
          default:
            $url = $url4;
            $_SESSION["count_connect"] = 1;
          break;
        }
        
        return $url;
      }
      else
      {
        return $url4;
      }
    }
    
    function kqttMienNam()
    {
      global $wpob;
      $wpob = new MienNam();
      $link = explode(',', $wpob->redirectConnect());
      $datas = $wpob->getKetQuaMienNam(trim('http://www.minhchinh.com/xstt/MN.php?visit=0'));
      // $datas = $wpob->getKetQuaMienNam(trim($link[2]));
      
      $result = '#';
      $i = 0;
      foreach($datas as $kdata=>$data)
      {
      
        if($i == 0)
        {
          $result .= $kdata.'$';
        }
        else
        {
          $result .= ']['.$kdata.'$';
        }
      
        foreach($data as $key=>$value)
        {
          switch($key)
          {
            case 'dacbiet':
              $result .= '"'.$key.':'.$value.'"';  
            break;
            
            case 'giainhat':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giainhi':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giaiba':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giaitu':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giainam':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giaisau':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            case 'giaibay':
              $result .= ',"'.$key.':'.$value.'"';
            break;
          
            case 'giaitam':
              $result .= ',"'.$key.':'.$value.'"';
            break;
            
            // case 'list1':
              // $result .= ',"Lototructiep:'.$value.'"';
            // break;
            
            // case 'list2':
              // if(is_array($value)): 
                // $convert = '';
                // for($i = 0; $i < count($value); $i++):
                  // if($i == 0):
                    // $convert .= $value[$i];
                  // else:
                    // $convert .= '#'.$value[$i];
                  // endif;
                // endfor;
                // $result .= ',"Dauloto:'.$convert.'"';
              // else:
                // $result .= $value;
              // endif;
            // break;
            
            // case 'list3':
              // if(is_array($value)):
                // $convert = '';
                // for($i = 0; $i < count($value); $i++):
                  // if($i == 0):
                    // $convert .= $value[$i];
                  // else:
                    // $convert .= '#'.$value[$i];
                  // endif;
                // endfor;
                // $result .= ',"Ditloto:'.$convert.'"';
              // else:
                // $result .= $value;
              // endif;
            // break;
          }
        }
        $i++;
      }
      $result .= '#';
      
      return $result;
    }
  }
  
  $miennam = new MienNam();
  echo $miennam->kqttMienNam();
  
  // $link = explode(',', $wpob->redirectConnect());
  // echo $link[2];