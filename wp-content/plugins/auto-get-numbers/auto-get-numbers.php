<?php 
date_default_timezone_set('Asia/Ho_Chi_Minh');
  /**
  * Plugin Name: Tu dong goi ket qua xo so
  * Plugin URI: http://m.giovangchotso.net
  * Description: Plugin tu dong goi ket qua mot so tu mot trang bat ky
  * Version: 1.0 // Day la phien ban dau tien cua plugin
  * Author: http://m.giovangchotso.net
  * Author URI: http://m.giovangchotso.net
  * License: GPLv2 or later // http://m.giovangchotso.net
  **/
  //echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>';
  /*** Hook, Action & Filter ***/
?>

<?php 
  session_start();
  require_once 'extends.php';
  if(isset($_SESSION['msisdn']) && isset($_SESSION['DATA'])){
    $red = new functionExtends();
    $red->__construct();
  }
  if(!isset($_SESSION['msisdn']) && $_SESSION['msisdn'] != 'null'){
      $extend = new functionExtends();
      $mobile = $_SESSION['msisdn'];
      $sub = $extend->getAllValueOfField('subscriber', 'id', 'msisdn', $mobile, 'insertSub');
      if(count($sub) == 0){
        $date = date('Y-m-d H:i:s');
        $field = "msisdn, created_at, updated_at";
        $values = "'".$mobile."','".$date."','".$date."'";
        $table = "subscriber ";
        $query = $extend->getInsertNewValues($table, $field, $values);
      }
  }
  require_once 'connection.php';
  require_once 'functionadmin.php';
  
  function addCssJs() 
  {
    echo '
    <Meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Lấy kết quả xổ số tự động</title>
    <link type="text/css" rel="stylesheet" href="'.plugins_url('/css/bootstrap.min.css', __FILE__).'" />
    <link type="text/css" rel="stylesheet" href="'.plugins_url('/css/css.css', __FILE__).'" />
    <link type="text/css" rel="stylesheet" href="'.plugins_url('/css/responsive.css', __FILE__).'" />
    
    <script type="text/javascript" scr="'.plugins_url('/js/jquery.min.js', __FILE__).'"></script>
    <script type="text/javascript" scr="'.plugins_url('/js/validator.js', __FILE__).'"></script>
    <script type="text/javascript" scr="'.plugins_url('/js/bootstrap.min.js', __FILE__).'"></script>
    
    
    <style type="text/css">
      
      .danhsach_tructiep .danhsach_tructiep_inner::after {
        content: "KẾT QUẢ XỔ SỐ HÔM NAY";
      }
      
      .xoso-mienbac-inner.xoso-ag-inner::after {
        content: "XỔ SỐ AN GIANG";
      }
      
      .xoso-mienbac-inner.xoso-bl-inner::after {
        content: "XỔ SỐ BẠC LIÊU";
      }
      
      .xoso-mienbac-inner.xoso-bt-inner::after {
        content: "XỔ SỐ BẾN TRE";
      }
      
      .xoso-mienbac-inner.xoso-bdi-inner::after {
        content: "XỔ SỐ BÌNH ĐỊNH";
      }
      
      .xoso-mienbac-inner.xoso-bd-inner::after {
        content: "XỔ SỐ BÌNH DƯƠNG";
      }
      
      .xoso-mienbac-inner.xoso-bp-inner::after {
        content: "XỔ SỐ BÌNH PHƯỚC";
      }
      
      .xoso-mienbac-inner.xoso-bth-inner::after {
        content: "XỔ SỐ BÌNH THUẬN";
      }
      
      .xoso-mienbac-inner.xoso-cm-inner::after {
        content: "XỔ SỐ CÀ MAU";
      }
      
      .xoso-mienbac-inner.xoso-ct-inner::after {
        content: "XỔ SỐ CẦN THƠ";
      }
      
      .xoso-mienbac-inner.xoso-dl-inner::after {
        content: "XỔ SỐ ĐÀ LẠT";
      }
      
      .xoso-mienbac-inner.xoso-dna-inner::after {
        content: "XỔ SỐ ĐÀ NẴNG";
      }
      
      .xoso-mienbac-inner.xoso-dlc-inner::after {
        content: "XỔ SỐ ĐẮC LẮC";
      }
      
      .xoso-mienbac-inner.xoso-dn-inner::after {
        content: "XỔ SỐ ĐỒNG NAI";
      }
      
      .xoso-mienbac-inner.xoso-dt-inner::after {
        content: "XỔ SỐ ĐỒNG THÁP";
      }
      
      .xoso-mienbac-inner.xoso-gl-inner::after {
        content: "XỔ SỐ GIA LAI";
      }
      
      .xoso-mienbac-inner.xoso-hg-inner::after {
        content: "XỔ SỐ HẬU GIANG";
      }
      
      .xoso-mienbac-inner.xoso-hcm-inner::after {
        content: "XỔ SỐ HỒ CHÍ MINH";
      }
      
      .xoso-mienbac-inner.xoso-tth-inner::after {
        content: "XỔ SỐ THỪA THIÊN HUẾ";
      }
      
      .xoso-mienbac-inner.xoso-kh-inner::after {
        content: "XỔ SỐ KHÁNH HÒA";
      }
      
      .xoso-mienbac-inner.xoso-kg-inner::after {
        content: "XỔ SỐ KIÊN GIANG";
      }
      
      .xoso-mienbac-inner.xoso-kt-inner::after {
        content: "XỔ SỐ KON TUM";
      }
      
      .xoso-mienbac-inner.xoso-la-inner::after {
        content: "XỔ SỐ LONG AN";
      }
      
      .xoso-mienbac-inner.xoso-mn-inner::after {
        content: "XỔ SỐ MIỀN NAM";
      }
      
      .xoso-mienbac-inner.xoso-nt-inner::after {
        content: "XỔ SỐ NINH THUẬN";
      }
      
      .xoso-mienbac-inner.xoso-py-inner::after {
        content: "XỔ SỐ PHÚ YÊN";
      }
      
      .xoso-mienbac-inner.xoso-qb-inner::after {
        content: "XỔ SỐ QUẢNG BÌNH";
      }
      
      .xoso-mienbac-inner.xoso-qna-inner::after {
        content: "XỔ SỐ QUẢNG NAM";
      }
      
      .xoso-mienbac-inner.xoso-qng-inner::after {
        content: "XỔ SỐ QUẢNG NGÃI";
      }
      
      .xoso-mienbac-inner.xoso-qt-inner::after {
        content: "XỔ SỐ QUẢNG TRỊ";
      }
      
      .xoso-mienbac-inner.xoso-st-inner::after {
        content: "XỔ SỐ SÓC TRĂNG";
      }
      
      .xoso-mienbac-inner.xoso-tn-inner::after {
        content: "XỔ SỐ TÂY NINH";
      }
      
      .xoso-mienbac-inner.xoso-tg-inner::after {
        content: "XỔ SỐ TIỀN GIANG";
      }
      
      .xoso-mienbac-inner.xoso-tv-inner::after {
        content: "XỔ SỐ TRÀ VINH";
      }
      
      .xoso-mienbac-inner.xoso-vl-inner::after {
        content: "XỔ SỐ VĨNH LONG";
      }
      
      .xoso-mienbac-inner.xoso-vt-inner::after {
        content: "XỔ SỐ VŨNG TÀU";
      }
      
      .xoso-mienbac-inner.xoso-mb-inner::after {
        content: "XỔ SỐ MIỀN BẮC";
      }
      
      .xoso-mienbac-inner.xoso-dno-inner::after {
        content: "XỔ SỐ ĐẮC NÔNG";
      }
      
      .xoso-mienbac-inner.xoso-mt-inner::after {
        content: "XỔ SỐ MIỀN TRUNG";
      }
      
      .xoso-mienbac-inner.xoso-btr-inner::after {
        content: "XỔ SỐ BẾN TRE";
      }
      
      .xoso-mienbac-inner.xoso-bdu-inner::after {
        content: "XỔ SỐ BÌNH DƯƠNG";
      }
      
      .xoso-mienbac-inner.xoso-dlt-inner::after {
        content: "XỔ SỐ ĐÀ LẠT";
      }
      
      .xoso-mienbac-inner.xoso-dni-inner::after {
        content: "XỔ SỐ ĐỒNG NAI";
      }
      
      .xoso-mienbac-inner.dau-loto.dit-loto::after {
        content: "ĐÍT - LÔ TÔ";
      }
      
      .xoso-mienbac-inner.dau-loto::after {
        content: "ĐẦU - LÔ TÔ";
      }
      
      .xoso-mienbac.xoso-bdi .loto-tructiep::after,
      .xoso-mienbac-inner.loto-tructiep::after {
        content: "LÔ TÔ TRỰC TIẾP";
      }
    
      .login-customer .login-customer-inner::after {
        content: "ĐĂNG NHẬP";
      } 
    
      .bar-kqxs .bar-kqxs-inner::after {
        content: "DỊCH VỤ XỔ SỐ TRỰC TUYẾN";
      }  
    
      .login-customer-inner::after,
      .bar-kqxs-inner::after,
      .xoso-mienbac-inner::after,
      .danhsach_tructiep_inner::after {
        color: #262626;
        content: "XỔ SỐ MIỀN BẮC";
        font-size: 15px;
        font-weight: 700;
        left: 15px;
        letter-spacing: 1px;
        position: absolute;
        text-transform: uppercase;
        top: 15px;
      }
      
      ul.nav-sms-web-wap {
        padding-right: 30px;
        margin-bottom: 0px;
      }
      
      .xoso-mienbac {
        padding: 50px 0 20px;
      }
      
      .service-register {
        display: none;
      }
      
      .service-register.active {
        display: block;
      }
      
      .login-customer,
      .bar-kqxs,
      .danhsach_tructiep {
        padding: 50px 0 35px;
      }
      
      .login-customer,
      .bar-kqxs,
      .xoso-mienbac,
      .danhsach_tructiep {
        background-color: #f8f8f8;
        border: 1px solid #e8e8e8;
        margin-bottom: 30px;
        max-width: 100%;
        overflow: auto;
        position: relative;
      }
      
      .xoso-mienbac .dau-loto td.first {
        text-align: center;
        max-width: 20px;
      }
      
      .xoso-mienbac td.first {
        border-right: 1px solid #e8e8e8;
      }
      
      .xoso-mienbac td {
        padding-left: 15px;
        font-size: 15px;
      }
      
      .xoso-mienbac td.loto-tt.first-lt {
        border-left: 0;
      }
      
      .xoso-mienbac td.loto-tt {
        border-left: 1px solid #e8e8e8;
        text-align: center;
      }
      
      .login-customer-inner {
        padding-left: 15px !important;
      }
      
      .login-customer-inner input {
        border: 1px solid #d8d8d8;
      }
      
      .login-customer-inner,
      .bar-kqxs-inner,
      .xoso-mienbac-inner,
      .danhsach_tructiep_inner {
        background-color: #fff;
        border-color: #ddd;
        border-radius: 4px 4px 0 0;
        border-width: 1px;
        box-shadow: none;
        -o-box-shadow: none;
        -ms-box-shadow: none;
        -moz-box-shadow: none;
        -webkit-box-shadow: none;
        margin-left: 0;
        margin-right: 0;
      }
      
      .login-customer-inner,
      .bar-kqxs-inner,
      .danhsach_tructiep_inner {
        padding: 10px 0;
        border-top: 1px solid #e8e8e8;
        border-radius: 0;
        -o-border-radius: 0;
        -ms-border-radius: 0;
        -moz-border-radius: 0;
        -webkit-border-radius: 0;
        border-bottom: 1px solid #e8e8e8;
        min-width: 360px;
        overflow: auto;
      }
      
      ul.nav_links_tructiep {
        margin: 0;
        padding-left: 35px;
        list-style-image: url("'.plugins_url('/images/bkg_block-layered-label.gif', __FILE__).'");
      }
      
      ul.nav_links_tructiep ul.nav_link_sub {
        width: 100%;
        padding-left: 20px;
        overflow: hidden;
        display: block;
        list-style-image: url("'.plugins_url('/images/icon-breadcrumbs.png', __FILE__).'");
      }
      
      .entry-content ul.nav_links_tructiep > li {
        background-attachment: scroll;
        background-color: transparent;
        background-image: url("'.plugins_url('/images/bkg_collapse.gif', __FILE__).'");
        background-position: 96% 8px;
        background-repeat: no-repeat;
        padding-right: 35px;
      }
      
      .entry-content ul.nav_links_tructiep > li.parent.active {
        background-position: 96% -50px;
      }
      
      .entry-content ul.nav_links_tructiep > li > a {
        color: #333;
        font-size: 13px;
        font-weight: bold;
        text-transform: uppercase;
      }
      
      .entry-content ul.nav_link_sub > li > a {
        color: #444;
        font-size: 13px;
        text-transform: capitalize;
      }
      
      .note-msg {
        padding-bottom: 10px;
        border-bottom: 1px solid #e8e8e8;
        margin-bottom: 10px;
      }
      
      .note-msg > .note-format {
        display: inline;
        padding-left: 15px;
      }
      
      .note-msg > .note-format > img {
        margin-top: -2px;
      }
      
      .note-msg > .note-format > label {
        padding-right: 5px;
      }
      
      div.div-status {
        width: 100%;
        min-width: 285px;
      }
      
      span.span-status.span-new {
        background-image: url("'.plugins_url('/images/icon_new32_new.gif', __FILE__).'");
      }
      
      span.span-status.span-waiting {
        background-image: url("'.plugins_url('/images/loading-hoz-waiting.gif', __FILE__).'");
      }
      
      span.span-status.span-pedding {
        background-image: url("'.plugins_url('/images/loading-hoz-pedding.gif', __FILE__).'");
        height: 14px;
      }
      
      span.span-status.span-new,
      span.span-status.span-waiting,
      span.span-status.span-pedding {
        background-attachment: scroll;
        background-repeat: no-repeat;
        background-position: 0 0;
        background-color: transparent;
      }
      
      span.span-status {
        content: new;
        display: inline-block;
        height: 13px;
        width: 50px;
        margin-left: 20px;
      }
      
      div.xoso-mienbac table.table span.span-status {
        margin-left: 0;
        margin-right: 10px;
      }
      
      .entry-content ul.nav_links_tructiep ul.nav_link_sub a > span.span-title {
        width: 69%;
      }

      .entry-content ul.nav_links_tructiep a > span.span-title {
        width: 100%;
        display: inline-block;
        min-width: 165px;
      }
      
      .bar-kqxs h4.welcome-customer {
        padding: 0 20px 0 25px;
        font-size: 14px;
        font-weight: bold;
        color:#bc360a;
      }
      
      input#customer-name,
      input#customer-mobile,
      input#customer-password,
      input#customer-password-confirm {
        min-width: 250px;
      }
      
      div.xoso-mienbac table.table tr > th {
        padding: 8px 15px;
        border-right: 1px solid #e8e8e8;
        min-width: 105px;
      }
      
      div.xoso-mienbac .dau-loto table.table tr > th {
        text-align: center;
      }
      
      div.xoso-mienbac table.table tr td.item-td {
        padding: 8px 15px;
      }
      
      div.xoso-mienbac table.table tr td.item-td-giainhat,
      div.xoso-mienbac table.table tr td.item-td-dacbiet {
        color: #f50f0f;
        font-weight: bold;
      }
      
      div.xoso-mienbac table.table tr td.item-td {
        background-image: url("'.plugins_url('/images/icon-item-filter.png', __FILE__).'");
        background-color: #fff;
        background-repeat: no-repeat;
        background-attachment: scroll;
        background-position: 96% 10px;
      }
      
      div.xoso-mienbac table.table tr td.item-td.active {
        background-position: 96% -38px;
      }
      
      span.clock-sp {
        display: inline-block; 
        padding-left: 5px; 
        padding-right: 5px; 
        background-color: #f5f5f5; 
        margin-right: 2px;
      }
      
      span.clock-timerxs-msg {
        display: inline-block; 
        padding-left: 5px; 
        padding-right: 5px; 
        background-color: #fff;
      }
      
      span.clock-timerxs {
        display: inline-block; 
        padding-left: 5px;  
        padding-right: 5px; 
        background-color: #e5e5e5; 
        margin-right: 2px;
      }
      
      .multiple-select-services select.form-control option.first {
        border-top: 1px solid #e8e8e8;
      }
      
      .multiple-select-services select.form-control option {
        padding: 10px 0;
        border-bottom: 1px solid #e8e8e8;
      }
      
      span.span-dk,
      span.span-dest {
        font-weight: bold;
      }
      
      .validation-advice {
        clear: both;
        min-height: 13px;
        margin: 3px 0 0;
        padding-left: 17px;
        font-size: 11px;
        font-weight: bold;
        line-height: 13px;
        background-image: url("'.plugins_url('/images/validation_advice_bg.gif', __FILE__).'");
        background-position: 2px 0; 
        background-repeat: no-repeat;
        color: #eb340a;
        width: 100%;
      }
      
      .service-register .btn-default,
      .service-register .btn-default:hover,
      .service-register .btn-default:active,
      .service-register .btn-default:focus
      {
        color: #333;
      }
      
      .service-register .btn-primary,
      .service-register .btn-success,
      .service-register .btn-info,
      .service-register .btn-warning,
      .service-register .btn-danger,
      .service-register .btn-primary:hover,
      .service-register .btn-success:hover,
      .service-register .btn-info:hover,
      .service-register .btn-warning:hover,
      .service-register .btn-danger:hover,
      .service-register .btn-primary:active,
      .service-register .btn-success:active,
      .service-register .btn-info:active,
      .service-register .btn-warning:active,
      .service-register .btn-danger:active
      {
        color: #fff;
      }
      
      ul.nav-option-customer {
        list-style: none;
        padding: 0 0 10px 0;
      }
      
      ul.nav-option-customer li.item {
        display: inline-block;
        padding-right: 5px;
        padding-bottom: 15px;
      }
      
      .option-customer {
        height: 50px;
      }
      
      button, input[type="submit"], input[type="button"], input[type="reset"] {
        border-top: 3px solid #e05d22 !important;
      }
    
    </style>
    ';
  }
  add_action( 'wp_head', 'addCssJs' );

  if(!class_exists('Auto_Get_Number')) 
  {
    class Auto_Get_Number 
    {
      function __construct(){ }
      
      function renderHtmlFromKey($data, $key, $keytt, $type, $start_js)
      {
        
        global $wpob;
        $wpob = new Auto_Get_Number();
        
        $_init_url = '';
        switch($type)
        {
          case 'mientrung':
            $_init_url = plugins_url('/mientrung.php', __FILE__);
          break;
          
          case 'miennam':
            $_init_url = plugins_url('/miennam.php', __FILE__);
          break;
        }
        
        $html .= '
            <div class="xoso-mienbac xoso-'.$key.'">
              <div class="xoso-mienbac-inner xoso-'.$key.'-inner">
                <table class="table">
                  <tbody>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-dacbiet">Đặc Biệt</th>
                    <td id="item-td-'.$key.'-0" class="last item-td item-td-'.$key.' item-td-dacbiet">'.$data['dacbiet'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giainhat">Giải Nhất</th>
                    <td id="item-td-'.$key.'-1" class="last item-td item-td-'.$key.' item-td-giainhat">'.$data['giainhat'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giainhi">Giải Nhì</th>
                    <td id="item-td-'.$key.'-2" class="last item-td item-td-'.$key.' item-td-giainhi">'.$data['giainhi'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giaiba">Giải Ba</th>
                    <td id="item-td-'.$key.'-3" class="last item-td item-td-'.$key.' item-td-giaiba">'.$data['giaiba'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giaitu">Giải Tư</th>
                    <td id="item-td-'.$key.'-4" class="last item-td item-td-'.$key.' item-td-giaitu">'.$data['giaitu'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giainam">Giải Năm</th>
                    <td id="item-td-'.$key.'-5" class="last item-td item-td-'.$key.' item-td-giainam">'.$data['giainam'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giaisau">Giải Sáu</th>
                    <td id="item-td-'.$key.'-6" class="last item-td item-td-'.$key.' item-td-giaisau">'.$data['giaisau'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giaibay">Giải Bảy</th>
                    <td id="item-td-'.$key.'-7" class="last item-td item-td-'.$key.' item-td-giaibay">'.$data['giaibay'].'</td>
                  </tr>';
        
        $html .= '<tr>
                    <th class="first item-th item-th-'.$key.' item-th-giaitam">Giải Tám</th>
                    <td id="item-td-'.$key.'-8" class="last item-td item-td-'.$key.' item-td-giaitam">'.$data['giaitam'].'</td>
                  </tr>';
        
        $html .= '</tbody>
                </table>
              </div>
            </div>';
        
        $dt8 = explode("-Lototructiep", $data[8]);
        // $da1 = explode("-Dauloto", $dt8[1]);
        // $lototructiep = explode('-', $da1[0]); $j = 0;
        
        // $html .= '<div class="xoso-mienbac xoso-'.$key.'"><div class="xoso-mienbac-inner loto-tructiep"><table class="table"><tbody>';
        // for($i = 0; $i < count($lototructiep); $i++) 
        // {
          // if($j == 0 || $j%9 == 0) 
          // {
            // $html .= '<tr>';
            // $first = 'first-lt';
          // } 
          
          // $html .= '<td class="loto-tt '.$first.'">'.$lototructiep[$i].'</td>';  
          // unset($first);
          // if(($j+1)%9 == 0 || ($j+1) == count($lototructiep))
          // {
            // $html .= '</tr>';
          // }
          // $j++;
        // }
        // $html .= '</tbody></table></div></div>';
        
        // $da2 = explode("Ditloto", $da1[1]);
        // $listex2 = explode('-', $da2[0]);
        
        // $html .= '<div class="xoso-mienbac xoso-'.$key.'"><div class="xoso-mienbac-inner dau-loto"><table class="table"><tbody>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-0">'.$listex2[0].'</th><td id="item-td-dau-'.$key.'-0" class="last item-td item-td-'.$key.' item-td-0">'.str_replace(';', '-', $listex2[1]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-1">'.$listex2[2].'</th><td id="item-td-dau-'.$key.'-1" class="last item-td item-td-'.$key.' item-td-1">'.str_replace(';', '-', $listex2[3]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-2">'.$listex2[4].'</th><td id="item-td-dau-'.$key.'-2" class="last item-td item-td-'.$key.' item-td-2">'.str_replace(';', '-', $listex2[5]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-3">'.$listex2[6].'</th><td id="item-td-dau-'.$key.'-3" class="last item-td item-td-'.$key.' item-td-3">'.str_replace(';', '-', $listex2[7]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-4">'.$listex2[8].'</th><td id="item-td-dau-'.$key.'-4" class="last item-td item-td-'.$key.' item-td-4">'.str_replace(';', '-', $listex2[9]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-5">'.$listex2[10].'</th><td id="item-td-dau-'.$key.'-5" class="last item-td item-td-'.$key.' item-td-5">'.str_replace(';', '-', $listex2[11]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-6">'.$listex2[12].'</th><td id="item-td-dau-'.$key.'-6" class="last item-td item-td-'.$key.' item-td-6">'.str_replace(';', '-', $listex2[13]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-7">'.$listex2[14].'</th><td id="item-td-dau-'.$key.'-7" class="last item-td item-td-'.$key.' item-td-7">'.str_replace(';', '-', $listex2[15]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-8">'.$listex2[16].'</th><td id="item-td-dau-'.$key.'-8" class="last item-td item-td-'.$key.' item-td-8">'.str_replace(';', '-', $listex2[17]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-9">'.$listex2[18].'</th><td id="item-td-dau-'.$key.'-9" class="last item-td item-td-'.$key.' item-td-9">'.str_replace(';', '-', $listex2[19]).'</td></tr>';
        // $html .= '</tbody></table></div></div>';
        
        
        // $da3 = explode(",#", $da2[1]);
        // $listex3 = explode('-',$da3[0]);
        
        // $html .= '<div class="xoso-mienbac xoso-'.$key.'"><div class="xoso-mienbac-inner dau-loto dit-loto"><table class="table"><tbody>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-0">'.$listex3[0].'</th><td id="item-td-dit-'.$key.'-0" class="last item-td item-td-'.$key.' item-td-0">'.str_replace(';', '-', $listex3[1]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-1">'.$listex3[2].'</th><td id="item-td-dit-'.$key.'-1" class="last item-td item-td-'.$key.' item-td-1">'.str_replace(';', '-', $listex3[3]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-2">'.$listex3[4].'</th><td id="item-td-dit-'.$key.'-2" class="last item-td item-td-'.$key.' item-td-2">'.str_replace(';', '-', $listex3[5]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-3">'.$listex3[6].'</th><td id="item-td-dit-'.$key.'-3" class="last item-td item-td-'.$key.' item-td-3">'.str_replace(';', '-', $listex3[7]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-4">'.$listex3[8].'</th><td id="item-td-dit-'.$key.'-4" class="last item-td item-td-'.$key.' item-td-4">'.str_replace(';', '-', $listex3[9]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-5">'.$listex3[10].'</th><td id="item-td-dit-'.$key.'-5" class="last item-td item-td-'.$key.' item-td-5">'.str_replace(';', '-', $listex3[11]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-6">'.$listex3[12].'</th><td id="item-td-dit-'.$key.'-6" class="last item-td item-td-'.$key.' item-td-6">'.str_replace(';', '-', $listex3[13]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-7">'.$listex3[14].'</th><td id="item-td-dit-'.$key.'-7" class="last item-td item-td-'.$key.' item-td-7">'.str_replace(';', '-', $listex3[15]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-8">'.$listex3[16].'</th><td id="item-td-dit-'.$key.'-8" class="last item-td item-td-'.$key.' item-td-8">'.str_replace(';', '-', $listex3[17]).'</td></tr>';
        // $html .= '<tr><th class="first item-th item-th-'.$key.' item-th-9">'.$listex3[18].'</th><td id="item-td-dit-'.$key.'-9" class="last item-td item-td-'.$key.' item-td-9">'.str_replace(';', '-', $listex3[19]).'</td></tr>';
        // $html .= '</tbody></table></div></div>';
        
        if($start_js == 'true')
        {
          $build_arrayIds = '';
          $build_arrayIdsDau = '';
          $build_arrayIdsDit = '';
          
          for($i = 0; $i < count($keytt); $i++)
          {
            $key = str_replace(' ', '', strtolower($keytt[$i]));
            
            $build_arrayIds .= ($i != 0 ? ',' : '').'"dacbiet:item-td-'.$key.'-0", "giainhat:item-td-'.$key.'-1", "giainhi:item-td-'.$key.'-2", "giaiba:item-td-'.$key.'-3", "giaitu:item-td-'.$key.'-4", "giainam:item-td-'.$key.'-5", "giaisau:item-td-'.$key.'-6", "giaibay:item-td-'.$key.'-7", "giaitam:item-td-'.$key.'-8"';
            
            $build_arrayIdsDau .= ($i != 0 ? ',' : '').'"0:item-td-dau-'.$key.'-0", "1:item-td-dau-'.$key.'-1", "2:item-td-dau-'.$key.'-2", "3:item-td-dau-'.$key.'-3", "4:item-td-dau-'.$key.'-4", "5:item-td-dau-'.$key.'-5", "6:item-td-dau-'.$key.'-6", "7:item-td-dau-'.$key.'-7", "8:item-td-dau-'.$key.'-8", "9:item-td-dau-'.$key.'-9"';
            
            $build_arrayIdsDit .= ($i != 0 ? ',' : '').'"0:item-td-dit-'.$key.'-0", "1:item-td-dit-'.$key.'-1", "2:item-td-dit-'.$key.'-2", "3:item-td-dit-'.$key.'-3", "4:item-td-dit-'.$key.'-4", "5:item-td-dit-'.$key.'-5", "6:item-td-dit-'.$key.'-6", "7:item-td-dit-'.$key.'-7", "8:item-td-dit-'.$key.'-8", "9:item-td-dit-'.$key.'-9"';
            
          }
          
          $html .= '
            <script type="text/javascript">
            //<![CDATA[
              jQuery(document).ready(function($){
                
                var arrayIds = ['.$build_arrayIds.'];
                var arrayIdsDau = ['.$build_arrayIdsDau.'];
                var arrayIdsDit = ['.$build_arrayIdsDit.'];
                
                //5:28:16 PM
                function checkAMorPM(time) 
                {
                  var d = new Date();
                  var hr = d.getHours();
                  var ampm = hr < 12 ? " AM" : " PM";
                  
                  return ampm;
                }
              
                function autoSetStatusMTMN() 
                {
                  var day = new Date();
                  var time = day.toLocaleTimeString();
                  var checker_timec = checkAMorPM(time);
                    
                  $("div.xoso-mienbac td.item-td").each(function(){
                    var element = $(this).attr("id"),
                        trim_element = $.trim(element),
                        idelement = "#" + trim_element;
                        
                    var data1 = $(idelement).html(),
                    data2 = data1.split("-"),
                    totals = data2.length;
                    
                    $(idelement).addClass("active");
                    if(checker_timec.trim() == "PM" && time >= "13:10:00") 
                    {
                      $(idelement).removeClass("active");
                      if(totals == 1) 
                      {
                        $(idelement).html("<div class=\"div-status\"><span class=\"span-status span-waiting\"></span></div>");
                      }
                      else 
                      {
                        var htmlStr = "<div class=\"div-status\">";
                        for(var i=0; i <= totals; i++) 
                        {
                          htmlStr += "<span class=\"span-status span-waiting\"></span>";
                        }
                        htmlStr += "</div>";
                        $(idelement).html(htmlStr);
                      }  
                    }
                    
                  });  
                
                  if(checker_timec.trim() == "PM" && time >= "13:10:00")
                  {
                  
                    setInterval(function(){
                      $.ajax({
                        type:"get",
                        url:"'.$_init_url.'",
                        success:function(response)
                        {
                          var ares = response.split("][");
                          for(var i = 0; i < ares.length; i++)
                          {
                            var ares_b1 = ares[i];
                            var ares_b1_split = ares_b1.split("$");
                            var next_value = ares_b1_split[1].replace("#","");
                            var next_split = ares_b1_split[0].split("#");
                            var columnkey = "";
                            
                            if(next_split.length == 1)
                            {
                              columnkey = next_split[0];
                            }
                            
                            if(next_split.length == 2) 
                            {
                              columnkey = next_split[1];
                            }
                            
                            var ares_ketq  = JSON.parse("[" + next_value + "]");
                            for(var ii = 0; ii < ares_ketq.length; ii++)
                            {
                              var ketq_a = ares_ketq[ii],
                                  ketq_a_kb = ketq_a.split(":");
                                  
                              var columnid = "#item-td-"+columnkey+"-"+ii;
                                  
                              if(ketq_a_kb[0] == "dacbiet") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1];
                                    if(arrayIds_id == columnid)
                                    {
                                      
                                      if(ketq_a_kb[1] != "" && $.isNumeric(ketq_a_kb[1]) == true)
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giainhat") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1];
                                    if(arrayIds_id == columnid)
                                    {
                                      if(ketq_a_kb[1] != "" && $.isNumeric(ketq_a_kb[1]) == true)
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              } 
                              
                              else if(ketq_a_kb[0] == "giainhi") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1];
                                    if(arrayIds_id == columnid)
                                    {            
                                      if(ketq_a_kb[1] != "" && $.isNumeric(ketq_a_kb[1]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giaiba") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                        
                                    if(arrayIds_id == columnid)
                                    {    
                                      if(b2[0] != "" && b2[1] != "" 
                                      && $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giaitu") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                        
                                    if(arrayIds_id == columnid)
                                    {     
                                      if(b2[0] != "" && b2[1] != "" && b2[2] != "" 
                                      && b2[3] != "" && b2[4] != "" && b2[5] != "" 
                                      && b2[6] != "" && $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true 
                                      && $.isNumeric(b2[2]) == true && $.isNumeric(b2[3]) == true && $.isNumeric(b2[4]) == true 
                                      && $.isNumeric(b2[5]) == true && $.isNumeric(b2[6]) == true ) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giainam") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                        
                                    if(arrayIds_id == columnid)
                                    {    
                                      if(b2[0] != "" && $.isNumeric(b2[0]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      { 
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }

                              else if(ketq_a_kb[0] == "giaisau") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                        
                                    if(arrayIds_id == columnid)
                                    {    
                                      if(b2[0] != "" && b2[1] != "" && b2[2] != "" 
                                      && $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active");
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giaibay") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                    if(arrayIds_id == columnid)
                                    {    
                                      if(b2[0] != "" && $.isNumeric(b2[0]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active"); 
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                              else if(ketq_a_kb[0] == "giaitam") 
                              {
                                for(var d = 0; d < arrayIds.length; d++)
                                {
                                  var arrayIds_b = arrayIds[d],
                                      arrayIds_b_c = arrayIds_b.split(":");
                                      
                                  if(arrayIds_b_c[0] == ketq_a_kb[0])
                                  {
                                    var arrayIds_id = "#" + arrayIds_b_c[1],
                                        b1 = ketq_a_kb[1],
                                        b2 = b1.split("-");
                                        
                                    if(arrayIds_id == columnid)
                                    {    
                                      if(b2[0] != "" && $.isNumeric(b2[0]) == true) 
                                      {
                                        $(arrayIds_id).html("<div class=\"div-status\">"+ketq_a_kb[1]+"</div>");
                                        $(arrayIds_id).addClass("active");
                                      }
                                      else 
                                      {
                                        $(arrayIds_id).removeClass("active"); 
                                        $(arrayIds_id).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                                      }
                                    }
                                  }
                                }
                              }
                              
                            }
                          }
                        },
                        error:function(){
                          alert("Invalid Request");
                        }
                      });
                    }, 6000);
                  }
                } 
                autoSetStatusMTMN();
              });
            //]]>  
            </script>
          ';
        }
        
        return $html;
      }
      
      function findValueNeed($id, $arr) 
      {
        $comp = "false";
        for ($i = 0; $i < count($arr); $i++) 
        {
          if ($id == $arr[$i]) 
          {
            $comp = "true";
          }
        }
        
        return $comp;
      }
      
      function checkdate($table_name, $date1)
      {
        global $wpob;
        $wpob = new Auto_Get_Number();
        $conn = new Connection();
        $str_conn = $conn->_Connection();
      
        $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
        mysql_select_db($str_conn[3],$con);
        mysql_query("SET NAMES 'utf8'");
        mysql_query("SHOW FULL PROCESSLIST");
        
        $query = mysql_query("SELECT date FROM ".$table_name." WHERE date='".$date1."'") or die(mysql_error());
        $row = mysql_fetch_array($query);
        if(isset($row['date']) && !empty($row['date'])){
          return 'true';
        }
        
        return 'false';
      }
    
      function getId($table_name, $date1)
      {
        global $wpob;
        $wpob = new Auto_Get_Number();
        $conn = new Connection();
        $str_conn = $conn->_Connection();
        
        $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
        mysql_select_db($str_conn[3],$con);
        mysql_query("SET NAMES 'utf8'");
        mysql_query("SHOW FULL PROCESSLIST");
        
        $query = mysql_query("SELECT id FROM ".$table_name." WHERE date='".$date1."'") or die(mysql_error());
        $row = mysql_fetch_array($query);
        return $row['id'];
      }
      
      function getKetQuaXoSo($link1)
      {
        $result='';
        $file1='';
        if($file1=fopen($link1,'rb'))
        {
          while (!feof($file1)) 
          {
            $result.= fread($file1, 8192);
          }
          fclose($file1);
        }
        
        return $result;
      }
      
      function getArrayFromExplode1($link1)
      {
        global $wpob;
        $wpob = new Auto_Get_Number();
        $kqxs = $wpob->getKetQuaXoSo($link1);
        $ex_array_1 = explode(";", $kqxs);
        
        return $ex_array_1;
      }
      
      function loadTinhAll()
      {
        return array(
                      "1"=>"TP. HCM","2"=>"Đồng Tháp","3"=>"Cà Mau","7"=>"Bến Tre","8"=>"Vũng Tàu","9"=>"Bạc Liêu","10"=>"Đồng Nai","11"=>"Cần Thơ","12"=>"Sóc Trăng","13"=>"Tây Ninh","14"=>"An Giang","15"=>"Bình Thuận","16"=>"Vĩnh Long","17"=>"Bình Dương","18"=>"Trà Vinh","19"=>"Long An","20"=>"Hậu Giang","21"=>"Bình Phước","22"=>"Tiền Giang","23"=>"Kiên Giang","24"=>"Đà Lạt","26"=>"Thừa T. Huế","27"=>"Phú Yên","28"=>"Quảng Nam","29"=>"Đắk Lắk","30"=>"Đà Nẵng","31"=>"Khánh Hòa","32"=>"Bình Định","33"=>"Quảng Trị","34"=>"Quảng Bình","35"=>"Gia Lai","36"=>"Ninh Thuận","37"=>"Quảng Ngãi","38"=>"Đắk Nông","39"=>"Kon Tum","46"=>"Miền bắc","47"=>"Miền bắc","48"=>"Miền bắc","49"=>"Miền bắc","50"=>"Miền bắc","51"=>"Miền bắc"
                    );
      }
      
      function loadKeyAll()
      {
        return array(
                  "1"=>"hcm","2"=>"dt","3"=>"cm","7"=>"bt","8"=>"vt","9"=>"bl","10"=>"dn","11"=>"ct","12"=>"st","13"=>"tn","14"=>"ag","15"=>"bth","16"=>"vl","17"=>"bd","18"=>"tv","19"=>"la","20"=>"hg","21"=>"bp","22"=>"tg","23"=>"kg","24"=>"dl","26"=>"tth","27"=>"py","28"=>"qna","29"=>"dlc","30"=>"dna","31"=>"kh","32"=>"bdi","33"=>"qt","34"=>"qb","35"=>"gl","36"=>"nt","37"=>"qng","38"=>"dno","39"=>"kt","46"=>"mb","47"=>"mb","48"=>"mb","49"=>"mb","50"=>"mb","51"=>"mb"
                );
      }
      
      function findTenOrKeyTinh($id, $array)
      {
        $tenkey = "";
        foreach($array as $key=>$value)
        {
          if((int)$key == (int)$id)
          {
            $tenkey = $value;
          }
        }
        
        return $tenkey;
      }
      
      function findIdArray($id, $array)
      {
        $result = '';
        for($i=0; $i < count($array); $i++)
        {
          if((int)$id == (int)$array[$i])
          {
            $result = $array[$i];
          }
        }
        
        return $result;
      }
      
      function renderArray($link1, $key)
      {
        $result = array(); 
        $rearray = array(); 
        global $wpob;
        $wpob = new Auto_Get_Number();
        $array = $wpob->getArrayFromExplode1($link1);
        $exmt = explode("=", $array[1]);
        $ids = explode(",", str_replace('"', '', $exmt[1]));
        $countIds = count($ids);
        
        if($countIds >= 2)
        {
          for($i=0; $i<$countIds; $i++)
          {
            $rearray[$i][] = $array[0]; 
            $rearray[$i][] = trim(str_replace('"', '', $array[1])); 
            $rearray[$i][] = $array[2]; 
            $rearray[$i][] = $array[3];
            
            switch($i)
            {
              case 0:
                for($k0=5; $k0<=22; $k0++)
                {
                  $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k0]));
                }
              break;
              
              case 1:
                for($k1=23; $k1<=41; $k1++)
                {
                  $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k1]));
                }
              break;
              
              case 2:
                for($k2=42; $k2<=60; $k2++)
                {
                  $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k2]));
                }
              break;
              
              case 3:
                for($k3=61; $k3<=78; $k3++)
                {
                  $rearray[$i][] = trim(str_replace(array('kqxs["', '"]', '"'), array('', '', ''), $array[$k3]));
                }
              break;
            }
            
            $keytinh = $wpob->findTenOrKeyTinh($ids[$i], $key);
            
            $gdb = ''; $g1 = ''; $g2 = ''; $g3 = ''; 
            $g4 = ''; $g5 = ''; $g6 = ''; $g7 = ''; 
            $g8 = ''; $tdttt4 = ''; $tdt123_1 = ''; 
            $tdt123_2 = ''; $tdt123_3 = '';
            
            foreach($rearray[$i] as $arr)
            {
              $ex_arr = explode('=', $arr);
              
              if($ex_arr[0] != '' && 
              $ex_arr[0] != 'runtt' &&  
              $ex_arr[0] != 'listtinhnew' &&
              $ex_arr[0] != 'newtime' && 
              $ex_arr[0] != 'delay')
              {
                $exmatinh = explode('_', $ex_arr[0]);
                switch($exmatinh[1])
                {
                  case 'Gdb':
                    $gdb = $ex_arr[1];
                  break;
                  
                  case 'G1':
                    $g1 = $ex_arr[1];
                  break;
                  
                  case 'G2':
                    $g2 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G3':
                    $g3 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G4':
                    $g4 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G5':
                    $g5 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G6':
                    $g6 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G7':
                    $g7 .= '-'.$ex_arr[1];
                  break;
                  
                  case 'G8':
                    $g8 .='-'.$ex_arr[1];
                  break;
                }
              }
            }
            $num = 1;
            $result[$keytinh] = array("dacbiet" => $gdb, "giainhat" => $g1, "giainhi" => substr($g2, $num), "giaiba" => substr($g3, $num), "giaitu" => substr($g4, $num), "giainam" => substr($g5, $num), "giaisau" => substr($g6, $num), "giaibay" => substr($g7, $num), "giaitam" => substr($g8, $num));
            
            unset($gdb); unset($g1); unset($g2); unset($g3); 
            unset($g4); unset($g5); unset($g6); unset($g7); 
            unset($g8);
          }
        }
        else
        {
          $rearray = array();
          
          for($re=0; $re<=31; $re++)
          {
            $rearray[] = $array[$re];
          }
          
          $keytinh = $wpob->findTenOrKeyTinh($ids[0], $key);
            
          $gdb = ''; $g1 = ''; $g2 = ''; $g3 = ''; 
          $g4 = ''; $g5 = ''; $g6 = ''; $g7 = ''; 
          $g8 = ''; $tdttt4 = ''; $tdt123_1 = ''; 
          $tdt123_2 = ''; $tdt123_3 = '';
          
          foreach($rearray as $arrs)
          {
            $arr =  trim(str_replace(array('"', 'kqxs[', ']'), array('', '', ''), $arrs));
            $ex_arr = explode('=', $arr);
            
            if($ex_arr[0] != '' && 
            $ex_arr[0] != 'runtt' &&  
            $ex_arr[0] != 'listtinhnew' &&
            $ex_arr[0] != 'newtime' && 
            $ex_arr[0] != 'delay')
            {
            
              $exmatinh = explode('_', $ex_arr[0]);
              switch($exmatinh[1])
              {
                case 'Gdb':
                  $gdb = $ex_arr[1];
                break;
                
                case 'G1':
                  $g1 = $ex_arr[1];
                break;
                
                case 'G2':
                  $g2 .= '-'.$ex_arr[1];
                break;
                
                case 'G3':
                  $g3 .= '-'.$ex_arr[1];
                break;
                
                case 'G4':
                  $g4 .= '-'.$ex_arr[1];
                break;
                
                case 'G5':
                  $g5 .= '-'.$ex_arr[1];
                break;
                
                case 'G6':
                  $g6 .= '-'.$ex_arr[1];
                break;
                
                case 'G7':
                  $g7 .= '-'.$ex_arr[1];
                break;
              }
              
            }
          }
          
          $num = 1;
          $result[$keytinh] = array("dacbiet" => $gdb, "giainhat" => $g1, "giainhi" => substr($g2, $num), "giaiba" => substr($g3, $num), "giaitu" => substr($g4, $num), "giainam" => substr($g5, $num), "giaisau" => substr($g6, $num), "giaibay" => substr($g7, $num));
          
          unset($gdb); unset($g1); unset($g2); unset($g3); 
          unset($g4); unset($g5); unset($g6); unset($g7);
        }
        
        return $result;
      }
      
      function getKetQuaMienBacTrungNam($link1)
      {
        $_SESSION["count_connect"] = $_SESSION["count_connect"] + 1;
        if($_SESSION["count_connect"] >= 5)
        {
          $_SESSION["count_connect"] = 1;
        }
        
        global $wpob;
        $wpob = new Auto_Get_Number();
        $key = $wpob->loadKeyAll();
        return $wpob->renderArray($link1, $key);
      }
      
      function redirectConnect()
      {
        $url1 = 'http://www.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
                  http://www.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
                  http://www.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
                  
        $url2 = 'http://ww1.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
                http://ww1.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
                http://ww1.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
                
        $url3 = 'http://ww2.minhngoc.net.vn/xstt/MB/MB.php?visit=0 , 
                http://ww2.minhngoc.net.vn/xstt/MT/MT.php?visit=0 , 
                http://ww2.minhngoc.net.vn/xstt/MN/MN.php?visit=0';
                
        $url4 = 'http://www.minhchinh.com/xstt/MB.php?visit=0 , 
                http://www.minhchinh.com/xstt/MT.php?visit=0 , 
                http://www.minhchinh.com/xstt/MN.php?visit=0';
                  
        if(isset($_SESSION["count_connect"]) && !empty($_SESSION["count_connect"]))
        {
          switch($_SESSION["count_connect"])
          {
            case 1:
              $url = $url1;
            break;
            
            case 2: 
              $url = $url2;
            break;
            
            case 3:
              $url = $url3;
            break;
            
            case 4:
              $url = $url4;
            break;
            
            default:
              $url = $url4;
              $_SESSION["count_connect"] = 1;
            break;
          }
          
          return $url;
        }
        else
        {
          return $url4;
        }
      }
    }
  }

  function short_code_clock_xoso($atts)
  {
    $attributes = shortcode_atts( array( 'statusupdate' => '', 'statusclock' => '' ), $atts );
    if(isset($attributes['statusclock']) && $attributes['statusclock'] == 'true') 
    {
      global $wpob;
      $wpob = new Auto_Get_Number();
      $date = current_time('mysql');
      $fd1 = explode('-', substr($date,0,10));
      $fd2 = $fd1[2].'-'.$fd1[1].'-'.$fd1[0];
      echo '
        <p>
          <span id="timerxs" class="clock-timerxs"></span>
          <span class="clock-sp">'.$fd2.'</span>
          <span id="timerxs-msg" class="clock-timerxs-msg"> Chưa đến giờ cập nhật kết quả xổ số!</span>
        </p>
        <script type="text/javascript">
          var myVar = setInterval(function(){clockTimer()}, 1000);
          function clockTimer() {
            var dd = new Date(),
                a = dd.toLocaleTimeString(),
                c = 1;
            document.getElementById("timerxs").innerHTML = a;
            if( a >= "16:10:00" && a <= "22:00:00" ){
              if(c == 1) {
                // document.getElementById("timerxs-msg").innerHTML = "";
                // c++;
              }
            }
          }
        </script>
      ';
    }
  }
  add_shortcode('clockxoso', 'short_code_clock_xoso');
  
  function short_code_danhsach_tructiep($atts)
  {
    global $wpob; 
    $wpob = new Auto_Get_Number(); 
    
    $link = explode(',', $wpob->redirectConnect());
    $datas = $wpob->getKetQuaMienBacTrungNam(trim('http://www.minhchinh.com/xstt/MB.php?visit=0'));
    $datamb = $datas["mb"];  
    
    $html = '';
    
    $attributes = shortcode_atts( array( 'template' => '', 'status' => '' ), $atts );
    
    if(isset($attributes['status']) && $attributes['status'] == 'true')
    {
      $data = array('xo-so-mien-bac   , Kết quả xổ số Miền bắc'    =>  array( 'xo-so-truyen-thong , Kết quả xổ số Truyền thống'),
                    'xo-so-mien-trung , Kết quả xổ số Miền trung'  =>  array( 'xo-so-binh-dinh , Kết quả xổ số Bình định', 
                                                                              'xo-so-da-nang , Kết quả xổ số Đà nẵng', 
                                                                              'xo-so-dac-lac , Kết quả xổ số Đắc lắc', 
                                                                              'xo-so-dac-nong , Kết quả xổ số Đắc nông', 
                                                                              'xo-so-gia-lai , Kết quả xổ số Gia lai', 
                                                                              'xo-so-khanh-hoa , Kết quả xổ số Khánh hòa', 
                                                                              'xo-so-kon-tum , Kết quả xổ số Kon tum', 
                                                                              'xo-so-ninh-thuan , Kết quả xổ số Ninh thuận', 
                                                                              'xo-so-phu-yen , Kết quả xổ số Phú yên', 
                                                                              'xo-so-quang-binh , Kết quả xổ số Quảng bình', 
                                                                              'xo-so-quang-ngai , Kết quả xổ số Quảng ngãi', 
                                                                              'xo-so-quang-nam , Kết quả xổ số Quảng nam', 
                                                                              'xo-so-quang-tri , Kết quả xổ số Quảng trị', 
                                                                              'xo-so-thua-thien-hue , Kết quả xổ số Thừa thiên huế'),
                    'xo-so-mien-nam   , Kết quả xổ số Miền nam'    =>  array( 'xo-so-an-giang , Kết quả xổ số An giang',
                                                                              'xo-so-bac-lieu , Kết quả xổ số Bạc liêu',
                                                                              'xo-so-ben-tre , Kết quả xổ số Bến tre',
                                                                              'xo-so-binh-duong , Kết quả xổ số Bình dương',
                                                                              'xo-so-binh-phuoc , Kết quả xổ số Bình phước',
                                                                              'xo-so-binh-thuan , Kết quả xổ số Bình thuận',
                                                                              'xo-so-ca-mau , Kết quả xổ số Cà mau',
                                                                              'xo-so-can-tho , Kết quả xổ số Cần thơ',
                                                                              'xo-so-da-lat , Kết quả xổ số Đà lạt',
                                                                              'xo-so-dong-nai , Kết quả xổ số Đồng nai',
                                                                              'xo-so-dong-thap , Kết quả xổ số Đồng tháp',
                                                                              'xo-so-hau-giang , Kết quả xổ số Hậu giang',
                                                                              'xo-so-ho-chi-minh , Kết quả xổ số Hồ chí minh',
                                                                              'xo-so-kien-giang , Kết quả xổ số Kiên giang',
                                                                              'xo-so-long-an , Kết quả xổ số Long an',
                                                                              'xo-so-soc-trang , Kết quả xổ số Sóc trăng',
                                                                              'xo-so-tay-ninh , Kết quả xổ số Tây ninh',
                                                                              'xo-so-tien-giang , Kết quả xổ số Tiền giang',
                                                                              'xo-so-tra-vinh , Kết quả xổ số Trà Vinh',
                                                                              'xo-so-vinh-long , Kết quả xổ số Vĩnh long',
                                                                              'xo-so-vung-tau , Kết quả xổ số Vũng tàu'));
                  
    
      $html .= '<div id="danhsach_tructiep" class="danhsach_tructiep '.(!empty($attributes["template"]) ? $attributes["template"] : "").'">';
        $html .= '<div class="danhsach_tructiep_inner">';
          $html .= '<div class="note-msg">';
            $html .= '<div class="note-cho note-format"><label for="dang-cho">Đang chờ</label><img height="11px" src="'.plugins_url('/images/loading-hoz-waiting.gif', __FILE__).'" alt="Đang chờ"></div>';
            $html .= '<div class="note-do note-format"><label for="dang-do">Đang đổ</label><img height="11px" src="'.plugins_url('/images/loading-hoz-pedding.gif', __FILE__).'" alt="Đang đổ"></div>';
            $html .= '<div class="note-moi note-format"><label for="moi-nhat">Mới nhất</label><img height="11px" src="'.plugins_url('/images/icon_new32_new.gif', __FILE__).'" alt="Mới nhất"></div>';
          $html .= '</div>';
          $html .= '<ul id="nav_links_tructiep" class="nav_links_tructiep">'; 
          $j = 0;
          foreach($data as $key=>$value) 
          {
            $key = explode(',', $key);
            $html .= '<li class="item parent active" id="'.str_replace(" ", "", $key[0]).'-'.$j.'"><a href="'.get_site_url().'/'.$key[0].'"><span class="span-title">'.$key[1].'</span></a>';
              $html .= '<ul class="nav_link_sub" id="nav_link_sub_'.$j.'">';
              for($i=0; $i < count($value); $i++)
              {
                $var = explode(',', $value[$i]);
                $html .= '<li class="item_sub"><a href="'.get_site_url().'/'.$var[0].'"><span class="span-title">'.$var[1].'</span><span id="'.trim($var[0]).'" class="span-status"></span></a></li>';
              }  
              $html .= '</ul>';
            $html .= '</li>';
            $j++;
          }
          $html .= '</ul>';
        $html .= '</div>';
      $html .= '</div>';
      
      $html .= '
      <script type="text/javascript">
      //<![CDATA[
        jQuery(document).ready(function($){
          $("#xo-so-mien-bac-0").on( "click", function() {
            $("#nav_link_sub_0").toggle( "slow", function() {
              if($("#xo-so-mien-bac-0").hasClass( "active" )) {
                $("#xo-so-mien-bac-0").removeClass("active");
              }
              else {
                $("#xo-so-mien-bac-0").addClass("active");
              }
            });
          });
          
          $("#xo-so-mien-trung-1").on( "click", function() {
            $("#nav_link_sub_1").toggle( "slow", function() { 
              if($("#xo-so-mien-trung-1").hasClass( "active" )) {
                $("#xo-so-mien-trung-1").removeClass("active");
              }
              else {
                $("#xo-so-mien-trung-1").addClass("active");
              }
            });
          });
          
          $("#xo-so-mien-nam-2").on("click", function() {
            $("#nav_link_sub_2").toggle("slow", function() { 
              if($("#xo-so-mien-nam-2").hasClass( "active" )) {
                $("#xo-so-mien-nam-2").removeClass("active");
              }
              else {
                $("#xo-so-mien-nam-2").addClass("active");
              }
            });
          });  
          
          var khu_vuc = [ "xo-so-truyen-thong#18:05:00#mb",    "xo-so-binh-dinh#17:15:00#bdi",  
                          "xo-so-da-nang#17:15:00#dna",        "xo-so-dac-lac#17:15:00#dlc", 
                          "xo-so-dac-nong#17:15:00#dno",       "xo-so-gia-lai#17:15:00#gl", 
                          "xo-so-khanh-hoa#17:13:00#kh",       "xo-so-kon-tum#17:15:00#kt", 
                          "xo-so-ninh-thuan#17:15:00#nt",      "xo-so-phu-yen#17:15:00#py", 
                          "xo-so-quang-binh#17:15:00#qb",      "xo-so-quang-ngai#17:15:00#qng", 
                          "xo-so-quang-nam#17:15:00#qna",      "xo-so-quang-tri#17:15:00#qt", 
                          "xo-so-thua-thien-hue#17:15:00#tth", "xo-so-an-giang#16:14:00#ag", 
                          "xo-so-bac-lieu#16:14:00#bl",        "xo-so-ben-tre#16:14:00#bt", 
                          "xo-so-binh-duong#16:14:00#bd",      "xo-so-binh-phuoc#16:14:00#bp", 
                          "xo-so-binh-thuan#16:14:00#bth",     "xo-so-ca-mau#16:14:00#cm", 
                          "xo-so-can-tho#16:15:00#ct",         "xo-so-da-lat#16:14:00#dl", 
                          "xo-so-dong-nai#16:14:00#dn",        "xo-so-dong-thap#16:14:00#dt", 
                          "xo-so-hau-giang#16:14:00#hg",       "xo-so-ho-chi-minh#16:14:00#hcm", 
                          "xo-so-kien-giang#16:14:00#kg",      "xo-so-long-an#16:14:00#la", 
                          "xo-so-soc-trang#16:14:00#st",       "xo-so-tay-ninh#16:14:00#tn", 
                          "xo-so-tien-giang#16:14:00#tg",      "xo-so-tra-vinh#16:14:00#tv", 
                          "xo-so-vinh-long#16:14:00#vl",       "xo-so-vung-tau#16:14:00#vt"
                        ];  
          
          function autoSet(time) 
          {
            $.ajax({
              type:"get",
              url:"'.plugins_url('/mienbac.php', __FILE__).'",
              success:function(responsemb){
                var ketq  = JSON.parse("[" + responsemb + "]");
                
                var idelement = "#xo-so-truyen-thong",
                    day = new Date(),
                    timec = day.toLocaleTimeString();
                
                var kv = khu_vuc[0],
                    kv_time = kv.split("#"),
                    trim_kv_time = $.trim(kv_time[0]),
                    trim_time = $.trim(kv_time[1]);
                    
                var checker_timec = checkAMorPM(timec);
                
                if(checker_timec.trim() == "PM") 
                {
                  if((trim_kv_time == "xo-so-truyen-thong") && (timec >= trim_time)) 
                  {
                    var db1 = ketq[0],
                    db2 = db1.split(":"),
                    dacbiet = db2[1],
                
                    gn1 = ketq[1],
                    gn2 = gn1.split(":"),
                    giainhat = gn2[1],
                
                    gni1 = ketq[2],
                    gni2 = gni1.split(":"),
                    giainhi = gni2[1],
                    giainhisp = giainhi.split("-"),
                
                    gb1 = ketq[3],
                    gb2 = gb1.split(":"),
                    giaiba = gb2[1],
                    giaibasp = giaiba.split("-"),
                
                    gt1 = ketq[4],
                    gt2 = gt1.split(":"),
                    giaitu = gt2[1],
                    giaitusp = giaitu.split("-"),
                
                    gna1 = ketq[5],
                    gna2 = gna1.split(":"),
                    giainam = gna2[1],
                    giainamsp = giainam.split("-"),
                
                    gs1 = ketq[6],
                    gs2 = gs1.split(":"),
                    giaisau = gs2[1],
                    giaisausp = giaisau.split("-"),
                
                    gbb1 = ketq[7],
                    gbb2 = gbb1.split(":"),
                    giaibay = gbb2[1],
                    giaibaysp = giaibay.split("-");
                    
                    if(
                      $.isNumeric(dacbiet) == true && 
                      $.isNumeric(giainhat) == true && 
                      $.isNumeric(giainhisp[0]) == true && $.isNumeric(giainhisp[1]) == true && 
                      $.isNumeric(giaibasp[0]) == true && $.isNumeric(giaibasp[1]) == true && $.isNumeric(giaibasp[2]) == true && $.isNumeric(giaibasp[3]) == true && $.isNumeric(giaibasp[4]) == true && $.isNumeric(giaibasp[5]) == true && 
                      $.isNumeric(giaitusp[0]) == true &&  $.isNumeric(giaitusp[1]) == true &&  $.isNumeric(giaitusp[2]) == true &&  $.isNumeric(giaitusp[3]) == true &&  
                      $.isNumeric(giainamsp[0]) == true &&  $.isNumeric(giainamsp[1]) == true &&  $.isNumeric(giainamsp[2]) == true &&  $.isNumeric(giainamsp[3]) == true &&  $.isNumeric(giainamsp[4]) == true &&  $.isNumeric(giainamsp[5]) == true &&  
                      $.isNumeric(giaisausp[0]) == true &&  $.isNumeric(giaisausp[1]) == true &&  $.isNumeric(giaisausp[2]) == true &&  
                      $.isNumeric(giaibaysp[0]) == true && $.isNumeric(giaibaysp[1]) == true && $.isNumeric(giaibaysp[2]) == true && $.isNumeric(giaibaysp[3]) == true
                    )
                    {
                      $(idelement).addClass("span-new");
                      $(idelement).removeClass("span-pedding");
                      $(idelement).removeClass("span-waiting");
                    }
                    else
                    {
                      $(idelement).addClass("span-pedding");
                      $(idelement).removeClass("span-new");
                      $(idelement).removeClass("span-waiting");
                    }
                  }
                  else 
                  {
                    $(idelement).addClass("span-waiting");
                    $(idelement).removeClass("span-pedding");
                    $(idelement).removeClass("span-new");
                  }
                }
                else 
                {
                  $(idelement).addClass("span-waiting");
                  $(idelement).removeClass("span-pedding");
                  $(idelement).removeClass("span-new");
                }
              },
              error:function(){
                alert("Invalid Request");
              }
            });  
            
            var mien = ["'.plugins_url('/mientrung.php', __FILE__).'", "'.plugins_url('/miennam.php', __FILE__).'"];
            
            for(var m = 0; m < mien.length; m++)
            {
              $.ajax({
                type:"get",
                url:mien[m],
                success:function(responsemt){
                  var idele = "";
                  var day = new Date();
                  var timec = day.toLocaleTimeString();
                  var checker_timec = checkAMorPM(timec);
                  var trim_time = "";  
                  
                  var mt1 = responsemt.split("][");
                  var vi = 0;
                  for(var t1 = 0; t1 < mt1.length; t1++)
                  {
                    var mt2 = mt1[t1].split("$");
                    var a1 = mt2[0].replace("#","").trim();
                    
                    for(var t2 = 0; t2 < khu_vuc.length; t2++)
                    {
                      var it = khu_vuc[t2].split("#");
                      
                      if(a1 == it[2])  
                      {
                        if(vi == 0)
                        {
                          idele += it[0];
                        }
                        else
                        {
                          idele += "#" + it[0];
                        }
                        vi++;
                      }
                    }
                  }
                  
                  for(var t1 = 0; t1 < mt1.length; t1++)
                  {   
                    var mt2 = mt1[t1].split("$");
                    var a1 = mt2[0].replace("#","");
                  
                    for(var v = 0; v < khu_vuc.length; v++)
                    {
                      var kv = khu_vuc[v].split("#");
                      if(a1 == kv[2]) 
                      {
                        trim_time = kv[1];
                      }
                    }
                    
                    if(checker_timec.trim() == "PM") 
                    {  
                      if(timec >= trim_time) 
                      {  
                        var result = "";
                        var mtrim = mt2[1].split("#");
                        
                        var kq = JSON.parse("[" + mtrim[0] + "]");
                        var c1 = 0;
                        for(var k = 0; k < kq.length; k++)
                        {
                          var k1 = kq[k].split(":");
                          
                          if(k1[0] == "dacbiet" || k1[0] == "giainhat" || k1[0] == "giainhi" || k1[0] == "giainam" || k1[0] == "giaibay" || k1[0] == "giaitam")
                          {
                            if(c1 == 0)
                            {
                              result +=  k1[1];
                            }
                            else
                            {
                              result += "#" + k1[1];
                            }
                            
                            c1++;
                          }
                          else
                          {
                            var k2 = k1[1].split("-");
                            for(var j = 0; j < k2.length; j++)
                            {
                              result += "#" + k2[j];
                            }
                          }
                        }
                        
                        var result1 = result.split("#");
                        if(
                          $.isNumeric(result1[0]) == true &&
                          $.isNumeric(result1[1]) == true &&
                          $.isNumeric(result1[2]) == true &&
                          $.isNumeric(result1[3]) == true &&
                          $.isNumeric(result1[4]) == true &&
                          $.isNumeric(result1[5]) == true &&
                          $.isNumeric(result1[6]) == true &&
                          $.isNumeric(result1[7]) == true &&
                          $.isNumeric(result1[8]) == true &&
                          $.isNumeric(result1[9]) == true &&
                          $.isNumeric(result1[10]) == true &&
                          $.isNumeric(result1[11]) == true &&
                          $.isNumeric(result1[12]) == true &&
                          $.isNumeric(result1[13]) == true &&
                          $.isNumeric(result1[14]) == true &&
                          $.isNumeric(result1[15]) == true &&
                          $.isNumeric(result1[16]) == true &&
                          $.isNumeric(result1[17]) == true
                        )
                        {
                          var sl = idele.split("#");
                          for(var i = 0; i < sl.length; i++)
                          {
                            var ide = "#" + sl[i];
                            $(ide).addClass("span-new");
                            $(ide).removeClass("span-pedding");
                            $(ide).removeClass("span-waiting");
                          }
                        }
                        else
                        { 
                          var sl = idele.split("#");
                          for(var i = 0; i < sl.length; i++)
                          {
                            var ide = "#" + sl[i];
                            $(ide).addClass("span-pedding");
                            $(ide).removeClass("span-new");
                            $(ide).removeClass("span-waiting");
                          } 
                        }
                      }
                      else 
                      {
                        var sl = idele.split("#");
                        for(var i = 0; i < sl.length; i++)
                        {
                          var ide = "#" + sl[i];  
                          $(ide).addClass("span-waiting");
                          $(ide).removeClass("span-pedding");
                          $(ide).removeClass("span-new");
                        }
                      }
                    }
                    else
                    {
                      var sl = idele.split("#");
                      for(var i = 0; i < sl.length; i++)
                      {
                        var ide = "#" + sl[i];  
                        $(ide).addClass("span-waiting");
                        $(ide).removeClass("span-pedding");
                        $(ide).removeClass("span-new");
                      }
                    }
                  }
                },
                error:function(){
                  alert("Invalid Request");
                }
              }); 
            }
          }
          
          //5:28:16 PM
          function checkAMorPM(time) 
          {
            var d = new Date();
            var hr = d.getHours();
            var ampm = hr < 12 ? " AM" : " PM";
            
            return ampm;
          }
          
          var day = new Date(),
              time = day.toLocaleTimeString();
          if(time >= "16:10:00" && time <= "23:59:00") {
            autoSet(time);
          }
          else {
            $("#nav_link_sub_2").hide();
            $("#nav_link_sub_1").hide();
            $("#nav_link_sub_0").hide();
            
            $("#xo-so-mien-bac-0").removeClass("active");
            $("#xo-so-mien-nam-2").removeClass("active");
            $("#xo-so-mien-trung-1").removeClass("active");
            
            autoSet(time);
          }
          
          var auto_load = setInterval(function(){
            var day = new Date(),
                time = day.toLocaleTimeString();
            if(time >= "16:10:00" && time <= "23:59:00") 
            {
              if($("#nav_link_sub_2").hide()) {
                $("#nav_link_sub_2").show();
              }
              
              if($("#nav_link_sub_1").hide()) {
                $("#nav_link_sub_1").show();
              }
              
              if($("#nav_link_sub_0").hide()) {
                $("#nav_link_sub_0").show();
              }
              
              if(!$("#xo-so-mien-bac-0").hasClass("active")) {
                $("#xo-so-mien-bac-0").addClass("active");
              }
              
              if(!$("#xo-so-mien-nam-2").hasClass("active")) {
                $("#xo-so-mien-nam-2").addClass("active");
              }
              
              if(!$("#xo-so-mien-trung-1").hasClass("active")) {
                $("#xo-so-mien-trung-1").addClass("active");
              }
              autoSet(time);
            }
          }, 6000);
        });
      //]]>  
      </script>
      ';
    }
    
    return $html;
  }
  add_shortcode('danhsach_tructiep', 'short_code_danhsach_tructiep');
  
  function short_code_thongbao_tructiep($atts)
  {
    $attributes = shortcode_atts( array( 'template' => '', 'status' => '' ), $atts );
  }
  add_shortcode('thongbao_tructiep', 'short_code_thongbao_tructiep');
  
  function short_code_build_xoso_mienbac() 
  { 
    $extend = new functionExtends();
    $serviceid = $extend->getAllResultValues('service','status', '1','id','asc','10');
    $sub = $extend->getResultValues('subscriber', 'msisdn', $_SESSION['msisdn'], 'id','1');
    global $wpob;
    $wpob = new Auto_Get_Number();
    
    $link = explode(',', $wpob->redirectConnect());
    $datas = $wpob->getKetQuaMienBacTrungNam(trim('http://www.minhchinh.com/xstt/MB.php?visit=0'));
    $data = $datas["mb"]; 
    if(isset($_SESSION['msisdn']) && $_SESSION['msisdn'] != 'null'){
        $html .= 'Xin chào '.$_SESSION['msisdn'] .'</br>';
        $command = 'REGISTER';
        for ($i =0; $i< count($serviceid); $i++){
          $keyService = $extend->encrypt($serviceid[$i]['id'], 'chotsogiovang');
          $html .='<a href="http://giovangchotso.vn/account.php?id='.$keyService.'&cmd='.$command.'">Đăng ký gói "'.$serviceid[$i]['service_name'].'"</a><br/>';
        }
        $usingServices = $extend->getAllResultValuesRecord('service_subscriber_mapping','subscriber_id', $sub['id'], 'is_active', '1','id','ASC','1');
        if(count($usingServices) > 0){
            $command = 'CANCEL';
            for ($i =0; $i< count($usingServices); $i++){
                $keyService = $extend->encrypt($usingServices[$i]['service_id'], 'chotsogiovang');
                $service_name = $extend->getAllResultValues('service','id', $usingServices[$i]['service_id'],'id','asc','10');
                $html .='<a href="http://giovangchotso.vn/account.php?id='.$keyService.'&cmd='.$command.'">Hủy gói "'.$service_name[$i]['service_name'].'"</a><br/>';
            } 
        }
    }else{
        $html .='Chưa nhận diện được thuê bao';
    }
    if(isset($_SESSION['responseToUser'])){
        $html .= $_SESSION['responseToUser'];
        unset($_SESSION['responseToUser']);
    }
    $html .= '<div class="xoso-mienbac"><div class="xoso-mienbac-inner">
              <table class="table">
                <tbody>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-dacbiet">Đặc Biệt</th>
                <td id="item-td-mb-0" class="last item-td item-td-mb item-td-dacbiet">'.$data['dacbiet'].'</td>
              </tr>';
              
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giainhat">Giải Nhất</th>
                <td id="item-td-mb-1" class="last item-td item-td-mb item-td-giainhat">'.$data['giainhat'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giainhi">Giải Nhì</th>
                <td id="item-td-mb-2" class="last item-td item-td-mb item-td-giainhi">'.$data['giainhi'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giaiba">Giải Ba</th>
                <td id="item-td-mb-3" class="last item-td item-td-mb item-td-giaiba">'.$data['giaiba'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giaitu">Giải Tư</th>
                <td id="item-td-mb-4" class="last item-td item-td-mb item-td-giaitu">'.$data['giaitu'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giainam">Giải Năm</th>
                <td id="item-td-mb-5" class="last item-td item-td-mb item-td-giainam">'.$data['giainam'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giaisau">Giải Sáu</th>
                <td id="item-td-mb-6" class="last item-td item-td-mb item-td-giaisau">'.$data['giaisau'].'</td>
              </tr>';
    
    $html .= '<tr>
                <th class="first item-th item-th-mb item-th-giaibay">Giải Bảy</th>
                <td id="item-td-mb-7" class="last item-td item-td-mb item-td-giaibay">'.$data['giaibay'].'</td>
              </tr>';
    
    $html .= '    </tbody>
                </table>
              </div>
            </div>';
    
    if(!empty($data['list2']) && !empty($data['list2']) && !empty($data['list1']))
    {
      $lototructiep = explode('-', $data['list1']); $j = 0;
      $html .= '<div class="xoso-mienbac"><div class="xoso-mienbac-inner loto-tructiep"><table class="table"><tbody>';
      for($i = 0; $i < count($lototructiep); $i++) 
      {
        if($j == 0 || $j%9 == 0) 
        {
          $html .= '<tr>';
          $first = 'first-lt';
        }  
        $html .= '<td class="loto-tt '.$first.'">'.$lototructiep[$i].'</td>';  
        unset($first);
        if(($j+1)%9 == 0 || ($j+1) == count($lototructiep))
        {
          $html .= '</tr>';
        }
        $j++;
      }
      $html .= '</tbody></table></div></div>';
      
      $t = 0;
      $html .= '<div class="xoso-mienbac"><div class="xoso-mienbac-inner dau-loto"><table class="table"><tbody>';
      foreach($data['list2'] as $key=>$value) 
      {
        $html .= '<tr>
                    <th class="first item-th item-th-mb  item-th-'.$t.'">'.$key.'</th>
                    <td id="item-td-dau-mb-'.$t.'" class="last item-td item-td-mb item-td-'.$t.'">'.str_replace(';', '-', $value).'</td>
                  </tr>';
        $t++;
      } unset($t);
      $html .= '</tbody></table></div></div>';
      
      $html .= '<div class="xoso-mienbac"><div class="xoso-mienbac-inner dau-loto dit-loto"><table class="table"><tbody>';
      $t = 0;
      foreach($data['list3'] as $key=>$value) 
      {
        $html .= '<tr>
                    <th class="first item-th item-th-mb item-th-'.$t.'">'.$key.'</th>
                    <td id="item-td-dit-mb-'.$t.'" class="last item-td item-td-mb item-td-'.$t.'">'.str_replace(';', '-', $value).'</td>
                  </tr>';
        $t++;
      } unset($t);
      $html .= '</tbody></table></div></div>';
    }
    
    $html .= '<script type="text/javascript">
      //<![CDATA[
        jQuery(document).ready(function($){
          
          //5:28:16 PM
          function checkAMorPM(time) 
          {
            var d = new Date();
            var hr = d.getHours();
            var ampm = hr < 12 ? " AM" : " PM";
            
            return ampm;
          }
          
          function autoSetStatus() 
          {
            var day = new Date();
            var time = day.toLocaleTimeString();
          
            $("div.xoso-mienbac td.item-td").each(function(){
              var element = $(this).attr("id"),
                  trim_element = $.trim(element),
                  idelement = "#" + trim_element;
                  
              var checker_timec = checkAMorPM(time);
              
              var data1 = $(idelement).html(),
                  data2 = data1.split("-"),
                  totals = data2.length;
              
              $(idelement).addClass("active");   
              if(checker_timec.trim() == "PM" && time >= "13:10:00") 
              {
                $(idelement).removeClass("active"); 
                if(totals == 1) 
                {
                  $(idelement).html("<div class=\"div-status\"><span class=\"span-status span-waiting\"></span></div>");
                }
                else 
                {
                  var htmlStr = "<div class=\"div-status\">";
                  for(var i=0; i <= totals; i++) 
                  {
                    htmlStr += "<span class=\"span-status span-waiting\"></span>";
                  }
                  htmlStr += "</div>";
                  $(idelement).html(htmlStr);
                }
              }
            });  
            
            var checker_timec = checkAMorPM(time);
            if(checker_timec.trim() == "PM" && time >= "13:10:00")
            {
              var arrayIds = ["dacbiet:item-td-mb-0", "giainhat:item-td-mb-1", "giainhi:item-td-mb-2", "giaiba:item-td-mb-3", "giaitu:item-td-mb-4", "giainam:item-td-mb-5", "giaisau:item-td-mb-6", "giaibay:item-td-mb-7"],
              
                  arrayIdsDau = ["0:item-td-dau-mb-0", "1:item-td-dau-mb-1", "2:item-td-dau-mb-2", "3:item-td-dau-mb-3", "4:item-td-dau-mb-4", "5:item-td-dau-mb-5", "6:item-td-dau-mb-6", "7:item-td-dau-mb-7", "8:item-td-dau-mb-8", "9:item-td-dau-mb-9"],
              
                  arrayIdsDit = ["0:item-td-dit-mb-0", "1:item-td-dit-mb-1", "2:item-td-dit-mb-2", "3:item-td-dit-mb-3", "4:item-td-dit-mb-4", "5:item-td-dit-mb-5", "6:item-td-dit-mb-6", "7:item-td-dit-mb-7", "8:item-td-dit-mb-8", "9:item-td-dit-mb-9"];
                  
              var auto_load = setInterval(function(){
                $.ajax({
                  type:"get",
                  url:"'.plugins_url('/mienbac.php', __FILE__).'",
                  success:function(response){
                  
                    var ketq  = JSON.parse("[" + response + "]");
                    
                    for(var i = 0; i < ketq.length; i++)
                    {
                    
                      var a = ketq[i],
                          kb = a.split(":");
                      
                      if(kb[0] == "dacbiet") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                            var e = "#" + c[1];
                            if(kb[1] != "" && $.isNumeric(kb[1]) == true)
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giainhat") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1];
                            
                            if(kb[1] != "" && $.isNumeric(kb[1]) == true)
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      } 
                      
                      else if(kb[0] == "giainhi") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giaiba") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && b2[2] != "" && b2[3] != "" && b2[4] != "" && b2[5] != "" 
                            && $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true && $.isNumeric(b2[3]) == true && $.isNumeric(b2[4]) == true && $.isNumeric(b2[5]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giaitu") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && b2[2] != "" && b2[3] != "" && 
                            $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true && $.isNumeric(b2[3]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giainam") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && b2[2] != "" && b2[3] != "" && b2[4] != "" && b2[5] != "" && 
                            $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true && $.isNumeric(b2[3]) == true && $.isNumeric(b2[4]) == true && $.isNumeric(b2[5]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            { 
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giaisau") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && b2[2] != "" && 
                            $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active");
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                      else if(kb[0] == "giaibay") 
                      {
                        for(var d = 0; d < arrayIds.length; d++)
                        {
                        
                          var b = arrayIds[d],
                              c = b.split(":");
                              
                          if(c[0] == kb[0])
                          {
                          
                            var e = "#" + c[1],
                                b1 = kb[1],
                                b2 = b1.split("-");
                                
                            if(b2[0] != "" && b2[1] != "" && b2[2] != "" && b2[3] != "" && 
                            $.isNumeric(b2[0]) == true && $.isNumeric(b2[1]) == true && $.isNumeric(b2[2]) == true && $.isNumeric(b2[3]) == true) 
                            {
                              $(e).html("<div class=\"div-status\">"+kb[1]+"</div>");
                              $(e).addClass("active");
                            }
                            else 
                            {
                              $(e).removeClass("active"); 
                              $(e).html("<div class=\"div-status\"><span class=\"span-status span-pedding\"></span></div>");
                            }
                          }
                        }
                      }
                      
                    }
                  },
                  error:function(){
                    alert("Invalid Request");
                  }
                });
              }, 6000);
            }
          }
          autoSetStatus();
        });
      //]]>  
      </script>';
    
    return $html;
  }
  add_shortcode('xoso_mienbac', 'short_code_build_xoso_mienbac');

  function short_code_build_xoso_mientrung($atts) 
  {
    global $wpob;
    $wpob = new Auto_Get_Number();
    
    $link = explode(',', $wpob->redirectConnect());
    $data = $wpob->getKetQuaMienBacTrungNam(trim('http://www.minhchinh.com/xstt/MT.php?visit=0'));
    
    $keytt = array();
    foreach($data as $kdata=>$da)
    {
      $keytt[] = $kdata;
    }
    
    $attributes = shortcode_atts( array( 'attribute' => '', 'count' => '', 'tructiep' => '' ), $atts );
    if(isset($attributes['attribute']) && !empty($attributes['attribute']))
    {
      $attrs = explode(',', $attributes['attribute']);
      $tructiep = $attributes['tructiep'];
      if(!$tructiep || $tructiep == "true") 
      {
        for($i = 0; $i < count($keytt); $i++)
        {
          if(($i + 1) == count($keytt))
          {
            $start_js = 'true';
          }
          else
          {
            $start_js = 'false';
          }
          $key = str_replace(' ', '', strtolower($keytt[$i]));
          $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "mientrung", $start_js);
        }
      }
      else
      {
        if(count($attrs) == 1) 
        {
          $start_js = 'false';
          $key = str_replace(' ', '', strtolower($attributes['attribute']));
          $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "mientrung", $start_js);
        }
        else 
        {
          if(count($attrs) > 1) 
          {
            if(isset($attributes['count']) && !empty($attributes['count'])) 
            {
              $count = (int)$attributes['count'];
            }
            else 
            {
              $count = count($attrs);
            }
            
            for($i=0; $i < $count; $i++) 
            {
              $start_js = 'false';
              $key = str_replace(' ', '', strtolower($attrs[$i]));
              $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "mientrung", $start_js);  
            }
          }
        }
      }
    }
    else 
    {
      foreach($data as $key=>$value) 
      {
        $start_js = 'false';
        $html .= $wpob->renderHtmlFromKey($value, $key, $keytt, "mientrung", $start_js);  
      }
    }
    
    return $html;
  }
  add_shortcode('xoso_mientrung', 'short_code_build_xoso_mientrung');

  function short_code_build_xoso_miennam($atts) 
  {
    global $wpob;
    $wpob = new Auto_Get_Number();
    
    $link = explode(',', $wpob->redirectConnect());
    $data = $wpob->getKetQuaMienBacTrungNam(trim('http://www.minhchinh.com/xstt/MN.php?visit=0'));
    
    
    $keytt = array();
    foreach($data as $kdata=>$da)
    {
      $keytt[] = $kdata;
    }
    
    $attributes = shortcode_atts( array( 'attribute' => '', 'count' => '', 'tructiep' => '' ), $atts );
    if(isset($attributes['attribute']) && !empty($attributes['attribute']))
    {
      $attrs = explode(',', $attributes['attribute']);
      $tructiep = $attributes['tructiep'];
      if(!$tructiep || $tructiep == "true") 
      {
        for($i = 0; $i < count($keytt); $i++)
        {
          if(($i + 1) == count($keytt))
          {
            $start_js = 'true';
          }
          else
          {
            $start_js = 'false';
          }
          $key = str_replace(' ', '', strtolower($keytt[$i]));
          $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "miennam", $start_js);
        }
      }
      else
      {
        if(count($attrs) == 1) 
        {
          $start_js = 'false';
          $key = str_replace(' ', '', strtolower($attributes['attribute']));
          $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "miennam", $start_js);
        }
        else 
        {
          if(count($attrs) > 1) 
          {
            if(isset($attributes['count']) && !empty($attributes['count'])) 
            {
              $count = (int)$attributes['count'];
            }
            else 
            {
              $count = count($attrs);
            }
            
            for($i=0; $i < $count; $i++) 
            {
              $start_js = 'false';
              $key = str_replace(' ', '', strtolower($attrs[$i]));
              $html .= $wpob->renderHtmlFromKey($data[$key], $key, $keytt, "miennam", $start_js);  
            }
          }
        }
      }
    }
    else 
    {
      foreach($data as $key=>$value) 
      {
        $start_js = 'false';
        $html .= $wpob->renderHtmlFromKey($value, $key, $keytt, "miennam", $start_js); 
      }
    }
    
    return $html;  
  }
  add_shortcode('xoso_miennam', 'short_code_build_xoso_miennam');

  function wpob_load() 
  {
    global $wpob;
    $wpob = new Auto_Get_Number();
  }
  add_action( 'plugins_loaded', 'wpob_load' );
?>

<?php
  /**
  *
  * Manage Plugin Auto_Get_Numbers
  *
  **/
  
  function add_css_js_auto_get_number_admin()
  {
    $admin = new functionAdmin();
    echo $admin->__getCssJs();
  }
  add_action( 'admin_head', 'add_css_js_auto_get_number_admin' );

  function auto_get_numbers_settings() 
  {
    //register our settings
    register_setting( 'auto-get-numbers-settings-group', 'select_status_of_update' );
  }
  //call register settings function
  add_action( 'admin_init', 'auto_get_numbers_settings' );

  function update_numbers() 
  {
    //register our settings
    register_setting( 'update-numbers-settings-group', 'update_numbers_1' );
  }
  //call register settings function
  add_action( 'admin_init', 'update_numbers' );

  function to_date() 
  {
    //register our settings
    register_setting( 'update-numbers-settings-group', 'to_date' );
  }
  //call register settings function
  add_action( 'admin_init', 'to_date' );

  function from_date() 
  {
    //register our settings
    register_setting( 'update-numbers-settings-group', 'from_date' );
  }
  //call register settings function
  add_action( 'admin_init', 'from_date' );

  function create_menu_xoso() 
  {
    //create new top-level menu
    add_menu_page('Quản Lý Xổ Số', 'Quản Lý Xổ Số', 'administrator', __FILE__, 'auto_get_numbers_page', plugins_url('/images/icon-xoso-1.gif', __FILE__));
    
  }
  // create custom plugin settings menu
  add_action('admin_menu', 'create_menu_xoso');
  
  function create_opening_tag($value) 
  { 
		$group_class = "";
		if (isset($value['grouping'])) 
    {
			$group_class = "suf-grouping-rhs";
		}
		echo '<div class="suf-section fix">'."\n";
		if ($group_class != "") 
    {
			echo "<div class='$group_class fix'>\n";
		}
		if (isset($value['name'])) 
    {
			echo "<h3>" . $value['name'] . "</h3>\n";
		}
		if (isset($value['desc']) && !(isset($value['type']) && $value['type'] == 'checkbox')) 
    {
			echo $value['desc']."<br />";
		}
		if (isset($value['note'])) 
    {
			echo "<span class=\"note\">".$value['note']."</span><br />";
		}
	}
  
  function create_opening_tag_text($value) 
  { 
		$group_class = "";
		if (isset($value['grouping'])) 
    {
			$group_class = "suf-grouping-rhs";
		}
		echo '<div class="suf-section fix">'."\n";
		if ($group_class != "") 
    {
			echo "<div class='$group_class fix'>\n";
		}
		if (isset($value['name'])) 
    {
			echo "<h3>" . $value['name'] . "</h3>\n";
		}
		if (isset($value['desc']) && !(isset($value['type']) && $value['type'] == 'checkbox')) 
    {
			echo $value['desc'];
		}
		if (isset($value['note'])) 
    {
			echo "<span class=\"note\">".$value['note']."</span><br />";
		}
	}

	function create_closing_tag($value) 
  { 
		if (isset($value['grouping'])) 
    {
			echo "</div>\n";
		}
		echo "</div>\n";
	}
  
  function create_section_for_text($value, $placeholder) 
  { 
		create_opening_tag_text($value);
		$text = "";
		if (get_option($value['id']) === FALSE) 
    {
			$text = $value['std'];
		}
		else 
    {
			$text = get_option($value['id']);
		}
	 
		echo '<input class="date-picker" type="text" id="'.$value['id'].'" placeholder="'.$placeholder.'" name="'.$value['id'].'" value="'.$text.'" />'."\n";
		create_closing_tag($value);
	}
  
  function create_section_for_textarea($value)
  { 
		create_opening_tag($value);
		echo '<textarea name="'.$value['id'].'" type="textarea" cols="" rows="">'."\n";
		if ( get_option( $value['id'] ) != "") 
    {
			echo get_option( $value['id'] );
		}
		else 
    {
			echo $value['std'];
		}
		echo '</textarea>';
		create_closing_tag($value);
	}
	
	function create_section_for_radio($value) 
  { 
		create_opening_tag($value);
		foreach ($value['options'] as $option_value => $option_text) 
    {
			$checked = ' ';
			if (get_option($value['id']) == $option_value) 
      {
				$checked = ' checked="checked" ';
			}
			else if (get_option($value['id']) === FALSE && $value['std'] == $option_value)
      {
				$checked = ' checked="checked" ';
			}
			else 
      {
				$checked = ' ';
			}
			echo '<div class="mnt-radio"><input type="radio" name="'.$value['id'].'" value="'.$option_value.'" '.$checked."/>".$option_text."</div>\n";
		}
		create_closing_tag($value);
	}
  
  function create_section_for_color_picker($value) 
  { 
		create_opening_tag($value);
		$color_value = "";
		if (get_option($value['id']) === FALSE) 
    {
			$color_value = $value['std'];
		}
		else 
    {
			$color_value = get_option($value['id']);
		}
	 
		echo '<div class="color-picker">'."\n";
		echo '<input type="text" id="'.$value['id'].'" name="'.$value['id'].'" value="'.$color_value.'" class="color" />';
		echo ' « Click to select color<br/>'."\n";
		echo "<strong>Default: <font color='".$value['std']."'> ".$value['std']."</font></strong>";
		echo " (You can copy and paste this into the box above)\n";
		echo "</div>\n";
		create_closing_tag($value);
  }
  
  function create_section_for_multi_select($value) 
  { 
		create_opening_tag($value);
		echo '<ul class="mnt-checklist" id="'.$value['id'].'" >'."\n";
		foreach ($value['options'] as $option_value => $option_list) 
    {
			$checked = " ";
			if (get_option($value['id']."_".$option_value)) 
      {
				$checked = " checked='checked' ";
			}
			echo "<li>\n";
			echo '<input type="checkbox" name="'.$value['id']."_".$option_value.'" value="true" '.$checked.' class="depth-'.($option_list['depth']+1).'" />'.$option_list['title']."\n";
			echo "</li>\n";
		}
		echo "</ul>\n";
		create_closing_tag($value);
	}
	
	function create_section_for_category_select($page_section,$value) 
  { 
		create_opening_tag($value);
		$all_categoris='';
    echo '<div class="wrap" id="'.$value['id'].'" >'."\n";
    echo '<h2>Theme Options</h2> '."\n" .'
      <p><strong>'.$page_section.':</strong></p>';
      echo "<select id='".$value['id']."' class='post_form' name='".$value['id']."' value='true'>\n";
      echo "<option id='all' value=''>All</option>";
      
    foreach ($value['options'] as $option_value => $option_list) 
    {
      $checked = ' ';
      echo 'value_id=' . $value['id'] .' value_id=' . get_option($value['id']) . ' options_value=' . $option_value;
      if (get_option($value['id']) == $option_value) 
      {
        $checked = ' checked="checked" ';
      }
      else if (get_option($value['id']) === FALSE && $value['std'] == $option_value)
      {
        $checked = ' checked="checked" ';
      }
      else 
      {
        $checked = '';
      }
      echo '<option value="'.$option_list['name'].'" class="level-0" '.$checked.' number="'.($option_list['number']).'" />'.$option_list['name']."</option>\n";
    }	
    echo "</select>\n </div>";
		create_closing_tag($value);
	}
  
  function createTableForMPS()
  {
    $extends = new functionExtends();
    $table = array('tbl_mps_api_subscribe', 'tbl_mps_api_getcontent', 'tbl_mps_api_receiveresult', 'tbl_sms_gw_molistener', 'tbl_section', 'tbl_province', 'tbl_result', 'tbl_customer');
    $query  = array(" id int(11) NOT NULL AUTO_INCREMENT,
                      username varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Username that CP provide to MPS',
                      password varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password that CP provide to MPS',
                      serviceID varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ServiceID provided by MPS',
                      msisdn varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Phone number',
                      chargetime varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Subscribed Time, format yyyyMMddHHmmss',
                      params tinyint(4) DEFAULT NULL COMMENT 'Parameter(s) for function\n0: Subscribe\n1: Unsubscribe\n2: pending\n3: Restore service',
                      mode varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For test function.\n- CHECK: check CP System with this content without any notify to user\n- REAL: get content for user\n',
                      amount int(11) DEFAULT NULL COMMENT 'Register fee which MPS has charged user',
                      command varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Register command',
                      status tinyint(1) DEFAULT NULL COMMENT 'Trạng thái thành công',
                      notes text COLLATE utf8_unicode_ci COMMENT 'Ghi chú nếu cần',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",  
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      username varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Username that CP provide to MPS',
                      password varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password that CP provide to MPS',
                      serviceID varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ServiceID provided by MPS',
                      msisdn varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Phone number',
                      params text COLLATE utf8_unicode_ci COMMENT 'Parameter(s) for function',
                      mode varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For test function.\n- CHECK: check CP System with this content without any notify to user\n- REAL: get content for user\n',
                      amount int(11) DEFAULT NULL COMMENT 'Service fee which MPS has charged user',
                      status tinyint(1) DEFAULT NULL COMMENT 'Trạng thái thành công',
                      notes text COLLATE utf8_unicode_ci COMMENT 'Ghi chú nếu cần',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      username varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Username that CP provide to MPS',
                      password varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password that CP provide to MPS',
                      serviceID varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ServiceID provided by MPS',
                      msisdn varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Phone number',
                      chargetime varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Subscribed Time, format yyyyMMddHHmmss',
                      params tinyint(4) DEFAULT NULL COMMENT 'Parameter(s) for function\n0: Charge Success\n1: Charge failed',
                      mode varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For test function.\n- CHECK: check CP System with this content without any notify to user\n- REAL: get content for user\n',
                      amount int(11) DEFAULT NULL COMMENT 'Service fee which MPS has charged user',
                      status tinyint(1) DEFAULT NULL COMMENT 'Trạng thái thành công',
                      notes text COLLATE utf8_unicode_ci COMMENT 'Ghi chú nếu cần',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      username varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Username that CP provide to MPS',
                      password varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password that CP provide to MPS',
                      source varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Phone number which sms send from',
                      dest varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Shortcode which SMS Send to',
                      content text COLLATE utf8_unicode_ci COMMENT 'Content of SMS\n',
                      status tinyint(1) DEFAULT NULL COMMENT 'Trạng thái thành công',
                      notes text COLLATE utf8_unicode_ci COMMENT 'Ghi chú nếu cần',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      name_section varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      description_section text COLLATE utf8_unicode_ci,
                      status_section tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: Hủy\n1: Đã kích hoạt\n2: Chờ kích hoạt',
                      key_section varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã miền\nMiền Bắc: MB\nMiền Trung: MT\nMiền Nam: MN',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      section_id int(11) NOT NULL,
                      name varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên tỉnh',
                      code varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã tỉnh',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id),
                      KEY fk_tbl_province_tbl_section1_idx (section_id)",
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      province_id int(11) NOT NULL,
                      content_result text COLLATE utf8_unicode_ci NOT NULL,
                      status_result tinyint(2) NOT NULL DEFAULT '1' COMMENT '0:\n1:\n2:\n',
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id),
                      KEY fk_tbl_result_tbl_province1_idx (province_id)",  
                      
                    " id int(11) NOT NULL AUTO_INCREMENT,
                      fullname_customer varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      username_customer varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      password_customer varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      mobile_customer varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      payments_customer varchar(250) COLLATE utf8_unicode_ci NOT NULL,
                      status_customer tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: Hủy\n1: Đã kích hoạt\n2: Chờ kích hoạt',
                      note_customer text COLLATE utf8_unicode_ci,
                      created_at datetime DEFAULT NULL,
                      updated_at datetime DEFAULT NULL,
                      PRIMARY KEY (id)",  
                );
    
    for($i = 0; $i < count($table); $i++)
    {
      $extends->createTable($table[$i], $query[$i]);
    }
    
    $keyns = keyNameSection();
    $fieldns = 'name_section, status_section, key_section, created_at';
    $status = 1;
    
    $created_at = current_time('mysql');
    foreach($keyns as $key=>$val)
    {
      if(checkExistValueField($key, "key_section", $table[4]) == "false")
      {
        $valuens = "'".$val."', ".$status.", '".$key."', '".$created_at."'";
        getInsertValues($table[4], $fieldns, $valuens);
      }
      else
      {
        $paramns = array('name_section'=>$val, 'status_section'=>$status, 'key_section'=>$key, 'created_at'=>$created_at);
        $idn = getValueOfField($table[4], "id", "key_section", $key);
        $wheren = 'id='.$idn;
        getUpdateValue($table[4], $paramns, $wheren);
      }
    }
    
    $keynp = keyNameProvince();
    $fieldps = 'section_id, name, code, created_at';
    foreach($keynp as $kkey=>$val)
    {
      $exkey  = explode(":", $kkey);
      $parkey = $exkey[0]; 
      $childkey = $exkey[1];
      
      $section_id = getValueOfField($table[4], "id", "key_section", $parkey);
      if(checkExistValueField($childkey, "code", $table[5]) == "false")
      {
        $valueps = $section_id.", '".$val."', '".$childkey."', '".$created_at."'";
        getInsertValues($table[5], $fieldps, $valueps);
      }
      else
      {
        $paramps = array('section_id'=>$section_id, 'name'=>$val, 'code'=>$childkey, 'created_at'=>$created_at);
        $idp = getValueOfField($table[5], "id", "code", $childkey);
        $wherep = 'id='.$idp;
        getUpdateValue($table[5], $paramps, $wherep);
      }
    }
  }
  
  add_action( 'admin_init', 'createTableForMPS' );
  
  function getValueOfField($table_name, $field, $field_condition, $value)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    
    $query = mysql_query("SELECT ".$field." FROM ".$table_name." WHERE ".$field_condition."='".$value."'") or die(mysql_error());
    $row = mysql_fetch_array($query);
    
    return $row[$field];
  }
  
  function keyNameSection()
  {
    return array( "mb"=>"Miền Bắc", "mt"=>"Miền Trung", "mn"=>"Miền Nam"  );
  }
  
  function keyNameProvince()
  {
    return array( "mb:mb"   =>  "Miền Bắc",
                  
                  "mt:bdi"  =>  "Bình Định",        "mt:dna"  =>  "Đà Nẵng", 
                  "mt:dlc"  =>  "Đắc lắc",          "mt:dno"  =>  "Đắc Nông", 
                  "mt:gl"   =>  "Gia Lai",          "mt:kh"   =>  "Khánh Hòa", 
                  "mt:kt"   =>  "Kon Tum",          "mt:nt"   =>  "Ninh Thuận", 
                  "mt:py"   =>  "Phú Yên",          "mt:qb"   =>  "Quảng Bình", 
                  "mt:qng"  =>  "Quảng Ngãi",       "mt:qna"  =>  "Quảng Nam", 
                  "mt:qt"   =>  "Quảng Trị",        "mt:tth"  =>  "Thừa T. Huế",
                  
                  "mn:ag"   =>  "An Giang",         "mn:bl"   =>  "Bạc Liêu", 
                  "mn:btr"  =>  "Bến Tre",          "mn:bdu"  =>  "Bình Dương", 
                  "mn:bp"   =>  "Bình Phước",       "mn:bth"  =>  "Bình Thuận", 
                  "mn:cm"   =>  "Cà Mau",           "mn:ct"   =>  "Cần Thơ", 
                  "mn:dlt"  =>  "Đà Lạt",           "mn:dni"  =>  "Đồng Nai", 
                  "mn:dt"   =>  "Đồng Tháp",        "mn:hg"   =>  "Hậu Giang", 
                  "mn:hcm"  =>  "TP. Hồ Chí Minh",  "mn:kg"   =>  "Kiên Giang", 
                  "mn:la"   =>  "Long An",          "mn:st"   =>  "Sóc Trăng", 
                  "mn:tn"   =>  "Tây Ninh",         "mn:tg"   =>  "Tiền Giang", 
                  "mn:tv"   =>  "Trà Vinh",         "mn:vl"   =>  "Vĩnh Long", 
                  "mn:vt"   =>  "Vũng Tàu"
                );
  }
  
  function checkExistValueField($value, $field, $table)
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
  
    $con = mysql_connect($str_conn[0],$str_conn[1],$str_conn[2]) or die(mysql_error());
    mysql_select_db($str_conn[3],$con);
    mysql_query("SET NAMES 'utf8'");
    mysql_query("SHOW FULL PROCESSLIST");
    
    $query = mysql_query("SELECT ".$field." FROM ".$table." WHERE ".$field."='".$value."'") or die(mysql_error());
    $row = mysql_fetch_array($query);
    if(isset($row[$field]) && !empty($row[$field])){
      return 'true';
    }
    
    return 'false';
  }
  
  // $fields is string: $fields = 'field1, field2, ...'
  // $values is string: $values = '"field1", "field2", ...'
  function getInsertValues($table_name, $fields, $values) 
  {
    $conn = new Connection();
    $str_conn = $conn->_Connection();
    
    $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
    if ($conn->connect_error) 
    {
      die("Connection failed: " . $conn->connect_error);
    } 
    $conn->set_charset("utf8");
    $sql = "INSERT INTO ".$table_name."(".$fields.") VALUES (".$values.")";
    $conn->query($sql);

    $conn->close();
  }
  
  // $params is array: $params = array("$field1"=>"$value1", "$field2"=>"$value2", ...);
  // $where is string: $where = "$field = $value";
  function getUpdateValue($table_name, $params, $where)
  { 
    $append_fields = ''; $i = 0;
    foreach($params as $field=>$value)
    {
      if($field == 'status_section')
      {
        if($i == 0)
        {
          $append_fields .= $field."=".$value."";
        }
        else
        {
          $append_fields .= ", ".$field."=".$value."";
        }
      }
      else
      {
        if($i == 0)
        {
          $append_fields .= $field."='".$value."'";
        }
        else
        {
          $append_fields .= ", ".$field."='".$value."'";
        }
      }
      $i++;
    }
  
    $conn = new Connection();
    $str_conn = $conn->_Connection();

    $conn = new mysqli($str_conn[0], $str_conn[1], $str_conn[2], $str_conn[3]);
    if ($conn->connect_error) 
    {
      die("Connection failed: " . $conn->connect_error);
    }  
    $conn->set_charset("utf8");
    $sql = "UPDATE ".$table_name." SET ".$append_fields." WHERE ".$where;
    $conn->query($sql);

    $conn->close();
  }
  
  function auto_get_numbers_page() 
  {
    global $wpob;
    $wpob = new Auto_Get_Number();
    $extends = new functionExtends();
    
    $date = current_time('mysql');
    $fd1 = explode('-', substr($date,0,10));
    $fd2 = $fd1[2].'-'.$fd1[1].'-'.$fd1[0];
    
    echo '
    <div class="wrap">
      <div class="manage-xoso">
        <h2>Quản Lý Xổ Số</h2><hr>    
    ';
    
    echo '
        <p>
          <span class="formart-msg-1">
            <span id="timerxs" class="timerxs-clock"></span>
            <span id="timerxs-day" class="timerxs-day">'.$fd2.'</span>
          </span>  
          <span id="timerxs-msg" class="timerxs-msg formart-msg-2"> Chưa đến giờ cập nhật kết quả xổ số!</span>
        </p><br>
        ';
        
    echo '
        <form method="post" action="options.php"> 
          <div class="form-options">';
          
            $options = array("name"    => "<span>Cấu hình chế độ cập nhật kết quả xổ số</span>",
                      "id"      => "select_status_of_update",
                      "type"    => "radio",
                      "desc"    => "<span class=\"msg-span \">Vui lòng chọn chế độ cập nhật! </span><br>",
                      "options" => array("1" => "Tự động", "0" => "Thủ công"),
                      "std"     => "");
                      
            create_section_for_radio($options);
                
    echo  '</div>';
          settings_fields( 'auto-get-numbers-settings-group' );
          do_settings_sections( 'auto-get-numbers-settings-group' );
    echo '       
          <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Lưu cấu hình">
          </p>
        </form>';
    
    echo '
        <form method="post" id="updateForm">
          <div class="form-options">';
  echo     '<div class="box-option-top">';          
              $options = array("name"    => "<span>Cập nhật kết quả xổ số</span>",
                        "id"      => "update_numbers_1",
                        "type"    => "radio",
                        "desc"    => "<span class=\"msg-span\">Vui lòng chọn khu vực cập nhật! </span><br>",
                        "options" => array("xo-so-mien-bac" => "Miền Bắc", "xo-so-mien-trung" => "Miền Trung", "xo-so-mien-nam" => "Miền Nam"),
                        "std"     => "");
              create_section_for_radio($options);
  echo     '</div>';         
  echo     '<div class="box-option-bottom">
              <div class="box-option-bottom-left">';          
                $options = array(
                          "id"      => "from_date",
                          "type" => "text",
                          "desc"    => "<span class=\"msg-span\">Từ ngày: </span><hr>",
                          "std"     => "");
                create_section_for_text($options, "Xin vui lòng chọn ngày!");
    echo      '</div>';
    echo      '<div class="box-option-bottom-middle">';    
                $options = array(
                          "id"      => "to_date",
                          "type" => "text",
                          "desc"    => "<span class=\"msg-span\">Đến ngày: </span><hr>",
                          "std"     => "");
                create_section_for_text($options, "Xin vui lòng chọn ngày!");
    echo      '</div>';
    echo      '<div class="msg-screen-xs">
                <div class="msg-screen-xs-inner" id="message-proccessing">
                  <div id="message-proccessing-inner">
                  Vui lòng thiết lập các chức năng và click "Bắt đầu cập nhật" để tiến trình bắt đầu ...';

    echo      '   </div>
                </div>
                <input type="hidden" name="date_hidden_id" id="date_hidden_id">
              </div>';
    echo      '<div style="clear:both;"></div>
            </div>
          </div>';
          
          settings_fields( 'update-numbers-settings-group' );
          do_settings_sections( 'update-numbers-settings-group' );       
          
    echo '
          <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Bắt đầu cập nhật">
          </p>
        </form>
    ';

    $allresults = $extends->getAllResult("tbl_result", "created_at", "0, 20");
    $msgToolbar = $extends->getMessageToolbar("tbl_result", "created_at", count($allresults));
    
    $pagerHtml = $extends->getPagerHtml("tbl_result", "created_at");
    
    echo '
      <div class="manage-kqxs-grid">
        <div class="manage-kqxs-grid-inner">
          <h3><span>Kết quả xổ số</span></h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
              <div class="btn-toolbar btn-toolbar-top" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group" role="group" aria-label="First group">'.
                  $pagerHtml
              .'</div>
                <div class="search-background1"><label><img src="'.plugins_url('/images/load.gif', __FILE__).'" alt="loading..." /></label></div>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              <p class="help-block">'.$msgToolbar.'</p>
            </div>
          </div>
          
          <div class="table-responsive">
            <div id="table-responsive-inner">
              <table class="table table-bordered" id="table-ketqua-xoso">
                <thead>
                  <tr>
                    <th>#ID</th>
                    <th>Tỉnh</th>
                    <th>Đặc biệt</th>
                    <th>Giải nhất</th>
                    <th>Giải nhì</th>
                    <th>Giải ba</th>
                    <th>Giải tư</th>
                    <th>Giải năm</th>
                    <th>Giải sáu</th>
                    <th>Giải bảy</th>
                    <th>Giải tám</th>
                    <th>Ngày tạo</th>
                    <th>Công cụ</th>
                  </tr>
                </thead>
                <tbody>';
                
          foreach($allresults as $result)
          {  
            if(!empty($result)){
            echo '<tr>';
            foreach($result as $key=>$val)
            {
              if($key == "id")
              {  
                echo '<th scope="row">'.$val.'</th>';
              }
              else if($key == "content_result")
              {
                if(!empty($val))
                {
                  $vals = unserialize($val);
                  if(!empty($vals))
                  {
                    foreach($vals as $kdt=>$dt)
                    {
                      foreach($dt as $ke=>$va)
                      {
                        if($kdt == "mb")
                        {
                          if(!in_array($ke, array("Lototructiep", "Dauloto", "Ditloto")))
                          {
                            if($ke == "giaibay")
                            {
                              echo '<td>'.str_replace("-", "<br>", $va).'</td><td>Không có</td>';
                            }
                            else
                            {
                              echo '<td>'.str_replace("-", "<br>", $va).'</td>';
                            }
                          }
                        }
                        else
                        {
                          if(!in_array($ke, array("Lototructiep", "Dauloto", "Ditloto")))
                          {
                            echo '<td>'.str_replace("-", "<br>", $va).'</td>';
                          }
                        }
                      }
                    }
                  }
                }
              }
              else if($key != "status_result" && $key != "updated_at")
              {
                echo '<td>'.($key == "created_at" ? str_replace("-", "/", $val) : str_replace("-", "<br>", $val)).'</td>';
              }      
            }  
        
              echo '
                    <td class="xs-tools">
                    
                      <a href="#" title="Chi tiết">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><span class="glyphicon-class">Chi tiết</span><br>
                      </a>
                      <a href="#" title="Chi tiết">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span><span class="glyphicon-class">Chi tiết</span><br>
                      </a>
                      <a href="#" title="Sửa chữa">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="glyphicon-class">Sửa chữa</span>
                      </a>
                      
                    </td>
                    
                  </tr>';
            }        
      }           
      echo '            
                </tbody>
              </table>
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
              <div class="btn-toolbar btn-toolbar-bottom" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group" role="group" aria-label="First group">'.
                  $pagerHtml
              .'</div>
                <div class="search-background1"><label><img src="'.plugins_url('/images/load.gif', __FILE__).'" alt="loading..." /></label></div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              <p class="help-block">'.$msgToolbar.'</p>
            </div>
          </div>
          
        </div>
      </div>
    ';
    
    echo '    
      </div>
    </div>
    '; 
    echo '
    <script type="text/javascript">
    //<![CDATA[
      var myVar = setInterval(function(){clockTimer()}, 1000);
      function clockTimer() {
        var dd = new Date();
        var a = dd.toLocaleTimeString();
        document.getElementById("timerxs").innerHTML = a;
        var c = 1;
        if( a >= "18:00:00" && a <= "22:00:00" )
        {
          if(c == 1) 
          {
            // document.getElementById("timerxs-msg").innerHTML = "";
            // c++;
          }
        }
      }
    
      jQuery(document).ready(function($){
        
        var n = $("button.btn-number").length;
        $(".btn-group-carousel-inner").width(39 * (n/2) + 10);
        
        $(".btn-backward").click(function(){
          var ml = $(".btn-group-carousel-inner").css("margin-left").replace("px", "").replace("-", "");
          var maxml = 33 * (n/2) - 198;
          if(ml < maxml)
          {
            if(ml == 0)
            {
              ml = ml - 33;
            }
            else
            {
              ml = -ml - 33;
            }  
            
            $(".btn-group-carousel-inner").animate({
              marginLeft: ml  + "px"
            }, "slow");
          }
          else
          {
            bw = 0;
          }
        });
        
        $(".btn-backward-first").click(function(){
          var maxml = 33 * (n/2) - 198;  
          $(".btn-group-carousel-inner").animate({
              marginLeft: "-" + maxml  + "px"
            }, "slow");
        });
        
        $(".btn-forward").click(function(){
          var ml = $(".btn-group-carousel-inner").css("margin-left").replace("px", "").replace("-", "");
          if(ml > 0)
          {
            if(ml == 33)
            {
              ml = ml - 33;
            }
            else
            {
              ml = -ml + 33;
            } 
            $(".btn-group-carousel-inner").animate({
              marginLeft: ml + "px"
            }, "slow");
          }
        });
        
        $(".btn-forward-last").click(function(){
          var maxml = 33 * (n/2) - 198;  
          $(".btn-group-carousel-inner").animate({
              marginLeft: "0px"
            }, "slow");
        });
        
        function showLoader1(){
          $(".search-background1").fadeIn(1000);
        }
        
        function hideLoader1(){
          $(".search-background1").fadeOut(1000);
        }	
        
        $("div.btn-toolbar div.btn-group button.btn-number").click(function(){
          //show the loading bar
          showLoader1();	
          
          $("div.btn-toolbar div.btn-group button").css({"background-color" : ""});
          
          var pa = $(this).html();
         $("#table-responsive-inner").load("'.plugins_url('/resdata.php', __FILE__).'?page="+pa, hideLoader1);
          
          var curclass = $(this).attr("class");
          $(curclass.replace("btn btn-default btn-number ", ".")).css({"background-color" : "#d8d8d8"});
        });
        
        // by default first time this will execute
        $(".btn-1").css({"background-color" : "#d8d8d8"});


        showLoader1();
       $("#table-responsive-inner").load("'.plugins_url('/resdata.php', __FILE__).'?page=1",hideLoader1);
        
        var msg = "";
        function excutedUpdateFormAjax()
        {
          $.ajax({
            type: "POST",
            url:  "options.php",
            data: $("#updateForm").serialize(),
            error: function(data) 
            {
              msg += "Cập nhật trạng thái bị lỗi!... <br><br><br>";
              $("#message-proccessing").empty().append(msg);
              $("#message-proccessing").css("cursor", "progress");
              // document.location.reload();
            },
            success: function(data)
            {
              msg += "Cập nhật trạng thái thành công!... <br><br><br>";
              $("#message-proccessing").empty().append(msg);
              $("#message-proccessing").css("cursor", "");
              // document.location.reload();
            }
          });
        }
        
        $("#updateForm").submit(function() {
          
          $("#message-proccessing").empty().append("");
          $("#message-proccessing").css("cursor", "progress");
          var update_numbers_1 = $("input[name=update_numbers_1]:checked", "#updateForm").val();
          var from_date = $("#from_date").val(), 
              frarray = from_date.split("-");
          var to_date = $("#to_date").val(),
              toarray = to_date.split("-");
              
          if(parseInt(frarray[0]) == parseInt(toarray[0]) && parseInt(frarray[1]) == parseInt(toarray[1]))
          { 
            if(parseInt(frarray[2]) <= parseInt(toarray[2]))
            {
              var totals = parseInt(toarray[2]) - parseInt(frarray[2]);
              var date = "";
              var m = 0;
              for(var d = 0; d <= totals; d++)
              {
                if(m == 0)
                {
                  date +=  (parseInt(frarray[2]) + d )+"/"+toarray[1]+"/"+toarray[0];
                }
                else
                {
                  date +=  ","+(parseInt(frarray[2]) + d )+"/"+toarray[1]+"/"+toarray[0];
                }
                m++;
              }
                
              $("#date_hidden_id").val(date);
              $.ajax({
                type: "POST",
                url:  "'.plugins_url('/controller.php', __FILE__).'",
                data: $("#updateForm").serialize(),
                error: function(data) 
                {
                  msg += "Tiến trình cập nhật bị lỗi!... <br>";
                  $("#message-proccessing").empty().append(msg);
                  $("#message-proccessing").css("cursor", "progress");
                },
                success: function(data)
                {
                  excutedUpdateFormAjax();
                  // var msgdata = $("#date_hidden_id").val();
                  // var da1 = msgdata.split(",");
                  
                  var da1 = data.split(",");
                  
                  var totalda = da1.length;
                  for(var da2 = 0; da2 < totalda; da2++)
                  {
                    msg += "Cập nhật thành công kết quả xổ số "+update_numbers_1+" ngày: "+da1[da2]+"!... <br>";
                  }
                  msg += "<br> Cập nhật thành công "+totalda+" bản ghi!... <br>";
                  
                  // $("#message-proccessing").parent("div").append(data);
                  $("#message-proccessing").empty().append(msg);
                  $("#message-proccessing").css("cursor", "");
                  $("#message-proccessing").scrollTop(9999);
                }
              });
              date="";
            }
            else
            {
              msg += "Cập nhật (Từ ngày) phải nhỏ hơn cập nhật (Đến ngày) !... <br>";
              $("#message-proccessing").empty().append(msg);
            }
          }
          else
          {
            msg += "Hệ thống chỉ cập nhật kết quả xổ số từng tháng một !... <br>";
            $("#message-proccessing").empty().append(msg);
          }
          
          return false;
        });
      
        function strToDate(str) {
          try {
            var array = str.split("-");
            var year = parseInt(array[0]);
            var month = parseInt(array[1]);
            var day = array.length > 2? parseInt(array[2]): 1 ;
            if (year > 0 && month >= 0) {
              return new Date(year, month - 1, day);
            } else {
              return null;
            }
          } 
          catch (err) {};
        };

        function dateToStr(d) {
          var year = d.getFullYear();
          var month = d.getMonth();
          return year + "-" + (month + 1) + "-" + d.getDate();
        };

        $.fn.calendar = function (options) {
          var _this = this;
          var opts = $.extend({}, $.fn.calendar.defaults, options);
          var week = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
          var tHead = week.map(function (day) {
            return "<th>" + ((day == "Sat" || day == "Sun") ? "<span class=\"sat-sun\">" + day + "</span>" : day) + "</th>";
          }).join("");

          _this.init = function () {
            var tpl = "<table class=\"cal\">" +
            "<caption>" +
            "	<span class=\"prev\"><a href=\"javascript:void(0);\">&larr;</a></span>" +
            "	<span class=\"next\"><a href=\"javascript:void(0);\">&rarr;</a></span>" +
            "	<span class=\"month\"><span>" +
            "</caption>" +
            "<thead><tr>" +
            tHead +
            "</tr></thead>" +
            "<tbody>" +
            "</tbody>" + "</table>";
            var html = $(tpl);
            _this.append(html);
          };

          function daysInMonth(d) {
            var newDate = new Date(d);
            newDate.setMonth(newDate.getMonth() + 1);
            newDate.setDate(0);
            return newDate.getDate();
          }

          _this.update = function (date) {
            var mDate = new Date(date);
            mDate.setDate(1); 
            var day = mDate.getDay(); 
            mDate.setDate(mDate.getDate() - day) 

            function dateToTag(d) {
              var tag = $("<td><a href=\"javascript:void(0);\"></a></td>");
              var a = tag.find("a");
              a.text(d.getDate());
              a.data("date", dateToStr(d));
              if (date.getMonth() != d.getMonth()) {
                tag.addClass("off");
              } else if (_this.data("date") == a.data("date")) {
                tag.addClass("active");
                _this.data("date", dateToStr(d));
              }
              return tag;
            };

            var tBody = _this.find("tbody");
            tBody.empty(); 
            var cols = Math.ceil((day + daysInMonth(date))/7);
            for (var i = 0; i < cols; i++) {
              var tr = $("<tr></tr>");
              for (var j = 0; j < 7; j++, mDate.setDate(mDate.getDate() + 1)) {
                tr.append(dateToTag(mDate));
              }
              tBody.append(tr);
            }

      
            var monthStr = dateToStr(date).replace(/-\d+$/, "");
            _this.find(".month").text(monthStr)
          };

          _this.getCurrentDate = function () {
            return _this.data("date");
          }

          _this.init();

          var initDate = opts.date? opts.date: new Date();
          if (opts.date || !opts.picker) {
            _this.data("date", dateToStr(initDate));
          }
          _this.update(initDate);

          _this.delegate("tbody td", "click", function () {
            var $this = $(this);
            _this.find(".active").removeClass("active");
            $this.addClass("active");
            _this.data("date", $this.find("a").data("date"));
            if ($this.hasClass("off")) {
              _this.update(strToDate(_this.data("date")));
            }
            if (opts.picker) { 
              // _this.hide();
            }
          });

          function updateTable(monthOffset) {
            var date = strToDate(_this.find(".month").text());
            date.setMonth(date.getMonth() + monthOffset);
            _this.update(date);
          };

          _this.find(".next").click(function () {
            updateTable(1);

          });

          _this.find(".prev").click(function () {
            updateTable(-1);
          });

          return this;
        };

        $.fn.calendar.defaults = {
          date: new Date(),
          picker: false,
        };

        $.fn.datePicker = function () {
          var _this = this;
          var picker = $("<div></div>")
            .addClass("picker-container")
            .show()
            .calendar({"date": strToDate(_this.val()), "picker": true});

          _this.after(picker);

          // $("body").click(function () {
            // picker.hide();
          // });

          _this.click(function () {
            picker.show();
            return false;
          });

          picker.click(function () {
            _this.val(picker.getCurrentDate());
            return false;
          });

          return this;
        };

        $(window).load(function () {
          $(".jquery-calendar").each(function () {
            $(this).calendar();
          });
          $(".date-picker:text").each(function () {
            $(this).datePicker();
          });
        });
      });
    //]]>  
    </script>
    ';  
  }
?>

<?php 

  // add custom interval
  function cron_add_minute( $schedules ) 
  {
    // Adds once every minute to the existing schedules.
    $schedules['every_minute'] = array(
      'interval' => 60,
      'display' => __( 'Once Every Minute' )
    );
    
    return $schedules;
  }
  add_filter( 'cron_schedules', 'cron_add_minute' );

  // create a scheduled event (if it does not exist already)
  function cron_starter_activation() 
  {
    if(!wp_next_scheduled( 'auto_xoso_cron_job' )) 
    {  
      wp_schedule_event( time(), 'every_minute', 'auto_xoso_cron_job' );  
    }
  }
  // and make sure it's called whenever WordPress loads
  add_action('wp', 'cron_starter_activation');

  // here's the function we'd like to call with our cron job
  function auto_repeat_update() 
  {
    // do here what needs to be done automatically as per your schedule
    global $wpob;
    $wpob = new Auto_Get_Number();
  }
  // hook that function onto our scheduled event:
  add_action ('auto_xoso_cron_job', 'auto_repeat_update');
  
