﻿<?php 
  session_start();
  
  require_once 'connection.php';
  require_once 'extends.php';
  require( str_replace('\wp-content\plugins\auto-get-numbers', '', dirname(__FILE__)) . '/wp-load.php' );
  require( str_replace('\wp-content\plugins\auto-get-numbers', '', dirname(__FILE__)) . '/wp-includes/class-phpass.php' );
  
  // qXgplArb
  // c2c464c88b4695b327b1685d8b06ca04
  if(isset($_POST['username_customer']) && !empty($_POST['username_customer']) && isset($_POST['password_customer']) && !empty($_POST['password_customer']))
  {
    $extends = new functionExtends();
    $password = $extends->getValueOfField(" tbl_customer", "password_customer", "mobile_customer", $_POST['username_customer']);
    
    if(hash_equals($password,  md5($_POST['password_customer'])))
    {
      $_SESSION['login'] = "0";
      $_SESSION['username_customer'] = $_POST['username_customer'];
    }
    else
    {
      $_SESSION['login'] = "1";  
      unset($_SESSION['login']);
      session_unset();
      session_destroy();
    } 
  }
  
  if(isset($_POST['logout_customer']) && !empty($_POST['logout_customer']) && $_POST['logout_customer'] == 1)
  {
    $_SESSION['login'] = "1";    
    unset($_SESSION['login']);
    session_unset();
    session_destroy();
  }

  if(isset($_POST['redirect_to']) && !empty($_POST['redirect_to']))
  {
    $redirect = $_POST['redirect_to'];
  }
  else
  {
    // $redirect = 'http://giovangchotso.vn/';
    $redirect = '/';
  }  
  
  header('Location: '.$redirect);
  exit;
