<?php
define("USING_TYPE_REGISTER", 1);
define("PURCHASE_TYPE_NEW", 1);
define("PURCHASE_TYPE_CANCEL", 3);
define("VT_COMMANSD_REGISTER", 'REGISTER');
define("VT_COMMANSD_CANCEL", 'CANCEL');
define("VT_COMMANSD_CHARGE", 'CHARGE');
define("VT_ERR_ALREADY_CANCELED", '411');
define("VT_ERR_STILL_IN_CHARGING_PERIOD", '414');
define("VT_ERR_NONE", '0');
//DCV@12#34
    session_start();
    require_once 'wp-content/plugins/auto-get-numbers/extends.php';
    if (isset($_REQUEST['DATA'])) {
        $DATA = $_REQUEST['DATA'];
    }
    else {
        $DATA = $_REQUEST['data'];
    }
    if (isset($_REQUEST['SIG'])) {
        $SIG = $_REQUEST['SIG'];
    }
    else {
        $SIG = $_REQUEST['signature'];
    }
    $_SESSION['DATA'] = $DATA;
    $mobile = null;
    $extend = new functionExtends();
    $date = date('Y-m-d H:i:s');
    if(isset($DATA) && isset($SIG)){
        $original_parma = $extend->decryptParam($DATA, $SIG);
//			echo "\n".$original_parma;
        parse_str($original_parma, $param);
        $price = 0;
        if (isset($param["REQ"])) {
                $req = $param["REQ"];
        }
        else if (isset($param["req"])) {
                $req = $param["req"];
        }
        if (isset($param["RES"])) {
                $res = $param["RES"];
        }
        else if (isset($param["res"])) {
                $res = $param["res"];
        }
        if (isset($param["MOBILE"])) {
                $mobile = $param["MOBILE"];
        }
        else if (isset($param["mobile"])) {
                $mobile = $param["mobile"];
        }
        if (isset($param["PRICE"])) {
                $price = $param["PRICE"];
        }
        else if (isset($param["price"])) {
                $price = $param["price"];
        }
        if (isset($param["CMD"])) {
                $cmd = $param["CMD"];
        }
        else if (isset($param["cmd"])) {
                $cmd = $param["cmd"];
        }
        if (isset($param["SOURCE"])) {
                $source = $param["SOURCE"];
        }
        else if (isset($param["source"])) {
                $source = $param["source"];
        }
    }
    if($cmd == VT_COMMANSD_REGISTER){
        try { 
            $loginfo = "-----------------------------------------------------------------------------------------\n";
            $loginfo .= "Params: ".$original_parma."\n";
            $loginfo .= "Error_code: ".$res."\n";
            file_put_contents("/var/log/register_log_". date("Ymd").".log",  date("YmdHis"). "-". $loginfo."\n", FILE_APPEND | LOCK_EX);
            $registerOrder = $extend->getResultValues('registerOrder','req_id', $req,'id','1');
            $date = date('Y-m-d H:i:s');
            if($res == VT_ERR_NONE){
                // insert service_subscriber_mapping
                $field = "subscriber_id, service_id, is_active, channel, created_at, updated_at";
                $values = "'".$registerOrder['subscriber_id']."','".$registerOrder['service_id']."','1','".$registerOrder['channel']."','".$date."','".$date."'";
                $table = "service_subscriber_mapping ";
                $query = $extend->getInsertNewValues($table, $field, $values);
                // insert subscriber_transition
                $field = "subscriber_id, service_id, purchase_type, channel, status, created_at, updated_at";
                $values = "'".$registerOrder['subscriber_id']."','".$registerOrder['service_id']."','1','".$registerOrder['channel']."','1','".$date."','".$date."'";
                $table = "subscriber_transition ";
                $query = $extend->getInsertNewValues($table, $field, $values);
            }else{
                // insert subscriber_transition
                $field = "subscriber_id, service_id, purchase_type, channel, status, created_at, updated_at";
                $values = "'".$registerOrder['subscriber_id']."','".$registerOrder['service_id']."','1','".$registerOrder['channel']."','2','".$date."','".$date."'";
                $table = "subscriber_transition ";
                $query = $extend->getInsertNewValues($table, $field, $values);
            }
            $errorName = $extend->getErrorCodeName($res);
            unset($_SESSION['responseToUser']);
            $_SESSION['responseToUser'] =$errorName;
            header('Location: http://giovangchotso.vn/');exit();
            //return va thong bao ket qua, khong xu ly nghiep vu vi da goi WS
        }
        catch (Exception $e) {
                $errorName = "Thông báo: Hệ thống đang bận, Quý khách vui lòng thử lại sau!";
                unset($_SESSION['responseToUser']);
                $_SESSION['responseToUser'] =$errorName;
                header('Location: http://giovangchotso.vn/');exit();
        }
    }
    //huy dich vu - xu ly nghiep vu
    elseif($cmd == VT_COMMANSD_CANCEL){
        try {
            $createTime = new DateTime();
            $registerOrder = $extend->getResultValues('registerOrder','req_id', $req,'id','1');
            if($res == VT_ERR_STILL_IN_CHARGING_PERIOD || $res == VT_ERR_ALREADY_CANCELED){
                // UPdate service_subscriber_mapping HUY
                $table = "service_subscriber_mapping";
                $service_map = $extend->getResultValuesService($table,'subscriber_id',$registerOrder['subscriber_id'], 'service_id', $registerOrder['service_id'], 'id',1);
                $query = $extend->getUpdateServiceMapping(0, $service_map['id'],$date);
                // insert subscriber_transition
                $field = "subscriber_id, service_id, purchase_type, channel, status, created_at, updated_at";
                $values = "'".$registerOrder['subscriber_id']."','".$registerOrder['service_id']."','3','".$registerOrder['channel']."','1','".$date."','".$date."'";
                $table = "subscriber_transition ";
                $query = $extend->getInsertNewValues($table, $field, $values);    
                $errorName = "Thông báo: Quý khách đã hủy thành công gói dịch vụ. Cảm ơn Quý khách đã sử dụng dịch vụ của Viettel!";
            }
            else {
                    $errorName = "Thông báo: Hệ thống đang bận, Quý khách vui lòng thử lại sau!";
            }
            unset($_SESSION['responseToUser']);
            $_SESSION['responseToUser'] =$errorName;
            header('Location: http://giovangchotso.vn/');exit();
        }
        catch (Exception $e) {
                $errorName = "Thông báo: Hệ thống đang bận, Quý khách vui lòng thử lại sau!";
        }
    } 
    elseif($cmd == 'MSISDN') {
        if($mobile != null){
            $mobile = $extend->validatorMobile($mobile);
        }
        $date = date('Y-m-d H:i:s', time());
        $_SESSION['msisdn'] = $mobile;
//        $_SESSION['msisdn'] = '84974573941';
        if($mobile != null){
            $field = 'msisdn,created_at,updated_at';
            $values = "'$mobile','$date','$date'";
            $query = $extend->getInsertNewValues('subscriber', $field, $values);
        }
        header('Location: http://giovangchotso.vn/');exit();
    }
    
    